let courseJs = undefined;

export function setCourceJsAPI(charpter, section) {
    courseJs = undefined;
    if (charpter === 'Charpter3') {
        if (section === 'section_1') {
            courseJs = require('../asset/cource/charpter3_1.js');
        }
        if (section === 'section_2') {
            courseJs = require('../asset/cource/charpter3_2.js');
        }
        if (section === 'section_3') {
            courseJs = require('../asset/cource/charpter3_3.js');
        }
        if (section === 'section_4') {
            courseJs = require('../asset/cource/charpter3_4.js');
        }
        if (section === 'section_5') {
            courseJs = require('../asset/cource/charpter3_5.js');
        }
        if (section === 'section_6') {
            courseJs = require('../asset/cource/charpter3_6.js');
        }
    }
}

export function getSectionIndexAPI(charpter, section) {
    return "單元範例-例題" + section.charAt(section.length-1)
}

export function getSectionTitleAPI(charpter, section) {
    return courseJs.courceTitle
}

export function getProgramTopicTagsAPI(charpter, section) {
    return courseJs.sampleCode
}

export function setNextStageAPI(charpter, section, sampleCodeIndicateIndex, interactiveIndicateIndex) {
    return courseJs.nextStage(sampleCodeIndicateIndex, interactiveIndicateIndex);
}

export function setPriviousStageAPI(charpter, section, sampleCodeIndicateIndex, interactiveIndicateIndex){
    return courseJs.priviousStage(sampleCodeIndicateIndex, interactiveIndicateIndex);
}

export function getCourceTeachingAreaAPI(sampleCodeIndicateIndex, interactiveIndicateIndex) {
    return courseJs.courceTeachingArea(sampleCodeIndicateIndex, interactiveIndicateIndex)
}

export function getProgramGrammarAPI(){
    return courseJs.getProgramGrammar()
}

export function getPrintResultAPI() {
    return courseJs.getPrintResult()
}

export function getInputObjectAPI(sampleCodeIndicateIndex, interactiveIndicateIndex){
    return courseJs.getInputObject(sampleCodeIndicateIndex, interactiveIndicateIndex)
}

export function getProgramExplainAPI(){
    return courseJs.programExplaiin
}