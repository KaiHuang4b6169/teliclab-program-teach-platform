import { useState } from 'react';
import { Modal, Stack } from 'react-bootstrap';
import { FaAngleLeft, FaAngleRight } from 'react-icons/fa';
import { getProgramExplainAPI } from '../../api/chapter3Api';
import { PROGRAM_EXPLAIN } from '../../utils/const';
import { controlToolIconStyle, courceAreaLabelStyle } from '../../utils/css/commonStyle';

function ControlTool(props){
    const {nextStage, previousStage, visualizeMode, simulationMode, setSimulationModePlaying} = props
    const [showDialog, setShowDialog] = useState(false);

    const hintContent = getProgramExplainAPI();
    
    const visualizeModeButton = (
        <div>
            <button className="btn btn-default px-1" onClick={previousStage}><FaAngleLeft style={controlToolIconStyle}/></button>
            <button className="btn btn-default px-1" onClick={nextStage}><FaAngleRight style={controlToolIconStyle}/></button>
        </div>
    );

    const simulationModeButton = (
        <div>
            <button className="btn btn-default px-1" onClick={() => setSimulationModePlaying(true)}>
                <img src={require('../../asset/simulation.png')} width="40px" height="40px"/>
            </button>
        </div>
    );

    const hintIcon = (
        <div>
            <button className="btn btn-default px-1" onClick={() => setShowDialog(true)}>
                <img src={require('../../asset/hint.png')} width="40px" height="40px"/>
            </button>
            <div style={courceAreaLabelStyle}>{PROGRAM_EXPLAIN}</div>
        </div>
    );

    const navigatorIcon = (
        <div className="ms-auto">
            {visualizeMode ? visualizeModeButton : ''}
            {simulationMode? simulationModeButton : ''}
            <div style={{height:24}}></div>
        </div>
    );

    return (
        <Stack direction="horizontal" gap={2} className="my-2">
            {hintIcon}
            {navigatorIcon}
            <Modal show={showDialog} onHide={() => setShowDialog(false)}>
                <Modal.Header closeButton>
                    <Modal.Title>Hint</Modal.Title>
                </Modal.Header>
                <Modal.Body style={{whiteSpace: 'pre-wrap'}}>{hintContent}</Modal.Body>
            </Modal>
        </Stack>
    );
}

export default ControlTool;