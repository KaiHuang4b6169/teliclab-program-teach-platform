import { Stack } from "react-bootstrap";


function CourceTeachingArea(){
    

    const elementTitleStyle = {color: 'DarkGreen'};
    const elementRangeBackgroundStyle = {background: '#D1A4CA'};

    return (
        
        <Stack direction="horizontal">
            
            <div style={{display: 'inline-block'}}>
                <div className="text-center" style={elementTitleStyle}></div>
                <Stack direction="horizontal" style={{whiteSpace: 'nowrap'}}>
                    <div>1</div>
                    <div>2</div>
                </Stack>
            </div>

            
            <div className='ms-auto px-1' style={{display: 'inline-block'}}>
                <div className="text-center" style={elementTitleStyle}></div>
                <Stack direction="horizontal" style={{...elementRangeBackgroundStyle, whiteSpace: 'nowrap'}}>
                    <div>
                        range(
                        <span style={{background: 'white'}}>1</span>
                        ,
                        <span style={{background: 'white'}}>11</span>
                        ,
                        <span style={{background: 'white'}}>1</span>
                        )
                    </div>
                </Stack>
            </div>


            <div className='px-1'>
                <div className="text-center" style={elementTitleStyle}></div>
                <div>迴圈變數</div>
                <div>範圍設定</div>
            </div>

    </Stack>  
    );
}

export default CourceTeachingArea;