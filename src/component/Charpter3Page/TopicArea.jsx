
import { Card, Table } from "react-bootstrap";
import { getProgramTopicTags } from "../../hook/useCharpter3";
import { debugArrowStyle } from "../../utils/css/commonStyle";

function TopicArea(props){
    const {charpterTitle, sectionUrl, indicateIndex} = props
    const DebugArror = (<img src={require('../../asset/debugArrow.png')} width="20px"/>);
    const programTopicTags = getProgramTopicTags();
    
    const sequenceProgramTopic = (
        <Table className="table-borderless my-0">
            <tbody>
                {programTopicTags.map(function(oneLineTopic, i){
                    return (
                        <tr key={i}>
                            <td className='px-0 py-0' style={debugArrowStyle}>{i+1 === indicateIndex ? DebugArror : null}</td>
                            <td  className='py-0'>{oneLineTopic}</td>
                        </tr>
                    );
                })}
            </tbody>
        </Table>
    );

    return (
        <Card>
            <Card.Body>
                {sequenceProgramTopic}
            </Card.Body>
        </Card>
    );
}

export default TopicArea;