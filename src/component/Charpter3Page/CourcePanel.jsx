import { useEffect, useState } from "react";
import { ButtonGroup, Card, Col, Container, Navbar, Row } from "react-bootstrap";
import { COURCE_INTERACT_AREA_TITLE, COURCE_INTERACT_COMPONENT_TITLE, COURCE_TOPIC_COMPONENT_TITLE, PROGRAMMING_GRAMMAR, SIMULATION_MODE, VIISUALIZE_MODE } from "../../utils/const";
import { courceAreaLabelStyle } from "../../utils/css/commonStyle";
import TopicArea from "./TopicArea";
import ControlTool from "./ControlTool";
import { getSectionIndex, getSectionTitle, setNextStage, setPriviousStage, setSection } from "../../hook/useCharpter3";
import { getCourceTeachingAreaAPI, getProgramGrammarAPI } from "../../api/chapter3Api";
import CourceInteractiveArea from "./CourceInteractiveArea";


function CourcePanel(props){
    const {charpterTitle, sectionUrl} = props;

    setSection(charpterTitle, sectionUrl);
    const [visualizeMode, setVisualizeMode] = useState(true);
    const [simulationMode, setSimulationMode] = useState(false);
    const [simulationModePlaying, setSimulationModePlaying] = useState(false);
    const [sampleCodeIndicateIndex, setSampleCodeIndicateIndex] = useState(0);
    const [interactiveIndicateIndex, setInteractiveIndicateIndex] = useState(0);
    const [newState, setNewState] = useState(0);
    const [teachingPanel, setTeachingPanel] = useState(undefined);
    const nextStage = () => {
        const netIndicateIndex = setNextStage(charpterTitle, sectionUrl, sampleCodeIndicateIndex, interactiveIndicateIndex);
        setSampleCodeIndicateIndex(netIndicateIndex.sampleCodeIndicateIndex);
        setInteractiveIndicateIndex(netIndicateIndex.interactiveIndicateIndex);
        setNewState(newState+1)
    };

    const previousStage = () => {
        const netIndicateIndex = setPriviousStage(charpterTitle, sectionUrl, sampleCodeIndicateIndex, interactiveIndicateIndex);
        setSampleCodeIndicateIndex(netIndicateIndex.sampleCodeIndicateIndex);
        setInteractiveIndicateIndex(netIndicateIndex.interactiveIndicateIndex);
        setNewState(newState-1)
    };

    useEffect(() => {
        setTeachingPanel(getCourceTeachingAreaAPI(sampleCodeIndicateIndex, interactiveIndicateIndex));
    }, [newState])
    
    const switchToSimulationMode = () => {
        setVisualizeMode(false);
        setSimulationMode(true);
        setSampleCodeIndicateIndex(0);
        setInteractiveIndicateIndex(0);
        setNewState(0)
    };
    
    const switchToVisualizeMode = () => {
        setVisualizeMode(true);
        setSimulationMode(false);
        setSimulationModePlaying(false);
        setSampleCodeIndicateIndex(0);
        setInteractiveIndicateIndex(0);
        setNewState(0)
    };

    const sectionIndexTitle = getSectionIndex(charpterTitle, sectionUrl);
    const courceTitle = getSectionTitle(charpterTitle, sectionUrl);
    const programGrammarContent = getProgramGrammarAPI();

    const programGrammar = (
        <Card>
            <Card.Body>
                <h5 style={courceAreaLabelStyle}>{PROGRAMMING_GRAMMAR}</h5>
                {programGrammarContent}
            </Card.Body>
        </Card>
    )

    const sectionIndex = (
        <Navbar>
            <Container fluid className="ps-0">
                <Navbar.Brand>{sectionIndexTitle}</Navbar.Brand>
            </Container>
        </Navbar>
    );

    const modSwticher = (
        <ButtonGroup aria-label="Basic radio toggle button group">
            <input type="radio" className="btn-check" name="btnradio" id="btnradio1" autoComplete="off" onChange={switchToVisualizeMode} checked={visualizeMode}/>
            <label className="btn btn-outline-secondary" htmlFor="btnradio1">{VIISUALIZE_MODE}</label>

            <input type="radio" className="btn-check" name="btnradio" id="btnradio2" autoComplete="off" onChange={switchToSimulationMode} checked={simulationMode}/>
            <label className="btn btn-outline-secondary" htmlFor="btnradio2">{SIMULATION_MODE}</label>
        </ButtonGroup>
    );

    const courceContentCard = (
        <Card>
            <Card.Title className="py-2">
                <Row>
                    <Col xs lg='1'><img src={require('../../asset/question.png')}/></Col>
                    <Col className="mx-5 position-relative">
                        <div className="position-absolute top-50 start-0 translate-middle-y">{courceTitle}</div>
                    </Col>
                </Row>
            </Card.Title>
            <Card.Body>
                <Row>
                    <Col xs lg="4">
                        <h5 style={courceAreaLabelStyle}>{COURCE_TOPIC_COMPONENT_TITLE}</h5>
                        <TopicArea 
                            charpterTitle={charpterTitle}
                            sectionUrl={sectionUrl}
                            indicateIndex={sampleCodeIndicateIndex}
                        />
                        <ControlTool
                            nextStage = {nextStage}
                            previousStage = {previousStage}
                            visualizeMode = {visualizeMode}
                            simulationMode = {simulationMode}
                            setSimulationModePlaying = {setSimulationModePlaying}
                        />
                        {programGrammar}
                    </Col>

                    <Col xs lg="8">
                        <h5 style={courceAreaLabelStyle}>{COURCE_INTERACT_COMPONENT_TITLE}</h5>
                        <Card>
                            <Card.Body>
                                {teachingPanel}
                            </Card.Body>

                            <Card.Footer className="bg-transparent">
                                <h5 style={courceAreaLabelStyle}>{COURCE_INTERACT_AREA_TITLE}</h5>
                                <CourceInteractiveArea 
                                    isPlaying = {simulationModePlaying}
                                    visualizeMode = {visualizeMode}
                                    nextStage = {nextStage}
                                    sampleCodeIndicateIndex = {sampleCodeIndicateIndex}
                                    interactiveIndicateIndex = {interactiveIndicateIndex}
                                    key = {visualizeMode}
                                />
                            </Card.Footer>
                        </Card>
                        
                    </Col>
                </Row>
            </Card.Body>
        </Card>
    );

    return (
        <Card>
            <Card.Body>
                {sectionIndex}
                <Row>{modSwticher}</Row>
                <Row className="mx-2 mt-4">{courceContentCard}</Row>
            </Card.Body>
        </Card>
    );
}


export default CourcePanel;