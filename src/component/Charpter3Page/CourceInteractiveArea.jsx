
import { useState } from "react";
import { Row, Button, Stack } from "react-bootstrap";
import { getInputObjectAPI, getPrintResultAPI } from "../../api/chapter3Api";

function CourceInteractiveArea(props){
    const {isPlaying, visualizeMode, nextStage, sampleCodeIndicateIndex, interactiveIndicateIndex} = props;

    const [inputValue, setInputValue] = useState(undefined);
    const [isValid, setIsValid] = useState(true);
    const [errorMsg, setErrorMsg] = useState('');
    let onClickCallBack = () => {nextStage()}
    
    const inputTages = (
        getInputObjectAPI(sampleCodeIndicateIndex, interactiveIndicateIndex).map((inputObject, key) => {
            let valueProps = {}
            if (visualizeMode){
                valueProps = {value: inputObject.defaultValue}
                inputObject.onChange(valueProps.value)
            }

            if (inputObject.isShow && inputObject.isActive) {
                // if (inputValue !== undefined) 
                onClickCallBack = () => {
                    const validObject = inputObject.isValid(inputValue)
                    setInputValue(undefined)
                    setErrorMsg(validObject.errorMsg)
                    setIsValid(validObject.valid)
                    if (validObject.valid) nextStage()
                }}
            
            const fieldElement =  (
                <Row className='my-2' key={key} style={{height: '40px'}}>
                    <Stack direction="horizontal" gap={2}>
                        <label>{inputObject.label}</label>
                        <div className = "input-group" style={{width: '100px'}} >
                            <input 
                                className={'form-control ' + (!(!isValid && inputObject.isActive) || visualizeMode ? '' : 'is-invalid')}
                                onChange={e => {
                                    setInputValue(e.target.value)
                                    inputObject.onChange(e.target.value)
                                }}  
                                disabled={!(inputObject.isActive) || visualizeMode}
                                {...valueProps}
                            />
                            <div className="invalid-tooltip">{errorMsg}</div>
                        </div>
                    </Stack>
                </Row>
            );
            
            return inputObject.isShow ? fieldElement : undefined;
        })
    )

    const nextButton = (
        <Button 
            onClick={onClickCallBack} 
            className="mx-2 ms-auto" 
            variant="primary" 
            style={{width: '80px'}}>
                下一步
        </Button>
);

    return (
        <div>
            {inputTages}

            {getPrintResultAPI().map((result, key) => {
                return <div key={key}>{result}</div>
            })}

            <Row>{isPlaying ? nextButton : ''}</Row>
        </div>
    );
}

export default CourceInteractiveArea;