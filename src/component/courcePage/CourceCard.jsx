import { useEffect } from "react";
import { useState } from "react";
import { ButtonGroup, Card, Col, Container, Navbar, Row } from "react-bootstrap";
import { getCourceData, getCourceIndexText, getCourceTitle, getStateObject, setCource } from "../../hook/useCourceContent";
import { SIMULATION_MODE, VIISUALIZE_MODE } from "../../utils/const";
import CourceCardInteractiveArea from "./InteractiveArea/CourceCardInteractiveArea";
import CourceCardTopicArea from "./TopicArea/CourceCardTopicArea";

const TeachModeObject = {
    visualizeMode: true,
    simulationMode: false
}

function CourceCard(props) {
    const {charpterTitle, sectionUrl} = props;
    const [teachMode, setTeachMode] = useState(TeachModeObject);

    setCource(charpterTitle, sectionUrl);

    const setSimulationMode = () => {
        let newTeachMode = {...teachMode}
        newTeachMode.visualizeMode = false;
        newTeachMode.simulationMode = true;
        setTeachMode(newTeachMode);
    };
    const setVisualizeMode = () => {
        let newTeachMode = {...teachMode}
        newTeachMode.visualizeMode = true;
        newTeachMode.simulationMode = false;
        setTeachMode(newTeachMode);
    };

    return (
    <Card>
        <Card.Body>
            <CourceInfomation/>
            <Row>
                <ModSwitcher 
                setSimulationMode={setSimulationMode} 
                setVisualizeMode={setVisualizeMode} 
                teachMode={teachMode}/>
            </Row>
            <Row className="mx-2 mt-4">
                <CourceContant teachMode={teachMode} sectionUrl={sectionUrl}/>
            </Row>
        </Card.Body>
    </Card>
    );
}

function CourceInfomation(){
    return (
        <Navbar>
            <Container fluid className="ps-0">
                <Navbar.Brand>{getCourceIndexText()}</Navbar.Brand>
            </Container>
        </Navbar>
    );
}

function ModSwitcher(props){
    const {setVisualizeMode, setSimulationMode, teachMode} = props

    return (
        <ButtonGroup aria-label="Basic radio toggle button group">
            
            <input type="radio" className="btn-check" name="btnradio" id="btnradio1" autoComplete="off" onChange={setVisualizeMode} checked={teachMode.visualizeMode}/>
            <label className="btn btn-outline-secondary" htmlFor="btnradio1">{VIISUALIZE_MODE}</label>

            <input type="radio" className="btn-check" name="btnradio" id="btnradio2" autoComplete="off" onChange={setSimulationMode} checked={teachMode.simulationMode}/>
            <label className="btn btn-outline-secondary" htmlFor="btnradio2">{SIMULATION_MODE}</label>
        </ButtonGroup>
    );
}

function CourceContant(props){
    const {teachMode, sectionUrl} = props
    const [activateState, setActivateState] = useState([]);
    const [teachData, setTeachData] = useState(getCourceData());
    const [isPlaying, setIsPlaying] = useState(false);
    const setPlaying = () => setIsPlaying(true);

    useEffect(() => {
        setActivateState(getStateObject());
        setTeachData(getCourceData());
        setIsPlaying(false);
    }, [teachMode, setActivateState])
    

    const setNextActivateStateStep = () => {
        let newActivateState = {...activateState}
        const isAddTeachStep = activateState.teachStep < activateState.teachStepOfTopic[activateState.topicStep];
        const isAddTopicStep = !(activateState.teachStep < activateState.teachStepOfTopic[activateState.topicStep]) && 
                                (activateState.topicStep < activateState.teachStepOfTopic.length - 1);

        if (isAddTeachStep) newActivateState.teachStep = activateState.teachStep + 1;
        if (isAddTopicStep) {
            newActivateState.teachStep = 0;
            newActivateState.topicStep = activateState.topicStep + 1;
        }
        setActivateState(newActivateState);
    };

    const setPreActivateStateStep = () => {
        let newActivateState = {...activateState};
        const isSubTeachStep = activateState.teachStep > 0;
        const isSubTopicStep = !(activateState.teachStep > 0) && 
                                (activateState.topicStep > 0);

        if (isSubTeachStep) newActivateState.teachStep = activateState.teachStep - 1;
        if (isSubTopicStep) {
            newActivateState.teachStep = activateState.teachStepOfTopic[activateState.topicStep - 1];
            newActivateState.topicStep = activateState.topicStep - 1
        };
        setActivateState(newActivateState);
    };
    
    return (
        <Card>
            <Card.Title className="py-2">
                <Row>
                    <Col xs lg='1'><img src={require('../../asset/question.png')}/></Col>
                    <Col className="mx-5 position-relative">
                        <div className="position-absolute top-50 start-0 translate-middle-y">{getCourceTitle()}</div>
                    </Col>
                </Row>
            </Card.Title>
            <Card.Body>
                <Row>
                    <Col xs lg="4">
                        <CourceCardTopicArea
                            teachMode={teachMode}
                            activateState={activateState}
                            setPlaying={setPlaying}
                            setNextActivateStateStep={setNextActivateStateStep}
                            setPreActivateStateStep={setPreActivateStateStep}
                            teachData={teachData}
                        />
                    </Col>
                    <Col xs lg="8">     
                        <CourceCardInteractiveArea
                            isPlaying={isPlaying}
                            teachMode={teachMode}
                            setNextActivateStateStep={setNextActivateStateStep}
                            activateState={activateState}
                            sectionUrl={sectionUrl}
                            teachData={teachData}
                            setTeachData={setTeachData}
                        />
                    </Col>
                </Row>
            </Card.Body>
        </Card>
    );
}


export default CourceCard;