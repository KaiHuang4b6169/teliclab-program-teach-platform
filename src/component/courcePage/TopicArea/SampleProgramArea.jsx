import { Card, Modal, Stack, Table } from "react-bootstrap";
import { getSampleCode, isShowSampleCodeArrow } from "../../../hook/useCourceContent";

function SampleProgramArea(props) {
    const {activateStep, teachData} = props
    
    const debugArrowColStyle = {
        width: '20px',
    }

    const DebugArror = (<img src={require('../../../asset/debugArrow.png')} width="20px"/>);

    return (
        <Table className="table-borderless my-0">
            <tbody>
                {getSampleCode().map(function(oneLineTopic, i){
                    return (
                        <tr key={i}>
                            <td className='px-0 py-0' style={debugArrowColStyle}>{isShowSampleCodeArrow(activateStep, i, teachData) ? DebugArror : null}</td>
                            <td  className='py-0'>{oneLineTopic}</td>
                        </tr>
                    );
                })}
            </tbody>
        </Table>
    );
}

export default SampleProgramArea;