import { Card} from "react-bootstrap";
import { COURCE_TOPIC_COMPONENT_TITLE, PROGRAMMING_GRAMMAR } from "../../../utils/const";
import SampleProgramArea from "./SampleProgramArea";
import FootTools from "./FootTools";
import { getProgrammingGrammar } from "../../../hook/useCourceContent";
import { useEffect } from "react";


function CourceCardTopicArea(props){
    const {teachMode, activateState, setPlaying, setNextActivateStateStep, setPreActivateStateStep, teachData} = props
    
    useEffect(()=>{
        document.addEventListener('contextmenu', (e) => {
            e.preventDefault();
          });
        document.addEventListener("keydown", function (event) {
            if (event.ctrlKey) {
                event.preventDefault();
            }   
        });
    });

    return (
        <div>
            <h5 style={{color:'red'}}>{COURCE_TOPIC_COMPONENT_TITLE}</h5>
            <Card>
                <Card.Body>
                    <SampleProgramArea activateStep={activateState.topicStep} teachData={teachData}/>
                </Card.Body>
            </Card>
            <FootTools
                teachMode={teachMode}
                setPlaying={setPlaying}
                setNextActivateStateStep={setNextActivateStateStep}
                setPreActivateStateStep={setPreActivateStateStep}
            />
            
            <Card>
                <Card.Body>
                    <h5 style={{color:'red'}}>{PROGRAMMING_GRAMMAR}</h5>
                    {getProgrammingGrammar()}
                </Card.Body>
            </Card>
        </div>
    );
}

export default CourceCardTopicArea;