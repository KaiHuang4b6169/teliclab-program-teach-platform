import { Card, Modal, Stack, Table } from "react-bootstrap";
import { FaAngleLeft, FaAngleRight } from 'react-icons/fa';
import { useState } from "react";
import { PROGRAM_EXPLAIN } from "../../../utils/const";
import { getHintContent } from "../../../hook/useCourceContent";

function FootTools(props){
    const {teachMode, setPlaying, setNextActivateStateStep, setPreActivateStateStep} = props
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const iconStyle = {
        width: '40px',
        height: '40px',
    }

    const visualizeModeButton = (
        <div>
            <button className="btn btn-default px-1" onClick={setPreActivateStateStep}><FaAngleLeft style={iconStyle}/></button>
            <button className="btn btn-default px-1" onClick={setNextActivateStateStep}><FaAngleRight style={iconStyle}/></button>
        </div>
    );
    const simulationModeButton = (
        <div>
            <button className="btn btn-default px-1" onClick={setPlaying}>
                <img src={require('../../../asset/simulation.png')} width="40px" height="40px"/>
            </button>
        </div>
    );

    return (
        <Stack direction="horizontal" gap={2} className="my-2">
            <div>
                <button className="btn btn-default px-1" onClick={handleShow}>
                    <img src={require('../../../asset/hint.png')} width="40px" height="40px"/>
                </button>
                <div style={{color:'red'}}>{PROGRAM_EXPLAIN}</div>
            </div>
            <div className="ms-auto">
                {teachMode.visualizeMode ? visualizeModeButton : ''}
                {teachMode.simulationMode ? simulationModeButton : ''}
                <div style={{height:24}}></div>
            </div>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                <Modal.Title>Hint</Modal.Title>
                </Modal.Header>
                <Modal.Body>{getHintContent()}</Modal.Body>
            </Modal>
        </Stack>
    );
}

export default FootTools;