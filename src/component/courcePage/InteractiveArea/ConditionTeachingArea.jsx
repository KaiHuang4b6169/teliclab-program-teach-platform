import { Stack } from "react-bootstrap";
import { getConditionCourceData, getGraphCondition } from "../../../hook/useCourceContent";
import { isShow } from "../../../utils/tool";

function statementFactory(type, label, statement) {
    let blockStyle = {width:'50px'};
    let labelStyle = {fontSize: '6px', height: '18px'}
    let statementStyle = {height: '32px'}

    if (type === 'variable'){
        statementStyle = {...statementStyle, background: 'peachpuff'}
        blockStyle = (statement.length > 4) ? {width: statement.length * 12 + 'px'} : blockStyle;
    }
    if (type === 'text'){
        blockStyle = {width: statement.length * 12 + 'px'};
    }

    return (
        <Stack gap={2} className='p-1' style={blockStyle}>
            <div style={labelStyle}>{label}</div>
            <div className="position-relative" style={statementStyle}>
                <div className="position-absolute top-50 start-50 translate-middle">{statement}</div>
            </div>
        </Stack>
    );

}

function statementBlockFactory(type, content, props) {
    const conditionElement =function (title, contentArray) {
        const conditionElementTitleStyle = {color: 'DarkGreen'};
        const conditionElementBackgroundStyle = {background: '#aad8c1'};
    
        return (
            <div style={{marginTop: '90px'}}>
                <div className="text-center" style={conditionElementTitleStyle}>{title}</div>
                <Stack direction="horizontal" style={{...conditionElementBackgroundStyle, whiteSpace: 'nowrap'}}>
                    {contentArray.map(function(component, i) {
                        return statementFactory(component.type, component.lable, component.content);
                    })}
                </Stack>
            </div>
        );
    }
    
    const printElement = function (title, contentArray) {
        return (
            <Stack className="align-self-center">
                <div className="text-start" style={{fontSize: '6px', whiteSpace: 'pre-wrap'}}>{title}</div>
                <Stack direction="horizontal" style={{background: 'lightskyblue', whiteSpace: 'nowrap'}}>                    
                    {contentArray.map(function(component, i) {
                        return statementFactory(component.type, component.lable, component.content);
                    })}
                </Stack>
            </Stack>
        );
    }
    if (type === 'conditionStatementBlock')
        return conditionElement(content.title, content.data);
    if (type === 'printBlock')
        return printElement(content.title, content.data);
    if (type === 'conditionBlock')
        return (<ConditionTeachingArea activateState={props.activateState} teachData={props.teachData} conditionCourceData={content.data} graphCondition = {props.graphCondition.childCondition.false}/>);
}

function ConditionTeachingArea(props) {
    const {activateState, teachData, conditionCourceData, graphCondition} = props;
    let conditinoStatementElement = undefined;
    let trueStatementElement = undefined;
    let falseStatementElement = undefined;

    const {showArrow, changeLink, isStatify} = graphCondition;
    const isShowConditionArrow = isShow(showArrow.showInTopicStep, showArrow.showInTeachStep,activateState)
    const isChangedLink = isShow(changeLink.showInTopicStep, changeLink.showInTeachStep,activateState)
    const conditionArrow = isStatify ? <img src={require('../../../asset/Ture_arrow.png')} height="75px" width="75px"/> : <img src={require('../../../asset/flase_arrow.png')} height="75px" width="75px"/>
    const conditionLink = isStatify ? <img src={require('../../../asset/True.png')} height="300px"/> : <img src={require('../../../asset/Flase.png')} height="300px"/>

    conditionCourceData.map(function(component, i) {
        const componentContent = component.content
        if (component.type === 'condition')
            conditinoStatementElement = statementBlockFactory(componentContent.type, componentContent.content, props);
        if (component.type === 'true')
            trueStatementElement = statementBlockFactory(componentContent.type, componentContent.content, props);
        if (component.type === 'false')
            falseStatementElement = statementBlockFactory(componentContent.type, componentContent.content, props);
    });

    const conditionTeachContent = (
        <Stack direction="horizontal" gap={3}>
            <Stack style={{flex: '0 1 auto'}}>
                {conditinoStatementElement}
                {isShowConditionArrow ? conditionArrow : undefined}
            </Stack>
            
            {isChangedLink ? conditionLink : <img src={require('../../../asset/if.png')} height="300px"/>}
            
            <Stack gap={2} className="flex-grow-0">                
                <div>{trueStatementElement}</div>
                <div className="mt-auto">{falseStatementElement}</div>
            </Stack>
        </Stack>
    );

    return conditionTeachContent;
}

export default ConditionTeachingArea;