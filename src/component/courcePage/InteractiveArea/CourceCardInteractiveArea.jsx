import { useState, useEffect } from "react";
import { Row, Card, Stack, Button } from "react-bootstrap";
import { COURCE_INTERACT_AREA_TITLE, COURCE_INTERACT_COMPONENT_TITLE } from "../../../utils/const";
import TeachingArea from "./TeachingArea";
import {isShow, isAtState} from "../../../utils/tool.js";
import { getCourceData, getVisualizeInputObject, getSimulationInputObject, getPrintResult } from "../../../hook/useCourceContent";

function CourceCardInteractiveArea(props){
    const {isPlaying, teachMode, setNextActivateStateStep, activateState, sectionUrl, teachData, setTeachData} = props;
    
    return (
        <div>
            <h5 style={{color:'red'}}>{COURCE_INTERACT_COMPONENT_TITLE}</h5>
            <Card>
                <Card.Body>
                    <TeachingArea
                        teachData={teachData}
                        isPlaying={isPlaying}
                        activateState={activateState}
                    />
                </Card.Body>
                <Card.Footer className="bg-transparent">
                    <InteractiveArea 
                        teachData={teachData} 
                        setTeachData={setTeachData}
                        isPlaying={isPlaying} 
                        teachMode={teachMode}
                        activateState={activateState} 
                        setNextActivateStateStep={setNextActivateStateStep}
                    />
                </Card.Footer>
            </Card>
        </div>
    );
}

function InteractiveArea(props){
    const {teachData, setTeachData, teachMode, isPlaying, activateState, setNextActivateStateStep} = props;

    const fieldStateObject = {
        value: '',
        callback: (value) => { return {isValid:true, errorMsg:''}},
    }

    const inputValidState = {
        valid: true,
        errorMsg: '',
    }

    const [inputData, setInputData] = useState(getVisualizeInputObject());
    const [fieldState, setFieldState] = useState(fieldStateObject);
    const [isInputValid, setIsInputValid] = useState(inputValidState);
    const printPanel = isShow(1, 0, activateState) ? getPrintResult(teachData) : [];

    useEffect(()=>{
        if (teachMode.visualizeMode) setInputData(getVisualizeInputObject());
        if (teachMode.simulationMode) setInputData(getSimulationInputObject());
    }, [teachMode])

    const handleChange = (event, index) => {
        let variableData = {...inputData[index], content: event.target.value}
        let newInputData = [ ...inputData]
        newInputData[index] = variableData
        setInputData(newInputData);
        setTeachData(variableData.setData([...teachData], event.target.value))
    };

    const nextButton = (
            <Button 
                onClick={() => {
                    const {isValid, errorMsg} = fieldState.callback(fieldState.value);
                    setIsInputValid({
                        valid: isValid,
                        errorMsg: errorMsg,
                    });
                    if (isValid)
                        setNextActivateStateStep(); 
                }} 
                className="mx-2 ms-auto" 
                variant="primary" 
                style={{width: '80px'}}>
                    下一步
            </Button>
    );

    return (
        <div>
            <Row>
                <h5 style={{color:'red'}}>{COURCE_INTERACT_AREA_TITLE}</h5>
            </Row>

            {inputData.map(function(field, i){
                const isActive = isPlaying && isAtState(field.showInTopicStep, field.showInTeachStep, activateState)
                const isNewState = !(fieldState.value === field.content && fieldState.callback === field.isValid)
                const isValid = !isActive || isInputValid.valid
                if (isActive && isNewState){
                    setFieldState({
                        value: field.content,
                        callback: field.isValid,
                    })
                }

                const fieldElement = (
                    <Stack direction="horizontal" gap={2}>
                        <label>{field.label}</label>
                        <div className = "input-group" style={{width: '100px'}} >
                            <input 
                                className={'form-control ' + (isValid ? '' : 'is-invalid')}
                                value={field.content} 
                                onChange={e => handleChange(e, i)}  
                                disabled={!isActive}
                            />
                            <div className="invalid-tooltip">{isInputValid.errorMsg} </div>
                        </div>
                    </Stack>
                );

                return (
                    <Row className='my-2' key={i} style={{height: '40px'}}>
                        {isShow(field.showInTopicStep, field.showInTeachStep, activateState) ? fieldElement : ''}
                    </Row>
                );
            })}

            {printPanel.map(function(result, i) {
                const printResultComponet =  (<Row style={{height: '40px'}}><label className="py-1">{result.print}</label></Row>)
                return isShow(result.showInTopicStep, result.showInTeachStep, activateState) ? printResultComponet : undefined
            })}

            <Row>{isPlaying ? nextButton : ''}</Row>
        </div>
    );
}

export default CourceCardInteractiveArea;