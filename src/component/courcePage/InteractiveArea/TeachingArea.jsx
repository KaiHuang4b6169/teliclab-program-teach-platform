import { getConditionCourceData, getGraphCondition } from "../../../hook/useCourceContent";
import ConditionTeachingArea from "./ConditionTeachingArea";
import { CodeExplain, TeachAreaElementParser, TeachAreaPrintElementParser } from "./TeachingAreaElement";

function TeachingArea(props){
    const {teachData, isPlaying, activateState} = props
    const teachAreatypeList = activateState.teachAreatype;
    let teachAreaType = 'normal';
    if (teachAreatypeList) 
        teachAreatypeList.map(function (element){
            if (element.topicStep <= activateState.topicStep) teachAreaType =  element.type;
        });
    
    const normalType = teachData.map(function(row, i){
        return (
            <CodeExplain key={i}>
                {row.data.map(function(element, j){
                    if (row.type === 'codeExplain') return (<TeachAreaElementParser {...{...element, activateState}} key={j}/>);
                    if (row.type === 'printer') return (<TeachAreaPrintElementParser {...{...element, activateState}} key={j}/>);
                })}
            </CodeExplain>
        );
    })
    
    return (
        <div>
            {teachAreaType === 'normal' ? normalType : null}
            {teachAreaType === 'condition' ? <ConditionTeachingArea activateState={activateState} teachData={teachData} conditionCourceData={getConditionCourceData(teachData)} graphCondition={getGraphCondition(teachData)}/> : null}
        </div>
    );
}

export default TeachingArea;