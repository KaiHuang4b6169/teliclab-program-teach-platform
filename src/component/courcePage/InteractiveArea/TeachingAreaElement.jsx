import { Table } from "react-bootstrap";
import {isShow} from "../../../utils/tool.js";

const icon = { assignArrow: require('../../../asset/assignArrow.png')};

const codeTableElementStyle = {
    height: '36px',
};

const labelTableElementStyle = {
    height: '24px',
};

function HintElement(props){
    const {label, content, fieldStyle, labelStyle, contextStyle, showInTopicStep, showInTeachStep, activateState} = props;
    const hintStyle = {
        width: '96px',
    }

    return (
        <td style={{...hintStyle, ...fieldStyle}}>
            <div className='mb-1' style={{...labelTableElementStyle, ...labelStyle}}>{label}</div>
            <div className='position-relative' style={{...codeTableElementStyle, ...contextStyle}}>
                <div className='position-absolute top-50 start-0 translate-middle-y'>
                    {isShow(showInTopicStep, showInTeachStep, activateState)? content: ''}
                </div>
            </div>
        </td>
    );
}

function ContentElement(props){
    const {label, content, fieldStyle, labelStyle, contextStyle, showInTopicStep, showInTeachStep, activateState} = props;
    const colWidth = Math.max(content.length, label.length) * 12
    return (
        <td className='px-0' style={{...fieldStyle, width: colWidth, whiteSpace: 'nowrap'}}>
            <div className='mb-1' style={{...labelTableElementStyle, ...labelStyle}}>{label}</div>
            <div className='position-relative' style={{...codeTableElementStyle, ...contextStyle}}>
                <div className='position-absolute top-50 start-50 translate-middle'>
                    {isShow(showInTopicStep, showInTeachStep, activateState)? content: ''}
                </div>
            </div>
        </td>
    );
}

function IconElement(props){
    const {content, fieldStyle, labelStyle, contextStyle, showInTopicStep, showInTeachStep, activateState} = props;
    const hintStyle = {
        width: '96px',
    }

    return (
        <td style={{...hintStyle, ...fieldStyle}}>
            <div style={{...labelTableElementStyle, ...labelStyle}}></div>
            <div style={{...codeTableElementStyle, ...contextStyle}}>
                {isShow(showInTopicStep, showInTeachStep, activateState)? content: ''}
            </div>
        </td>
    );
}

function CodeExplain(props){
    return (
    <Table className="table-borderless my-0">
        <tbody>
            <tr>{props.children}</tr>
        </tbody>
    </Table>
    );
}

function TeachAreaElementParser(props) {
    const {type, lable, content, showInTopicStep, showInTeachStep, activateState} = props
    
    const fontOrange = {
        color: 'LightSalmon'
    }

    const backgroundOrange = {
        background: 'PeachPuff',
    }

    const boarderSolid = {
        borderColor : 'black',
        borderStyle: 'solid'
    }

    if (type === 'hint') 
        return (
            <HintElement 
                label={lable} 
                content={content} 
                labelStyle={fontOrange} 
                contextStyle={fontOrange} 
                showInTopicStep={showInTopicStep} 
                showInTeachStep={showInTeachStep} 
                activateState={activateState}
            />
        );

    if (type === 'variable') 
        return (
            <ContentElement 
                label={lable}
                content={content}
                contextStyle={backgroundOrange}
                showInTopicStep={showInTopicStep}
                showInTeachStep={showInTeachStep}
                activateState={activateState}
            />
        );

    if (type === 'input') 
        return (
            <ContentElement 
                label={lable} 
                content={content} 
                contextStyle={boarderSolid} 
                showInTopicStep={showInTopicStep} 
                showInTeachStep={showInTeachStep} 
                activateState={activateState}
            />
        );

        if (type === 'text')
        return (
            <ContentElement 
                label={lable} 
                content={content} 
                showInTopicStep={showInTopicStep} 
                showInTeachStep={showInTeachStep} 
                activateState={activateState}
            />
        );

    if (type === 'assign') {
        const assignArror = (<img src={icon.assignArrow}  width="72" height="34"/>);
        return (
            <IconElement
                content={assignArror}
                showInTopicStep={showInTopicStep}
                showInTeachStep={showInTeachStep}
                activateState={activateState}
            />
        );
    }

    return (<td></td>);
}

function TeachAreaPrintElementParser(props) {
    const {type, lable, content, showInTopicStep, showInTeachStep, activateState} = props

    const backGroundColor = {
        background: 'lightskyblue'
    }

    const fontOrange = {
        color: 'LightSalmon'
    }

    const backgroundOrange = {
        background: 'PeachPuff',
    }

    const boarderSolid = {
        borderColor : 'black',
        borderStyle: 'solid'
    }
    
    const labelTextStyle = {
        marginBottom: '0px',
        fontSize: '6px',
    }

    if (type === 'hint')
        return (
            <HintElement
                label={lable} 
                content={content} 
                labelStyle={fontOrange} 
                contextStyle={fontOrange} 
                showInTopicStep={showInTopicStep} 
                showInTeachStep={showInTeachStep} 
                activateState={activateState}
            />
        );

    if (type === 'variable')
        return (
            <ContentElement 
                label={lable} 
                content={content} 
                fieldStyle={backGroundColor} 
                contextStyle={backgroundOrange} 
                labelStyle={labelTextStyle} 
                showInTopicStep={showInTopicStep} 
                showInTeachStep={showInTeachStep} 
                activateState={activateState}
            />
        );

    if (type === 'input')
        return (
            <ContentElement 
                label={lable} 
                content={content} 
                fieldStyle={backGroundColor} 
                contextStyle={boarderSolid} 
                labelStyle={labelTextStyle} 
                showInTopicStep={showInTopicStep} 
                showInTeachStep={showInTeachStep} 
                activateState={activateState}
            />
        );

    if (type === 'text')
        return (
            <ContentElement 
                label={lable} 
                content={content} 
                fieldStyle={backGroundColor} 
                labelStyle={labelTextStyle} 
                showInTopicStep={showInTopicStep} 
                showInTeachStep={showInTeachStep} 
                activateState={activateState}
            />
        );

    if (type === 'assign') {
        const assignArror = (<img src={icon.assignArrow}  width="72" height="34"/>);
        return (
            <IconElement 
                content={assignArror} 
                fieldStyle={backGroundColor} 
                showInTopicStep={showInTopicStep} 
                showInTeachStep={showInTeachStep}
                activateState={activateState}
            />
        );
    }

    return (<td></td>);
}

export {CodeExplain, TeachAreaElementParser, TeachAreaPrintElementParser};