import HTMLReactParser from "html-react-parser";

let courseJs;

export function setCource(charpter, section){
    if (charpter === 'Charpter1') {
        if (section === 'section_1') {
            courseJs = require('../asset/cource/charpter1_1.js');
        }
        if (section === 'section_2') {
            courseJs = require('../asset/cource/charpter1_2.js');
        }
        if (section === 'section_3') {
            courseJs = require('../asset/cource/charpter1_3.js');
        }
        if (section === 'section_4') {
            courseJs = require('../asset/cource/charpter1_4.js');
        }
        if (section === 'section_5') {
            courseJs = require('../asset/cource/charpter1_5.js');
        }
        if (section === 'section_6') {
            courseJs = require('../asset/cource/charpter1_6.js');
        }
        if (section === 'section_7') {
            courseJs = require('../asset/cource/charpter1_7.js');
        }
    }
    if (charpter === 'Charpter2') {
        if (section === 'section_1') {
            courseJs = require('../asset/cource/charpter2_1.js');
        }
        if (section === 'section_2') {
            courseJs = require('../asset/cource/charpter2_2.js');
        }
        if (section === 'section_3') {
            courseJs = require('../asset/cource/charpter2_3.js');
        }
        if (section === 'section_4') {
            courseJs = require('../asset/cource/charpter2_4.js');
        }
        if (section === 'section_5') {
            courseJs = require('../asset/cource/charpter2_5.js');
        }
    }
}

export function getHintContent() {
    return HTMLReactParser(courseJs.hintDialogContent);
}

export function getCourceTitle() {
    return HTMLReactParser(courseJs.courceTitle);
}

export function getCourceIndexText() { return courseJs.courceIndex; }

export function getSampleCode(){
    return courseJs.sampleCode.map(function(oneLineCode) {
        return HTMLReactParser(oneLineCode);
    })
}

export function getCourceData() {
    return JSON.parse(JSON.stringify(courseJs.defaultCourceData))
}

export function getConditionCourceData(teachData) {
    return JSON.parse(JSON.stringify(courseJs.conditionCourceData(teachData)))
}

export function getStateObject() {
    return JSON.parse(JSON.stringify(courseJs.stateObject))
}

export function getVisualizeInputObject(){
    return courseJs.inputObject.map(object => ({...object}));
}

export function getSimulationInputObject(){
    let simulationInputObject = courseJs.inputObject.map(object => ({...object}));
    simulationInputObject.forEach(field => field.content = "");
    return simulationInputObject
}

export function getPrintResult(courceData){
    return JSON.parse(JSON.stringify(courseJs.getResult(courceData)))
}

export function isShowSampleCodeArrow(activateStep, index, courceData){
    return courseJs.isShowSampleCodeArrow ? courseJs.isShowSampleCodeArrow(activateStep, index, courceData) : (activateStep === index + 1);
}

export function getGraphCondition(courceData){
    return courseJs.graphCondition(courceData);
}

export function getProgrammingGrammar(){
    return HTMLReactParser(courseJs.programmingGrammar);
}