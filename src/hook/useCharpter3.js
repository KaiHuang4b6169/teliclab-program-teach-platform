import { getProgramTopicTagsAPI, getSectionIndexAPI, getSectionTitleAPI, setCourceJsAPI, setNextStageAPI, setPriviousStageAPI } from "../api/chapter3Api";

export function setSection(charpter, section){
    setCourceJsAPI(charpter, section);
}

export function getSectionIndex(charpter, section){
    return getSectionIndexAPI(charpter, section)
}

export function getSectionTitle(charpter, section){
    return getSectionTitleAPI(charpter, section)
}

export function getProgramTopicTags(charpter, section){
    return getProgramTopicTagsAPI(charpter, section)
}

export function setNextStage(charpter, section, sampleCodeIndicateIndex, interactiveIndicateIndex){
    return setNextStageAPI(charpter, section, sampleCodeIndicateIndex, interactiveIndicateIndex);
}

export function setPriviousStage(charpter, section, sampleCodeIndicateIndex, interactiveIndicateIndex){
    return setPriviousStageAPI(charpter, section, sampleCodeIndicateIndex, interactiveIndicateIndex);
}