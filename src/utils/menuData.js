export const menuData = [
    {
        charterTitle: 'Charpter1',
        charterLabel: '循序結構',
        sectionList: [
            {
                sectionTitle: '輸入姓名、座號、顯示姓名座號',
                url: 'section_1',
            },
            {
                sectionTitle: '計算紀念書包數量及單價',
                url: 'section_2',
            },
            {
                sectionTitle: '計算梯形面積',
                url: 'section_3',
            },
            {
                sectionTitle: '計算新台幣兌換日圓',
                url: 'section_4',
            },
            {
                sectionTitle: '計算圓面積及圓周長',
                url: 'section_5',
            },
            {
                sectionTitle: '計算 BMI 值',
                url: 'section_6',
            },
            {
                sectionTitle: '呎吋轉換為公分',
                url: 'section_7',
            },
        ]
    },
    {
        charterTitle: 'Charpter2',
        charterLabel: '條件',
        sectionList: [
            {
                sectionTitle: '計算期末的總成績',
                url: 'section_1',
            },
            {
                sectionTitle: '計算百貨公司折扣',
                url: 'section_2',
            },
            {
                sectionTitle: '判別直角三角形',
                url: 'section_3',
            },
            {
                sectionTitle: '測謊機',
                url: 'section_4',
            },
            {
                sectionTitle: '電影售票機',
                url: 'section_5',
            },
        ]
    },
    {
        charterTitle: 'Charpter3',
        charterLabel: '迴圈',
        sectionList: [
            {
                sectionTitle: 'for 迴圈從1印到10',
                url: 'section_1',
            },
            {
                sectionTitle: 'while迴圈算出1~10的 值並列印到螢幕上',
                url: 'section_2',
            },
            {
                sectionTitle: 'for 迴圈輸入n，從1,3,5....印到n，奇數',
                url: 'section_3',
            },
            {
                sectionTitle: 'for 迴圈輸入a,b,c，從a,a+c,a+2*c,....印到b，如3，15,3...即印出3,6 ,9,12,15',
                url: 'section_4',
            },
            {
                sectionTitle: '用For迴圈算出1~N的奇數和並列印到螢幕上',
                url: 'section_5',
            },            
            {
                sectionTitle: 'while迴圈(1)請輸入幾次考試?(2)並計算平均成績。',
                url: 'section_6',
            }
        ]
    }
];