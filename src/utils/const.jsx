export const WEB_TITLE = "運算思維導向程式設計視覺化輔助學習平台";
export const LOGIN_HINT = "請輸入您的電子郵件及密碼登入平臺。";

export const VIISUALIZE_MODE = "程式視覺化";
export const SIMULATION_MODE ="程式模擬"

export const COURCE_TOPIC_COMPONENT_TITLE = "程式碼";
export const COURCE_INTERACT_COMPONENT_TITLE = "變數記憶區";
export const COURCE_INTERACT_AREA_TITLE = "執行結果";

export const PROGRAM_EXPLAIN = "程式解釋";
export const PROGRAMMING_GRAMMAR = "程式語法";