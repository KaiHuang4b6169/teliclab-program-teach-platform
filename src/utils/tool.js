export function isShow(showInTopicStep, showInTeachStep, activateState){
    const isShowAnyway = (showInTopicStep === undefined) || (showInTeachStep === undefined);
    const isOverTopicStep = showInTopicStep < activateState.topicStep;
    const isOverTeachStep = (showInTopicStep === activateState.topicStep) && (showInTeachStep <= activateState.teachStep);
    const isShowContent = isShowAnyway || isOverTopicStep || isOverTeachStep;
    return isShowContent
}

export function isAtState(showInTopicStep, showInTeachStep, activateState){
    return (showInTopicStep === activateState.topicStep) && (showInTeachStep === activateState.teachStep);
}