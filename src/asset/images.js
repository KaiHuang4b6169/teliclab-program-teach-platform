export const RangeArrow = function (props) {
    return (<img src={require('./f_arrow.png')}  {...props}/>)
};

export const RangeAsignArrow = function (props) {
    return (<img src={require('./arrow2.png')}  {...props}/>)
};


export const AsignArrow = function (props) {
    return (<img src={require('./assignArrow.png')}  {...props}/>)
};

export const WhileTrue = function (props) {
    return (<img src={require('./F_True.png')}  {...props}/>)
};


export const WhileFalse = function (props) {
    return (<img src={require('./f_flase.png')}  {...props}/>)
};

export const UpAssign = function (props) {
    return (<img src={require('./a1.png')}  {...props}/>)
};

export const DownAssign = function (props) {
    return (<img src={require('./a2.png')}  {...props}/>)
};

export const WhileGrammar = function (props) {
    return (<img src={require('./while.png')}  {...props}/>)
};