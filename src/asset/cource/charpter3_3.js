import { Row, Stack } from "react-bootstrap";
import { isShow } from "../../utils/tool";
import { DownAssign, UpAssign } from "../images";
import { stageAddTools, stageSubTools } from "./common/commonTools";
import { elementOrigeBackgroundStyle, elementRangeBackgroundStyle, variableMinWidth } from "./common/courceCss";
import { assignProgramComponent, explainProgramComponent, getPrint, getPurpleBlock, inputProgramComponent, textProgramComponent, variableProgramComponent } from "./common/teachingAreaCommon";
import { getExplainOfVariableOfRangeLine, getRangeAsignArrowOfRangeLine, getRangeExplain, getRangeOfRangeLine, getRangeTags, getVariableOfRangeLine, interfaceOfRangeLine } from "./common/teachingAreaLoopCommon";

let nValue = ''
let nInputValue = undefined
let nParseIntValue = undefined
let nAssignValue = undefined

let inputValueAssign = false
let parseIntAssign = false

let rangeVariableOfLoop = undefined;
let rangeVariableOfPrint = undefined;
let loopIndecateIndex = undefined;
let isAssginShow = false
let numberOfPrintInRange = -1;

let isAddStage = undefined
let isSubStage = undefined
let rangeKeyList = []
let rangeTextConfig = {start: 0, end: 0, gap: 0}
let rangeConfig = {start: 1, end: 11, gap: 1}

const numberOfEachStage = [0, 6, 3, 1]

export const courceTitle = (
    <div>
        <p className="my-0">for 迴圈輸入n，從1,3,5....印到n，奇數</p>
        <p className="my-0">(EX:輸入10  輸出1 3 5 7 9)</p>
    </div>
)

export const sampleCode = [
    (<div style={{display: 'flex'}}>
        <span className='text-center' style={{...elementOrigeBackgroundStyle, ...variableMinWidth}}>n</span>
        <span>&nbsp;=&nbsp;int(input("請輸入n: "))</span>
    </div>),
    (<div style={{display: 'flex'}}>
        <span>for&nbsp;</span>
        <span className='text-center' style={{...elementOrigeBackgroundStyle, ...variableMinWidth}}>i</span>
        <span>&nbsp;in&nbsp;</span>
        <div style={elementRangeBackgroundStyle}>
            range(1, 
            <span className='text-center' style={{...elementOrigeBackgroundStyle, ...variableMinWidth}}>n</span>
            +1, 2)
        </div>
    </div>),
    (<p className="my-0"><UpAssign className='px-1' height='20px'/><span style={{background: '#C9C9C9'}}>print(i)</span><DownAssign className='px-1' height='20px'/></p>),
]

export const programExplaiin = 
`程式碼第1行 : 
透過input()函式，取得使用者所輸入的字串，為了使輸入的數字呈數值資料，使用int()函式，從字串型別轉換為整數型別，再將整數 assign(指派) 給變數 n

程式碼第2-3行：
for迴圈可以重複相同的指令，直到執行完設定的次數，而range()函式可以很有效率地創建一個整數序列，用法為 (起始值, 結束值, 遞增/減值)，range(1, n+1, 2)：產生從1到n之間的奇數的整數序列(遞增值為2)。`;

export function nextStage(sampleCodeIndicateIndex, interactiveIndicateIndex){
    isAddStage = true
    isSubStage = false
    if (numberOfPrintInRange >= 3 || numberOfPrintInRange === rangeKeyList.length) {
        numberOfPrintInRange = rangeKeyList.length 
        return {sampleCodeIndicateIndex, interactiveIndicateIndex}
    }
    let {margeStage, minorSage} = stageAddTools(sampleCodeIndicateIndex, interactiveIndicateIndex, numberOfEachStage)
    margeStage = (margeStage > numberOfEachStage.length - 1) ? 2 : margeStage

    return {
        sampleCodeIndicateIndex : margeStage,
        interactiveIndicateIndex: minorSage
    };
}

export function priviousStage(sampleCodeIndicateIndex, interactiveIndicateIndex){
    isAddStage = false
    isSubStage = true
    if (numberOfPrintInRange > 3) {
        numberOfPrintInRange = 3
        return {sampleCodeIndicateIndex, interactiveIndicateIndex}
    }
    const isMinStage = (sampleCodeIndicateIndex === 2) && (interactiveIndicateIndex === 0)
    let {margeStage, minorSage} = stageSubTools(sampleCodeIndicateIndex, interactiveIndicateIndex, numberOfEachStage)
    if (loopIndecateIndex !== undefined && isMinStage) {
        margeStage = numberOfEachStage.length - 1
        minorSage = numberOfEachStage[numberOfEachStage.length - 1]
    }

    return {
        sampleCodeIndicateIndex : margeStage,
        interactiveIndicateIndex: minorSage
    };
}

export function courceTeachingArea(sampleCodeIndicateIndex, interactiveIndicateIndex){
    if (sampleCodeIndicateIndex === 0) setMajorStageOfZero(interactiveIndicateIndex)
    if (sampleCodeIndicateIndex === 1) setMajorStageOfOne(interactiveIndicateIndex)
    if (sampleCodeIndicateIndex === 2) setMajorStageOfTwo(interactiveIndicateIndex)
    if (sampleCodeIndicateIndex === 3) setMajorStageOfThree(interactiveIndicateIndex)

    const assignTeachArea = (
        <div>
            <Stack className="my-1 justify-content-evenly" direction="horizontal">
                {explainProgramComponent('變數名稱', '變數值')}
                {variableProgramComponent('n', nAssignValue)}
                {assignProgramComponent(parseIntAssign)}
                {variableProgramComponent('int()', nParseIntValue)}
                {assignProgramComponent(inputValueAssign)}
                {inputProgramComponent('input("請輸入n: ")', nInputValue)}
                {explainProgramComponent('函數', '函數回傳值')}
            </Stack>
        </div>    
    )

    const loopTeachingArea = (
        <div>
            <Stack className="my-1" direction="horizontal">          
                {getPurpleBlock(
                    [
                        textProgramComponent('range( ' + rangeTextConfig.start + ', '),
                        variableProgramComponent('i', nValue),
                        textProgramComponent('+ 1 , ' + rangeTextConfig.gap + ' )'),
                    ], {},'ms-auto' 
                )}
                {getRangeExplain(['迴圈變數', '範圍設定'])}
            </Stack>

            <Stack direction="horizontal">
                {getExplainOfVariableOfRangeLine()}
                {getVariableOfRangeLine('i', rangeVariableOfLoop)}
                {getRangeAsignArrowOfRangeLine(isAssginShow)}
                {getRangeOfRangeLine(rangeConfig.start, rangeConfig.end , rangeConfig.gap, loopIndecateIndex)}
                {interfaceOfRangeLine(undefined, getRangeExplain(['','迴圈變數', 'list']), undefined)}
            </Stack>

            <Stack className="justify-content-center" direction="horizontal">
                {getPrint(
                    [
                        textProgramComponent('print('),
                        variableProgramComponent('i', rangeVariableOfPrint),
                        textProgramComponent(')'),
                    ]
                )}
            </Stack>
        </div>        
    );
    
    return (sampleCodeIndicateIndex < 2)  ? assignTeachArea : loopTeachingArea ;
}

function setMajorStageOfZero(){ 
    rangeConfig = {start: 0, end: 0, gap: 0}
    rangeTextConfig = {start: 0, end: 0, gap: 0}

    nValue = ''
    nInputValue = undefined
    nParseIntValue = undefined
    nAssignValue = undefined

    inputValueAssign = false
    parseIntAssign = false

    rangeVariableOfLoop = undefined;
    rangeVariableOfPrint = undefined;
    loopIndecateIndex = undefined;
    isAssginShow = false
    numberOfPrintInRange = -1;
}

function setMajorStageOfOne(interactiveIndicateIndex){ 
    if (isAddStage){
        if (interactiveIndicateIndex === 2)
            nInputValue = '"' + nValue + '"'
        if (interactiveIndicateIndex === 3)
            inputValueAssign = true
        if (interactiveIndicateIndex === 4)
            nParseIntValue = nValue
        if (interactiveIndicateIndex === 5)
            parseIntAssign = true
        if (interactiveIndicateIndex === 6)
            nAssignValue = nParseIntValue
    }

    if (isSubStage) {
        if (interactiveIndicateIndex === 1)
            nInputValue = undefined
        if (interactiveIndicateIndex === 2)
            inputValueAssign = false
        if (interactiveIndicateIndex === 3)
            nParseIntValue = undefined
        if (interactiveIndicateIndex === 4)
            parseIntAssign = false
        if (interactiveIndicateIndex === 5)
            nAssignValue = undefined
        if (interactiveIndicateIndex === 6){            
            numberOfPrintInRange = -1
            rangeTextConfig = {start: 0, end: 0, gap: 0}
            rangeKeyList = []
        }
    }
}

function setMajorStageOfTwo(interactiveIndicateIndex){ 
    if (isAddStage){
        if (interactiveIndicateIndex === 0){
            rangeTextConfig = {start: 1, end: Number.parseInt(nValue), gap: 2}
            isAssginShow = false
            rangeVariableOfLoop = undefined
            rangeVariableOfPrint = undefined
            numberOfPrintInRange = (numberOfPrintInRange === -1 ) ?  0 : numberOfPrintInRange
            rangeKeyList = []
            for(let i = rangeTextConfig.start; i < rangeTextConfig.end + 1; i += rangeTextConfig.gap){
                rangeKeyList.push(i)
            }
                
        }
        if (interactiveIndicateIndex === 1){
            rangeConfig = rangeTextConfig
            rangeConfig.end += 1
            loopIndecateIndex = (loopIndecateIndex === undefined) ? 0 : loopIndecateIndex + 1
        }
        if (interactiveIndicateIndex === 2)
            isAssginShow = true
        if (interactiveIndicateIndex === 3)
            rangeVariableOfLoop = rangeKeyList[loopIndecateIndex]
    }

    if (isSubStage) {
        if (interactiveIndicateIndex === 0){
            loopIndecateIndex = (loopIndecateIndex === 0) ? undefined : loopIndecateIndex - 1
            if (loopIndecateIndex === undefined) rangeConfig = {start: 0, end: 0, gap: 0}
        }
        if (interactiveIndicateIndex === 1)
            isAssginShow = false
        if (interactiveIndicateIndex === 2)
            rangeVariableOfLoop = undefined
    }
}

function setMajorStageOfThree(interactiveIndicateIndex){   
    if (isAddStage){
        if (interactiveIndicateIndex === 1){
            rangeVariableOfPrint = rangeVariableOfLoop
            if (numberOfPrintInRange < rangeKeyList.length) numberOfPrintInRange += 1
        }
    }
    if (isSubStage){
        if (interactiveIndicateIndex === 0){
            numberOfPrintInRange = rangeKeyList.indexOf(rangeVariableOfPrint)
            rangeVariableOfPrint = undefined
        }
        if (interactiveIndicateIndex === 1){
            rangeVariableOfPrint = rangeKeyList[numberOfPrintInRange - 1]
            rangeVariableOfLoop = rangeVariableOfPrint
            isAssginShow = true
        }
    }
}

export function getPrintResult(){
    let printResult = []
    rangeKeyList.map((element, key) => {
        if (numberOfPrintInRange > key ) printResult.push(<Row style={{height: '40px'}}><label className="py-1">{element}</label></Row>)
    })
    return printResult;
}

export function getProgramGrammar(){
    return (
        <div>
            <div style={{display: 'flex'}}>
                <span>for&nbsp;</span>
                <span className='text-center' style={{...elementOrigeBackgroundStyle, ...variableMinWidth}}>&nbsp;變數&nbsp;</span>
                <span>&nbsp;in&nbsp;</span>
                <div style={elementRangeBackgroundStyle}>&nbsp;range(起始直, 結束值, 遞增/遞減)&nbsp;</div>
            </div>
            <p className="my-0"><UpAssign className='px-1' height='20px'/><span style={{background: '#C9C9C9'}}>重複執行的指令</span><DownAssign className='px-1' height='20px'/></p>
        </div>
    )
}

export function getInputObject(sampleCodeIndicateIndex, interactiveIndicateIndex){
    return [
        {
            isShow: sampleCodeIndicateIndex > 1 || (sampleCodeIndicateIndex === 1 && interactiveIndicateIndex > 0),
            label: '請輸入n: ',
            defaultValue: 10,
            value: nValue,
            isActive: (sampleCodeIndicateIndex === 1 && interactiveIndicateIndex === 1),
            onChange(value) {nValue = value},
            isValid(inputString) {
                const inputValue = Number.parseInt(inputString)
                if (isEmpty(inputString)) return {valid:false, errorMsg:'輸入欄位不能為空'};
                if (isNaN(inputValue)) return {valid:false, errorMsg:'請輸入數字'};
                if (inputValue < 0) return {valid:false, errorMsg:'輸入數字必須大於零'};
                return {valid:true, errorMsg:''}
            }
        },
    ]
}

function isEmpty(valid){
    return (valid === undefined) || (valid === '');
}
