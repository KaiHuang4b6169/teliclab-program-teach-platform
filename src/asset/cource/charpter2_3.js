export const courceTitle = `
    <p className="my-0">就讀國小的寧寧最近在學三角形，他發現當三角形的三個邊長一種特別關係的話，就可以畫成直角三角形。</p>
    <p className="my-0">因此寧寧希望你幫忙寫一個程式，讓寧寧依序輸入最短邊長、次短邊長及最長邊長，如果這三邊可以畫成直角三角形，請在螢幕印出 YES，否則請印出 NO。</p>
    <p className="my-0">( EX: 輸入- 最短邊長：3    次短邊長: 4    最長邊長:5      輸出-YES)</p>
`

export const programmingGrammar = `
    <p className="my-0"><span style="background:#AAD8C1;">if 條件運算值 :</span></p>
    <p className="my-0"><span style="background:#AAD8C1; white-space:pre;">  </span>&nbsp;條件"成立"時所執行的指令</p>
    <p className="my-0"><span style="background:#AAD8C1;">else:</span></p>
    <p className="my-0"><span style="background:#AAD8C1; white-space:pre;">  </span>&nbsp;條件"不成立"時所執行的指令</p>
`

export const hintDialogContent = `
    <p>程式碼第1-3行：</p>
    <p>透過input()函式，取得使用者所輸入的字串，為了使輸入的邊長都呈數值資料，使用int()函式，從字串型別轉換為整數型別，再 assign(指派) 給變數 a、b 和 c</p>

    <p>程式碼第4-7行：</p>
    <p>運用if…else條件判斷語法，如果輸入的邊長符合畢氏定理，代表表三邊可以畫</p>
    <p>成直角三角形，反之不符合定理。</p>
`;

export const courceIndex = '單元範例-例題3'

export const sampleCode = [
    `<p className="my-0"><span style="background: peachpuff;">h</span>= int(input("請輸入三角形最短邊長："))</p>`,
    `<p className="my-0"><span style="background: peachpuff;">t</span>= int(input("請輸入三角形次短邊長："))</p>`,
    `<p className="my-0"><span style="background: peachpuff;">f</span>= int(input("請輸入三角形最長邊長："))</p>`,
    `<p className="my-0"><span style="background:#AAD8C1;">if (a *a + b *b== c *c):</p>`,
    `<p className="my-0"><span style="background:#AAD8C1; white-space:pre;">  </span>&nbsp;<span style="background: LightSkyBlue;">print("YES")</span></p>`,
    `<p className="my-0"><span style="background:#AAD8C1;">else:</span></p>`,
    `<p className="my-0"><span style="background:#AAD8C1; white-space:pre;">  </span>&nbsp;<span style="background: LightSkyBlue;">print("NO")</span></p>`

]

export const defaultCourceData = [
    {
        type: 'codeExplain',
        data: [
            {type: 'hint', lable:'變數名稱', content:'變數值'},
            {type: 'variable', lable:'a', content:'3', showInTopicStep: 1, showInTeachStep: 5},
            {type: 'assign', showInTopicStep: 1, showInTeachStep: 4},
            {type: 'input', lable:'int()', content:'3', showInTopicStep: 1, showInTeachStep: 3},
            {type: 'assign', showInTopicStep: 1, showInTeachStep: 2},
            {type: 'input', lable:'input("請輸入三角形最短邊長：")', content:'"3"', showInTopicStep: 1, showInTeachStep: 1},
            {type: 'hint', lable:'函式', content:'函數傳回值'},
        ],
    },
    {
        type: 'codeExplain',
        data: [
            {type: 'hint'},
            {type: 'variable', lable:'b', content:'4', showInTopicStep: 2, showInTeachStep: 5},
            {type: 'assign', showInTopicStep: 2, showInTeachStep: 4},
            {type: 'input', lable:'int()', content:'4', showInTopicStep: 2, showInTeachStep: 3},
            {type: 'assign', showInTopicStep: 2, showInTeachStep: 2},
            {type: 'input', lable:'input("請輸入三角形次短邊長：")', content:'"4"', showInTopicStep: 2, showInTeachStep: 1},
            {type: 'hint'},
        ],
    },
    {
        type: 'codeExplain',
        data: [
            {type: 'hint'},
            {type: 'variable', lable:'c', content:'5', showInTopicStep: 3, showInTeachStep: 5},
            {type: 'assign', showInTopicStep: 3, showInTeachStep: 4},
            {type: 'input', lable:'int()', content:'5', showInTopicStep: 3, showInTeachStep: 3},
            {type: 'assign', showInTopicStep: 3, showInTeachStep: 2},
            {type: 'input', lable:'input("請輸入三角形最長邊長：")', content:'"5"', showInTopicStep: 3, showInTeachStep: 1},
            {type: 'hint'},
        ],
    },
]

export function conditionCourceData(teachData) {
    const a = teachData[0].data[1].content;
    const b = teachData[1].data[1].content;
    const c = teachData[2].data[1].content;
    return [
        {
            type: 'condition',
            content: {
                type: 'conditionStatementBlock',
                content:{
                    title: 'if條件運算值',
                    data: [
                        {type: 'variable', lable:'a', content: a, showInTopicStep: 1, showInTeachStep: 0},
                        {type: 'text', lable:'', content:' * '},
                        {type: 'variable', lable:'a', content: a, showInTopicStep: 1, showInTeachStep: 0},
                        {type: 'text', lable:'', content:' * '},
                        {type: 'variable', lable:'b', content: b, showInTopicStep: 1, showInTeachStep: 0},
                        {type: 'text', lable:'', content:' * '},
                        {type: 'variable', lable:'b', content: b, showInTopicStep: 1, showInTeachStep: 0},
                        {type: 'text', lable:'', content:' == '},
                        {type: 'variable', lable:'c', content: c, showInTopicStep: 1, showInTeachStep: 0},
                        {type: 'text', lable:'', content:' * '},
                        {type: 'variable', lable:'c', content: c, showInTopicStep: 1, showInTeachStep: 0},
                    ],
                }
            }
        },
        {
            type: 'true',
            content: {
                type: 'printBlock',
                content: {
                    title: '條件 "成立" 時\n所執行的指令',
                    data: [
                        {type: 'text', lable:'', content:'print("YES")'},
                    ],
                }
            }
        },
        {
            type: 'false',
            content: {
                type: 'printBlock',
                content: {
                    title: '條件 "不成立" 時\n所執行的指令',
                    data: [
                        {type: 'text', lable:'', content:'print("NO")'},
                    ],
                }
            }
        },
    ]
}

export const stateObject = {
    topicStep: 0,
    teachStep: 0,
    teachStepOfTopic: [0, 5, 5, 5, 1, 1],
    teachAreatype: [
        {type: "normal", topicStep: 0},
        {type: "condition", topicStep: 4}
    ]
}

export const inputObject = [
    {
        label: '請輸入三角形最短邊長：',
        content: '80', 
        invalidMsg: '輸入欄位不能為空',
        showInTopicStep: 1, 
        showInTeachStep: 0, 
        setData(courceData, value) { return setSideA(courceData, value)}, 
        isValid(value){ 
            if (isEmpty(value)) return {isValid:false, errorMsg:'輸入欄位不能為空'};
            if (isNaN(value)) return {isValid:false, errorMsg:'請輸入數字'};
            return {isValid:true, errorMsg:''}
        }
    },
    {
        label: '請輸入三角形次短邊長：',
        content: '70', 
        invalidMsg: '輸入欄位不能為空',
        showInTopicStep: 2, 
        showInTeachStep: 0, 
        setData(courceData, value) { return setSideB(courceData, value)}, 
        isValid(value){ 
            if (isEmpty(value)) return {isValid:false, errorMsg:'輸入欄位不能為空'};
            if (isNaN(value)) return {isValid:false, errorMsg:'請輸入數字'};
            return {isValid:true, errorMsg:''}
        }
    },
    {
        label: '請輸入三角形最長邊長：',
        content: '64', 
        invalidMsg: '輸入欄位不能為空',
        showInTopicStep: 3, 
        showInTeachStep: 0, 
        setData(courceData, value) { return setSideC(courceData, value)}, 
        isValid(value){ 
            if (isEmpty(value)) return {isValid:false, errorMsg:'輸入欄位不能為空'};
            if (isNaN(value)) return {isValid:false, errorMsg:'請輸入數字'};
            return {isValid:true, errorMsg:''}
        }
    },
]

function setSideA(courceData, value){    
    courceData[0].data[1].content = Number.parseInt(value);
    courceData[0].data[3].content = Number.parseInt(value);
    courceData[0].data[5].content = '"' + value + '"';
    return courceData;
}

function setSideB(courceData, value){    
    courceData[1].data[1].content = Number.parseInt(value);
    courceData[1].data[3].content = Number.parseInt(value);
    courceData[1].data[5].content = '"' + value + '"';
    return courceData;
}

function setSideC(courceData, value){    
    courceData[2].data[1].content = Number.parseInt(value);
    courceData[2].data[3].content = Number.parseInt(value);
    courceData[2].data[5].content = '"' + value + '"';
    return courceData;
}

function isEmpty(valid){
    return (valid === undefined) || (valid === '');
}

export function getResult(courceData){
    return [
        {
            showInTopicStep: 5,
            showInTeachStep: 1, 
            print: isTriangle(courceData) ? 'YES' : 'NO'
        }
    ];
}

export function isShowSampleCodeArrow(activateStep, index, courceData){
    if (activateStep < 5)
        return activateStep === index+1
    else if (activateStep === 5)
        return isTriangle(courceData) ? index === 4 : index === 6
    else
        return false
}

export function graphCondition(courceData){
    return {showArrow: {showInTopicStep: 4, showInTeachStep: 1}, changeLink: {showInTopicStep: 5, showInTeachStep: 1}, isStatify: isTriangle(courceData)}
}

function isTriangle(courceData){    
    const a = Number.parseInt(courceData[0].data[1].content);
    const b = Number.parseInt(courceData[1].data[1].content);
    const c = Number.parseInt(courceData[2].data[1].content);

    return a*a + b*b === c*c;
}