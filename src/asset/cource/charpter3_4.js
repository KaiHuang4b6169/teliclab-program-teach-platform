import { Row, Stack } from "react-bootstrap";
import { isShow } from "../../utils/tool";
import { DownAssign, UpAssign } from "../images";
import { stageAddTools, stageSubTools } from "./common/commonTools";
import { elementOrigeBackgroundStyle, elementRangeBackgroundStyle, variableMinWidth } from "./common/courceCss";
import { assignProgramComponent, explainProgramComponent, getPrint, getPurpleBlock, inputProgramComponent, textProgramComponent, variableProgramComponent } from "./common/teachingAreaCommon";
import { getExplainOfVariableOfRangeLine, getRangeAsignArrowOfRangeLine, getRangeExplain, getRangeOfRangeLine, getRangeTags, getVariableOfRangeLine, interfaceOfRangeLine } from "./common/teachingAreaLoopCommon";

let aValue = [undefined, undefined, undefined, undefined]
let aAssign = [false, false]
let bValue = [undefined, undefined, undefined, undefined]
let bAssign = [false, false]
let cValue = [undefined, undefined, undefined, undefined]
let cAssign = [false, false]

let inputValueAssign = false
let parseIntAssign = false

let rangeVariableOfLoop = undefined;
let rangeVariableOfPrint = undefined;
let loopIndecateIndex = undefined;
let isAssginShow = false
let numberOfPrintInRange = -1;

let isAddStage = undefined
let isSubStage = undefined
let rangeKeyList = []
let rangeTextConfig = {start: 0, end: 0, gap: 0}
let rangeConfig = {start: 1, end: 11, gap: 1}

const numberOfEachStage = [0, 6, 6, 6, 3, 1]

export const courceTitle = (
    <div>
        <p className="my-0">for 迴圈輸入a,b,c，從a,a+c,a+2*c,....印到b，如3，15,3...即印出3,6 ,9,12,15</p>
    </div>
)

export const sampleCode = [
    (<div className="py-1" style={{display: 'flex'}}>
        <span className='text-center' style={{...elementOrigeBackgroundStyle, ...variableMinWidth}}>a</span>
        <span>&nbsp;=&nbsp;int(input("請輸入a: "))</span>
    </div>),
    (<div className="py-1" style={{display: 'flex'}}>
        <span className='text-center' style={{...elementOrigeBackgroundStyle, ...variableMinWidth}}>b</span>
        <span>&nbsp;=&nbsp;int(input("請輸入b: "))</span>
    </div>),
    (<div className="py-1" style={{display: 'flex'}}>
        <span className='text-center' style={{...elementOrigeBackgroundStyle, ...variableMinWidth}}>c</span>
        <span>&nbsp;=&nbsp;int(input("請輸入c: "))</span>
    </div>),
    (<div className="py-1" style={{display: 'flex'}}>
        <span>for&nbsp;</span>
        <span className='text-center' style={{...elementOrigeBackgroundStyle, ...variableMinWidth}}>i</span>
        <span>&nbsp;in&nbsp;</span>
        <div style={elementRangeBackgroundStyle}>
            range(
            <span className='text-center' style={{...elementOrigeBackgroundStyle, ...variableMinWidth}}>a</span>&nbsp;,
            <span className='text-center' style={{...elementOrigeBackgroundStyle, ...variableMinWidth}}>b</span>&nbsp;+&nbsp;1&nbsp;, 
            <span className='text-center' style={{...elementOrigeBackgroundStyle, ...variableMinWidth}}>c</span>&nbsp;
            )
        </div>
    </div>),
    (<p className="my-0"><UpAssign className='px-1' height='20px'/><span style={{background: '#C9C9C9'}}>print(i)</span><DownAssign className='px-1' height='20px'/></p>),
]

export const programExplaiin = 
`程式碼第1-3行 : 
透過input()函式，取得使用者所輸入的字串，為了使輸入的數字呈數值資料，使用int()函式，從字串型別轉換為整數型別，再將整數 assign(指派) 給變數  a , b 和 c

程式碼第4-5行：
for迴圈可以重複相同的指令，直到執行完設定的次數，而range()函式可以很有效率地創建一個整數序列，用法為 (起始值, 結束值, 遞增/減值)，range(a, b+1, c)：產生a,a+c,a+2*c,....到b之間的整數序列(遞增/減值為c)。

6.用For迴圈算出1~N的和並列印到螢幕上
`;

export function nextStage(sampleCodeIndicateIndex, interactiveIndicateIndex){
    isAddStage = true
    isSubStage = false
    if (numberOfPrintInRange >= 3 || numberOfPrintInRange === rangeKeyList.length) {
        numberOfPrintInRange = rangeKeyList.length 
        return {sampleCodeIndicateIndex, interactiveIndicateIndex}
    }
    let {margeStage, minorSage} = stageAddTools(sampleCodeIndicateIndex, interactiveIndicateIndex, numberOfEachStage)
    margeStage = (margeStage > numberOfEachStage.length - 1) ? 4 : margeStage

    return {
        sampleCodeIndicateIndex : margeStage,
        interactiveIndicateIndex: minorSage
    };
}

export function priviousStage(sampleCodeIndicateIndex, interactiveIndicateIndex){
    isAddStage = false
    isSubStage = true
    if (numberOfPrintInRange > 3) {
        numberOfPrintInRange = 3
        return {sampleCodeIndicateIndex, interactiveIndicateIndex}
    }
    const isMinStage = (sampleCodeIndicateIndex === 4) && (interactiveIndicateIndex === 0)
    let {margeStage, minorSage} = stageSubTools(sampleCodeIndicateIndex, interactiveIndicateIndex, numberOfEachStage)
    if (loopIndecateIndex !== undefined && isMinStage) {
        margeStage = numberOfEachStage.length - 1
        minorSage = numberOfEachStage[numberOfEachStage.length - 1]
    }

    return {
        sampleCodeIndicateIndex : margeStage,
        interactiveIndicateIndex: minorSage
    };
}

export function courceTeachingArea(sampleCodeIndicateIndex, interactiveIndicateIndex){
    if (sampleCodeIndicateIndex === 0) setMajorStageOfZero(interactiveIndicateIndex)
    if (sampleCodeIndicateIndex === 1) setMajorStageOfOne(interactiveIndicateIndex)
    if (sampleCodeIndicateIndex === 2) setMajorStageOfTwo(interactiveIndicateIndex)
    if (sampleCodeIndicateIndex === 3) setMajorStageOfThree(interactiveIndicateIndex)
    if (sampleCodeIndicateIndex === 4) setMajorStageOfFour(interactiveIndicateIndex)
    if (sampleCodeIndicateIndex === 5) setMajorStageOfFive(interactiveIndicateIndex)

    const assignTeachArea = (
        <div>
            <Stack className="my-1 justify-content-evenly" direction="horizontal">
                {explainProgramComponent('變數名稱', '變數值')}
                {variableProgramComponent('a', aValue[3])}
                {assignProgramComponent(aAssign[1])}
                {variableProgramComponent('int()', aValue[2])}
                {assignProgramComponent(aAssign[0])}
                {inputProgramComponent('input("請輸入a: ")', aValue[1])}
                {explainProgramComponent('函數', '函數回傳值')}
            </Stack>
            <Stack className="my-1 justify-content-evenly" direction="horizontal">
                {explainProgramComponent('變數名稱', '變數值')}
                {variableProgramComponent('b', bValue[3])}
                {assignProgramComponent(bAssign[1])}
                {variableProgramComponent('int()', bValue[2])}
                {assignProgramComponent(bAssign[0])}
                {inputProgramComponent('input("請輸入b: ")', bValue[1])}
                {explainProgramComponent('函數', '函數回傳值')}
            </Stack>
            <Stack className="my-1 justify-content-evenly" direction="horizontal">
                {explainProgramComponent('變數名稱', '變數值')}
                {variableProgramComponent('c', cValue[3])}
                {assignProgramComponent(cAssign[1])}
                {variableProgramComponent('int()', cValue[2])}
                {assignProgramComponent(cAssign[0])}
                {inputProgramComponent('input("請輸入c: ")', cValue[1])}
                {explainProgramComponent('函數', '函數回傳值')}
            </Stack>
        </div>    
    )

    const loopTeachingArea = (
        <div>
            <Stack className="my-1" direction="horizontal">          
                {getPurpleBlock(
                    [
                        textProgramComponent('range( '),
                        variableProgramComponent('a', rangeTextConfig.start),
                        textProgramComponent(', '),
                        variableProgramComponent('b', rangeTextConfig.end),
                        textProgramComponent(' + 1, '),
                        variableProgramComponent('c', rangeTextConfig.gap),
                        textProgramComponent(' )')
                    ], {},'ms-auto' 
                )}
                {getRangeExplain(['迴圈變數', '範圍設定'])}
            </Stack>

            <Stack direction="horizontal">
                {getExplainOfVariableOfRangeLine()}
                {getVariableOfRangeLine('i', rangeVariableOfLoop)}
                {getRangeAsignArrowOfRangeLine(isAssginShow)}
                {getRangeOfRangeLine(rangeConfig.start, rangeConfig.end , rangeConfig.gap, loopIndecateIndex)}
                {interfaceOfRangeLine(undefined, getRangeExplain(['','迴圈變數', 'list']), undefined)}
            </Stack>

            <Stack className="justify-content-center" direction="horizontal">
                {getPrint(
                    [
                        textProgramComponent('print('),
                        variableProgramComponent('i', rangeVariableOfPrint),
                        textProgramComponent(')'),
                    ]
                )}
            </Stack>
        </div>        
    );
    
    return (sampleCodeIndicateIndex < 4)  ? assignTeachArea : loopTeachingArea ;
}

function setMajorStageOfZero(){ 
    rangeConfig = {start: 0, end: 0, gap: 0}
    rangeTextConfig = {start: 0, end: 0, gap: 0}

    aValue = [undefined, undefined, undefined, undefined]
    aAssign = [false, false]
    bValue = [undefined, undefined, undefined, undefined]
    bAssign = [false, false]
    cValue = [undefined, undefined, undefined, undefined]
    cAssign = [false, false]

    inputValueAssign = false
    parseIntAssign = false

    rangeVariableOfLoop = undefined;
    rangeVariableOfPrint = undefined;
    loopIndecateIndex = undefined;
    isAssginShow = false
    numberOfPrintInRange = -1;
}

function setMajorStageOfOne(interactiveIndicateIndex){ 
    if (isAddStage){
        if (interactiveIndicateIndex === 2)
            aValue[1] = '"' + aValue[0] + '"'
        if (interactiveIndicateIndex === 3)
            aAssign[0] = true
        if (interactiveIndicateIndex === 4)
            aValue[2] = aValue[0]
        if (interactiveIndicateIndex === 5)
            aAssign[1] = true
        if (interactiveIndicateIndex === 6)
            aValue[3] = aValue[0]
    }

    if (isSubStage) {
        if (interactiveIndicateIndex === 1)
            aValue[1] = undefined
        if (interactiveIndicateIndex === 2)
        aAssign[0] = false
        if (interactiveIndicateIndex === 3)
            aValue[2] = undefined
        if (interactiveIndicateIndex === 4)
            aAssign[1] = false
        if (interactiveIndicateIndex === 5)
            aValue[3] = undefined
        // if (interactiveIndicateIndex === 6){            
        //     numberOfPrintInRange = -1
        //     rangeTextConfig = {start: 0, end: 0, gap: 0}
        //     rangeKeyList = []
        // }
    }
}

function setMajorStageOfTwo(interactiveIndicateIndex){ 
    if (isAddStage){
        if (interactiveIndicateIndex === 2)
            bValue[1] = '"' + bValue[0] + '"'
        if (interactiveIndicateIndex === 3)
            bAssign[0] = true
        if (interactiveIndicateIndex === 4)
            bValue[2] = bValue[0]
        if (interactiveIndicateIndex === 5)
            bAssign[1] = true
        if (interactiveIndicateIndex === 6)
            bValue[3] = bValue[0]
    }

    if (isSubStage) {
        if (interactiveIndicateIndex === 1)
            bValue[1] = undefined
        if (interactiveIndicateIndex === 2)
            bAssign[0] = false
        if (interactiveIndicateIndex === 3)
            bValue[2] = undefined
        if (interactiveIndicateIndex === 4)
            bAssign[1] = false
        if (interactiveIndicateIndex === 5)
            bValue[3] = undefined
        // if (interactiveIndicateIndex === 6){            
        //     numberOfPrintInRange = -1
        //     rangeTextConfig = {start: 0, end: 0, gap: 0}
        //     rangeKeyList = []
        // }
    }
}

function setMajorStageOfThree(interactiveIndicateIndex){ 
    if (isAddStage){
        if (interactiveIndicateIndex === 2)
            cValue[1] = '"' + cValue[0] + '"'
        if (interactiveIndicateIndex === 3)
            cAssign[0] = true
        if (interactiveIndicateIndex === 4)
            cValue[2] = cValue[0]
        if (interactiveIndicateIndex === 5)
            cAssign[1] = true
        if (interactiveIndicateIndex === 6)
            cValue[3] = cValue[0]
    }

    if (isSubStage) {
        if (interactiveIndicateIndex === 1)
            cValue[1] = undefined
        if (interactiveIndicateIndex === 2)
            cAssign[0] = false
        if (interactiveIndicateIndex === 3)
            cValue[2] = undefined
        if (interactiveIndicateIndex === 4)
            cAssign[1] = false
        if (interactiveIndicateIndex === 5)
            cValue[3] = undefined
        if (interactiveIndicateIndex === 6){            
            numberOfPrintInRange = -1
            rangeTextConfig = {start: 0, end: 0, gap: 0}
            rangeKeyList = []
        }
    }
}

function setMajorStageOfFour(interactiveIndicateIndex){ 
    if (isAddStage){
        if (interactiveIndicateIndex === 0){
            rangeTextConfig = {start: Number.parseInt(aValue[0]), end: Number.parseInt(bValue[0]), gap: Number.parseInt(cValue[0])}
            isAssginShow = false
            rangeVariableOfLoop = undefined
            rangeVariableOfPrint = undefined
            numberOfPrintInRange = (numberOfPrintInRange === -1 ) ?  0 : numberOfPrintInRange
            rangeKeyList = []
            for(let i = Number.parseInt(rangeTextConfig.start); i < Number.parseInt(rangeTextConfig.end)+ 1; i +=  Number.parseInt(rangeTextConfig.gap)){
                rangeKeyList.push(i)
            }
                
        }
        if (interactiveIndicateIndex === 1){
            rangeConfig = {...rangeTextConfig}
            rangeConfig.end += 1
            loopIndecateIndex = (loopIndecateIndex === undefined) ? 0 : loopIndecateIndex + 1
        }
        if (interactiveIndicateIndex === 2)
            isAssginShow = true
        if (interactiveIndicateIndex === 3)
            rangeVariableOfLoop = rangeKeyList[loopIndecateIndex]
    }

    if (isSubStage) {
        if (interactiveIndicateIndex === 0){
            loopIndecateIndex = (loopIndecateIndex === 0) ? undefined : loopIndecateIndex - 1
            if (loopIndecateIndex === undefined) rangeConfig = {start: 0, end: 0, gap: 0}
        }
        if (interactiveIndicateIndex === 1)
            isAssginShow = false
        if (interactiveIndicateIndex === 2)
            rangeVariableOfLoop = undefined
    }
}

function setMajorStageOfFive(interactiveIndicateIndex){   
    if (isAddStage){
        if (interactiveIndicateIndex === 1){
            rangeVariableOfPrint = rangeVariableOfLoop
            if (numberOfPrintInRange < rangeKeyList.length) numberOfPrintInRange += 1
        }
    }
    if (isSubStage){
        if (interactiveIndicateIndex === 0){
            numberOfPrintInRange = rangeKeyList.indexOf(rangeVariableOfPrint)
            rangeVariableOfPrint = undefined
        }
        if (interactiveIndicateIndex === 1){
            rangeVariableOfPrint = rangeKeyList[numberOfPrintInRange - 1]
            rangeVariableOfLoop = rangeVariableOfPrint
            isAssginShow = true
        }
    }
}

export function getPrintResult(){
    let printResult = []
    rangeKeyList.map((element, key) => {
        if (numberOfPrintInRange > key ) printResult.push(<Row style={{height: '40px'}}><label className="py-1">{element}</label></Row>)
    })
    return printResult;
}

export function getProgramGrammar(){
    return (
        <div>
            <div style={{display: 'flex'}}>
                <span>for&nbsp;</span>
                <span className='text-center' style={{...elementOrigeBackgroundStyle, ...variableMinWidth}}>&nbsp;變數&nbsp;</span>
                <span>&nbsp;in&nbsp;</span>
                <div style={elementRangeBackgroundStyle}>&nbsp;range(起始直, 結束值, 遞增/遞減)&nbsp;</div>
            </div>
            <p className="my-0"><UpAssign className='px-1' height='20px'/><span style={{background: '#C9C9C9'}}>重複執行的指令</span><DownAssign className='px-1' height='20px'/></p>
        </div>
    )
}

export function getInputObject(sampleCodeIndicateIndex, interactiveIndicateIndex){
    return [
        {
            isShow: sampleCodeIndicateIndex > 1 || (sampleCodeIndicateIndex === 1 && interactiveIndicateIndex > 0),
            label: '請輸入a: ',
            defaultValue: 3,
            value: aValue[0],
            isActive: (sampleCodeIndicateIndex === 1 && interactiveIndicateIndex === 1),
            onChange(value) {aValue[0] = value},
            isValid(inputString) {
                const inputValue = Number.parseInt(inputString)
                if (isEmpty(inputString)) return {valid:false, errorMsg:'輸入欄位不能為空'};
                if (isNaN(inputValue)) return {valid:false, errorMsg:'請輸入數字'};
                if (inputValue < 0) return {valid:false, errorMsg:'輸入數字必須大於零'};
                return {valid:true, errorMsg:''}
            }
        },
        {
            isShow: sampleCodeIndicateIndex > 2 || (sampleCodeIndicateIndex === 2 && interactiveIndicateIndex > 0),
            label: '請輸入b: ',
            defaultValue: 15,
            value: bValue[0],
            isActive: (sampleCodeIndicateIndex === 2 && interactiveIndicateIndex === 1),
            onChange(value) {bValue[0] = value},
            isValid(inputString) {
                const inputValue = Number.parseInt(inputString)
                if (isEmpty(inputString)) return {valid:false, errorMsg:'輸入欄位不能為空'};
                if (isNaN(inputValue)) return {valid:false, errorMsg:'請輸入數字'};
                if (inputValue < 0) return {valid:false, errorMsg:'輸入數字必須大於零'};
                return {valid:true, errorMsg:''}
            }
        },
        {
            isShow: sampleCodeIndicateIndex > 3 || (sampleCodeIndicateIndex === 3 && interactiveIndicateIndex > 0),
            label: '請輸入c: ',
            defaultValue: 3,
            value: cValue[0],
            isActive: (sampleCodeIndicateIndex === 3 && interactiveIndicateIndex === 1),
            onChange(value) {cValue[0] = value},
            isValid(inputString) {
                const inputValue = Number.parseInt(inputString)
                if (isEmpty(inputString)) return {valid:false, errorMsg:'輸入欄位不能為空'};
                if (isNaN(inputValue)) return {valid:false, errorMsg:'請輸入數字'};
                if (inputValue < 0) return {valid:false, errorMsg:'輸入數字必須大於零'};
                return {valid:true, errorMsg:''}
            }
        },
    ]
}

function isEmpty(valid){
    return (valid === undefined) || (valid === '');
}
