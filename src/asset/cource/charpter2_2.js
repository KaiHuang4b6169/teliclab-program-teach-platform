export const courceTitle = `
    <p className="my-0">GOGO百貨週年慶「全館八折，滿萬七折」活動中，請讀取採購總金額，計算折扣，並顯示折扣後該收取的總金額。</p>
    <p className="my-0">(EX: 23456元滿萬打七折，應付16419.2  )</p>
`

export const programmingGrammar = `
    <p className="my-0"><span style="background:#AAD8C1;">if 條件運算值 :</span></p>
    <p className="my-0"><span style="background:#AAD8C1; white-space:pre;">  </span>&nbsp;條件"成立"時所執行的指令</p>
    <p className="my-0"><span style="background:#AAD8C1;">else:</span></p>
    <p className="my-0"><span style="background:#AAD8C1; white-space:pre;">  </span>&nbsp;條件"不成立"時所執行的指令</p>
`

export const hintDialogContent = `
    <p>程式碼第1行：</p>
    <p>透過input()函式，取得使用者所輸入的字串，為了使輸入的金額呈數值資料，使用int()函式，從字串型別轉換為整數型別，再 assign(指派) 給變數 price</p>

    <p>程式碼第2-5行：</p>
    <p>運用if…else條件判斷語法，如果總金額高於或等於一萬元，代表總金額可以打七折，反之小於一萬元，則總金額打八折。</p>
`;

export const courceIndex = '單元範例-例題2'

export const sampleCode = [
    `<p className="my-0" style="font-size: 10px"><span style="background: peachpuff;">price</span>= int(input("請輸入採購總金額："))</p>`,
    `<p className="my-0" style="font-size: 10px"><span style="background:#AAD8C1;">if (price >= 10000):</span></p>`,
    `<p className="my-0" style="font-size: 10px"><span style="background:#AAD8C1; white-space:pre;">  </span>&nbsp;<span style="background: LightSkyBlue;">print(price,"元滿萬打七折，應付",price*0.7,"元")</span></p>`,
    `<p className="my-0" style="font-size: 10px"><span style="background:#AAD8C1;">else:</span></p>`,
    `<p className="my-0" style="font-size: 10px"><span style="background:#AAD8C1; white-space:pre;">  </span>&nbsp;<span style="background: LightSkyBlue;">print(price,"元打八折，應付",price*0.8,"元")</span></p>`

]

export const defaultCourceData = [
    {
        type: 'codeExplain',
        data: [
            {type: 'hint', lable:'變數名稱', content:'變數值'},
            {type: 'variable', lable:'price', content:'23456', showInTopicStep: 1, showInTeachStep: 5},
            {type: 'assign', showInTopicStep: 1, showInTeachStep: 4},
            {type: 'input', lable:'int()', content:'23456', showInTopicStep: 1, showInTeachStep: 3},
            {type: 'assign', showInTopicStep: 1, showInTeachStep: 2},
            {type: 'input', lable:'input("請輸入採購總金額：")', content:'"23456"', showInTopicStep: 1, showInTeachStep: 1},
            {type: 'hint', lable:'函式', content:'函數傳回值'},
        ],
    },
]

export function conditionCourceData(teachData) {
    return [
        {
            type: 'condition',
            content: {
                type: 'conditionStatementBlock',
                content:{
                    title: 'if條件運算值',
                    data: [
                        {type: 'variable', lable:'price', content: teachData[0].data[1].content, showInTopicStep: 2, showInTeachStep: 0},
                        {type: 'text', lable:'', content:'>=10000'},
                    ],
                }
            }
        },
        {
            type: 'true',
            content: {
                type: 'printBlock',
                content: {
                    title: '條件 "成立" 時\n所執行的指令',
                    data: [
                        {type: 'text', lable:'', content:'print( '},
                        {type: 'variable', lable:'price', content: teachData[0].data[1].content, showInTopicStep: 3, showInTeachStep: 1},
                        {type: 'text', lable:'', content:' ,"元滿萬打七折，應付", '},
                        {type: 'variable', lable:'price', content: teachData[0].data[1].content, showInTopicStep: 3, showInTeachStep: 1},
                        {type: 'text', lable:'', content:' *0.7,"元")'},
                    ],
                }
            }
        },
        {
            type: 'false',
            content: {
                type: 'printBlock',
                content: {
                    title: '條件 "不成立" 時\n所執行的指令',
                    data: [
                        {type: 'text', lable:'', content:'print( '},
                        {type: 'variable', lable:'price', content: teachData[0].data[1].content, showInTopicStep: 3, showInTeachStep: 1},
                        {type: 'text', lable:'', content:' ,"元打八折，應付", '},
                        {type: 'variable', lable:'price', content: teachData[0].data[1].content, showInTopicStep: 3, showInTeachStep: 1},
                        {type: 'text', lable:'', content:' *0.8,"元")'},
                    ],
                }
            }
        },
    ]
}

export const stateObject = {
    topicStep: 0,
    teachStep: 0,
    teachStepOfTopic: [0, 5, 1, 1],
    teachAreatype: [
        {type: "normal", topicStep: 0},
        {type: "condition", topicStep: 2}
    ]
}

export const inputObject = [
    {
        label: '請輸入採購總金額：',
        content: '23456', 
        invalidMsg: '輸入欄位不能為空',
        showInTopicStep: 1, 
        showInTeachStep: 0, 
        setData(courceData, value) { return setPrice(courceData, value)}, 
        isValid(value){ 
            if (isEmpty(value)) return {isValid:false, errorMsg:'輸入欄位不能為空'};
            if (isNaN(value)) return {isValid:false, errorMsg:'請輸入數字'};
            return {isValid:true, errorMsg:''}
        }
    },
]

function setPrice(courceData, value){    
    courceData[0].data[1].content = Number.parseInt(value);
    courceData[0].data[3].content = Number.parseInt(value);
    courceData[0].data[5].content = '"' + value + '"';

    return courceData;
}

function isEmpty(valid){
    return (valid === undefined) || (valid === '');
}

export function getResult(courceData){
    
    const price = Number.parseInt(courceData[0].data[1].content);
    return [  
        {
            showInTopicStep: 3,
            showInTeachStep: 1, 
            print: price >= 10000 ? price + " 元滿萬打七折，應付 " + price*0.7 + " 元" : price + " 元滿萬打八折，應付 " + price*0.8 + " 元"
        }
    ];
}

export function isShowSampleCodeArrow(activateStep, index, courceData){
    const price = Number.parseInt(courceData[0].data[1].content);

    if (activateStep < 3)
        return activateStep === index+1
    else if (activateStep === 3)
        return (price >= 10000) ? index === 2 : index === 4
    else
        return false
}

export function graphCondition(courceData){
    return {showArrow: {showInTopicStep: 2, showInTeachStep: 1}, changeLink: {showInTopicStep: 3, showInTeachStep: 1}, isStatify: Number.parseInt(courceData[0].data[1].content) >= 10000}
}