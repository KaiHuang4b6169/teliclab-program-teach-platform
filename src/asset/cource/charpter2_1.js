export const courceTitle = `
    <p className="my-0">假設本學期期末成績計算比重分配如下：作業40%、小考20%、期末考40%，請寫一個程式來計算期末的總成績。</p>
    <p className="my-0">輸入：請讓使用者分別輸入作業、小考、期末考分數(假設都是整數) </p>
    <p className="my-0">輸出：請輸出期末的總成績 </p>
    <p className="my-0">加入條件判斷式，學期總成績大於等於60分，則顯示出”你及格了!” 學期總成績小於60分，則顯示出”你不及格!” </p>
    <p className="my-0">(EX:作業:80 小考:70 期末考:64 學期期末總成績:71.6 你及格了!)</p>
`

export const programmingGrammar = `
    <p className="my-0"><span style="background:#AAD8C1;">if 條件運算值 :</span></p>
    <p className="my-0"><span style="background:#AAD8C1; white-space:pre;">  </span>&nbsp;條件"成立"時所執行的指令</p>
    <p className="my-0"><span style="background:#AAD8C1;">else:</span></p>
    <p className="my-0"><span style="background:#AAD8C1; white-space:pre;">  </span>&nbsp;條件"不成立"時所執行的指令</p>
`

export const hintDialogContent = `
    <p>程式碼第1-3行：</p>
    <p>透過input()函式，取得使用者所輸入的字串，為了使輸入的分數呈數值資料，使用int()函式，從字串型別轉換為整數型別，再 assign(指派) 給變數 h、t和f</p>

    <p>程式碼第4-5行：</p>
    <p>為了使期末成績按照比重計算，而進行乘法算術運算，再將運算後的總成績 assign(指派) 給變數 socre，並利用print()函式輸出</p>

    <p>程式碼第6-9行：</p>
    <p>運用if…else條件判斷語法，如果總成績大於等於60分，代表及格了，反之小於60分，則不及格。</p>
`;

export const courceIndex = '單元範例-例題1'

export const sampleCode = [
    `<p className="my-0"><span style="background: peachpuff;">h</span>= int(input("請輸入作業分數："))</p>`,
    `<p className="my-0"><span style="background: peachpuff;">t</span>= int(input("請輸入小考分數："))</p>`,
    `<p className="my-0"><span style="background: peachpuff;">f</span>= int(input("請輸入期末考分數："))</p>`,
    `<p className="my-0"><span style="background: peachpuff;">score</span> = h * 0.4 +t * 0.2 +f * 0.4</p>`,
    `<p className="my-0"><span style="background: LightSkyBlue;">print("學期期末總成績:",score,"分")</span></p>`,
    `<p className="my-0"><span style="background:#AAD8C1;">if (score >= 60):</span></p>`,
    `<p className="my-0"><span style="background:#AAD8C1; white-space:pre;">  </span>&nbsp;<span style="background: LightSkyBlue;">print("你及格了！")</span></p>`,
    `<p className="my-0"><span style="background:#AAD8C1;">else:</span></p>`,
    `<p className="my-0"><span style="background:#AAD8C1; white-space:pre;">  </span>&nbsp;<span style="background: LightSkyBlue;">print("你不及格！")</span></p>`

]

export const defaultCourceData = [
    {
        type: 'codeExplain',
        data: [
            {type: 'hint', lable:'變數名稱', content:'變數值'},
            {type: 'variable', lable:'h', content:'80', showInTopicStep: 1, showInTeachStep: 5},
            {type: 'assign', showInTopicStep: 1, showInTeachStep: 4},
            {type: 'input', lable:'int()', content:'80', showInTopicStep: 1, showInTeachStep: 3},
            {type: 'assign', showInTopicStep: 1, showInTeachStep: 2},
            {type: 'input', lable:'input("請輸入作業分數：")', content:'"80"', showInTopicStep: 1, showInTeachStep: 1},
            {type: 'hint', lable:'函式', content:'函數傳回值'},
        ],
    },
    {
        type: 'codeExplain',
        data: [
            {type: 'hint'},
            {type: 'variable', lable:'t', content:'70', showInTopicStep: 2, showInTeachStep: 5},
            {type: 'assign', showInTopicStep: 2, showInTeachStep: 4},
            {type: 'input', lable:'int()', content:'70', showInTopicStep: 2, showInTeachStep: 3},
            {type: 'assign', showInTopicStep: 2, showInTeachStep: 2},
            {type: 'input', lable:'input("請輸入小考分數：")', content:'"70"', showInTopicStep: 2, showInTeachStep: 1},
            {type: 'hint'},
        ],
    },
    {
        type: 'codeExplain',
        data: [
            {type: 'hint'},
            {type: 'variable', lable:'f', content:'64', showInTopicStep: 3, showInTeachStep: 5},
            {type: 'assign', showInTopicStep: 3, showInTeachStep: 4},
            {type: 'input', lable:'int()', content:'64', showInTopicStep: 3, showInTeachStep: 3},
            {type: 'assign', showInTopicStep: 3, showInTeachStep: 2},
            {type: 'input', lable:'input("請輸入期末考分數：")', content:'"64"', showInTopicStep: 3, showInTeachStep: 1},
            {type: 'hint'},
        ],
    },
    {
        type: 'codeExplain',
        data: [
            {type: 'hint'},
            {type: 'variable', lable:'score', content:'71.6', showInTopicStep: 4, showInTeachStep: 3},
            {type: 'assign', showInTopicStep: 4, showInTeachStep: 2},
            {type: 'variable', lable:'h', content:'80', showInTopicStep: 4, showInTeachStep: 1},
            {type: 'text', lable:'', content:' * 0.4 + '},
            {type: 'variable', lable:'t', content:'70', showInTopicStep: 4, showInTeachStep: 1},
            {type: 'text', lable:'', content:' * 0.2 +'},
            {type: 'variable', lable:'f', content:'64', showInTopicStep: 4, showInTeachStep: 1},
            {type: 'text', lable:'', content:' * 0.4 '},
            {type: 'hint'},
        ],
    },
    {
        type: 'printer',
        data: [
            {type: 'hint'},
            {type: 'text', lable:'', content:'print("學期期末總成績:", '},
            {type: 'variable', lable:'score', content:'71.6', showInTopicStep: 5, showInTeachStep: 1},
            {type: 'text', lable:'', content:' ,"分")'},
            {type: 'hint'},
        ]
    },
]

export function conditionCourceData(teachData) {
    return [
        {
            type: 'condition',
            content: {
                type: 'conditionStatementBlock',
                content:{
                    title: 'if條件運算值',
                    data: [
                        {type: 'variable', lable:'score', content: getScore(teachData), showInTopicStep: 1, showInTeachStep: 5},
                        {type: 'text', lable:'', content:'>=60'},
                    ],
                }
            }
        },
        {
            type: 'true',
            content: {
                type: 'printBlock',
                content: {
                    title: '條件 "成立" 時\n所執行的指令',
                    data: [
                        {type: 'text', lable:'', content:'print("你及格了!")'},
                    ],
                }
            }
        },
        {
            type: 'false',
            content: {
                type: 'printBlock',
                content: {
                    title: '條件 "不成立" 時\n所執行的指令',
                    data: [
                        {type: 'text', lable:'', content:'print("你不及格了!")'},
                    ],
                }
            }
        },
    ]
}

export const stateObject = {
    topicStep: 0,
    teachStep: 0,
    teachStepOfTopic: [0, 5, 5, 5, 3, 1, 1, 1],
    teachAreatype: [
        {type: "normal", topicStep: 0},
        {type: "condition", topicStep: 6}
    ]
}

export const inputObject = [
    {
        label: '請輸入作業分數：',
        content: '80', 
        invalidMsg: '輸入欄位不能為空',
        showInTopicStep: 1, 
        showInTeachStep: 0, 
        setData(courceData, value) { return setHomeWorkScore(courceData, value)}, 
        isValid(value){ 
            if (isEmpty(value)) return {isValid:false, errorMsg:'輸入欄位不能為空'};
            if (isNaN(value)) return {isValid:false, errorMsg:'請輸入數字'};
            return {isValid:true, errorMsg:''}
        }
    },
    {
        label: '請輸入小考分數：',
        content: '70', 
        invalidMsg: '輸入欄位不能為空',
        showInTopicStep: 2, 
        showInTeachStep: 0, 
        setData(courceData, value) { return setTestkScore(courceData, value)}, 
        isValid(value){ 
            if (isEmpty(value)) return {isValid:false, errorMsg:'輸入欄位不能為空'};
            if (isNaN(value)) return {isValid:false, errorMsg:'請輸入數字'};
            return {isValid:true, errorMsg:''}
        }
    },
    {
        label: '請輸入期末考分數：',
        content: '64', 
        invalidMsg: '輸入欄位不能為空',
        showInTopicStep: 3, 
        showInTeachStep: 0, 
        setData(courceData, value) { return setExampleScore(courceData, value)}, 
        isValid(value){ 
            if (isEmpty(value)) return {isValid:false, errorMsg:'輸入欄位不能為空'};
            if (isNaN(value)) return {isValid:false, errorMsg:'請輸入數字'};
            return {isValid:true, errorMsg:''}
        }
    },
]

function setHomeWorkScore(courceData, value){    
    courceData[0].data[1].content = Number.parseInt(value);
    courceData[0].data[3].content = Number.parseInt(value);
    courceData[0].data[5].content = '"' + value + '"';

    courceData[3].data[3].content = Number.parseInt(value);
    return courceData;
}

function setTestkScore(courceData, value){
    courceData[1].data[1].content = Number.parseInt(value);
    courceData[1].data[3].content = Number.parseInt(value);
    courceData[1].data[5].content = '"' + value + '"';

    courceData[3].data[5].content = Number.parseInt(value);
    return courceData;
}

function setExampleScore(courceData, value){
    courceData[2].data[1].content = Number.parseInt(value);
    courceData[2].data[3].content = Number.parseInt(value);
    courceData[2].data[5].content = '"' + value + '"';

    courceData[3].data[1].content = courceData[2].data[1].content * 0.4 + courceData[1].data[1].content * 0.2 + courceData[0].data[1].content * 0.4;
    courceData[3].data[7].content = Number.parseInt(value);

    courceData[4].data[2].content = courceData[3].data[1].content
    return courceData;
}

function isEmpty(valid){
    return (valid === undefined) || (valid === '');
}

export function getResult(courceData){
    const score = getScore(courceData);
    return [
        {
            showInTopicStep: 5,
            showInTeachStep: 1, 
            print: '學期期末總成績： ' + score + ' 分'
        },        
        {
            showInTopicStep: 7,
            showInTeachStep: 1, 
            print: score >= 60 ? '你及格了!' : '你不及格了!'
        }
    ];
}

export function isShowSampleCodeArrow(activateStep, index, courceData){
    const score = getScore(courceData);

    if (activateStep < 7)
        return activateStep === index+1
    else if (activateStep === 7)
        return (score >= 60) ? index === 6 : index === 8 
    else
        return false
}

export function graphCondition(courceData){
    return {showArrow: {showInTopicStep: 6, showInTeachStep: 1}, changeLink: {showInTopicStep: 7, showInTeachStep: 1}, isStatify: getScore(courceData)>=60}
}

function getScore(courceData){
    const homeWork = Number.parseInt(courceData[0].data[1].content);
    const test = Number.parseInt(courceData[1].data[1].content);
    const example = Number.parseInt(courceData[2].data[1].content);

    return homeWork*0.4 + test*0.2 + example*0.4;
}