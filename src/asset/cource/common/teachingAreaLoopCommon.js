import { Stack, Table } from "react-bootstrap";
import { RangeArrow, RangeAsignArrow } from "../../images";
import {elementTitleStyle, elementRangeBackgroundStyle, elementWhiteBackgroundStyle, elementOrigeBackgroundStyle, explainTextStyle, lableStyle, rangeTableRowStyle, arrowSize, middleStyle, variableMinWidth} from "./courceCss.js"

export function getRangeTags(start, end, step){
    return (
        <div className='ms-auto px-1' style={{display: 'inline-block'}}>
            <div className="text-center" style={elementTitleStyle}></div>
            <Stack direction="horizontal" style={{...elementRangeBackgroundStyle, whiteSpace: 'nowrap'}}>
                <div>
                    range({start} ,{end} ,{step} )
                </div>
            </Stack>
        </div>
    );
}

export function getRangeExplain(explaintext){
    return (
        <div className='px-1'>
            <div className="text-center" style={elementTitleStyle}></div>
            {explaintext.map((text, key) => <div style={explainTextStyle} key={key}>{text}</div>)}
        </div>
    );
}

export function getExplainOfVariableOfRangeLine(){
    
    const variableLabelTag = (<div style={explainTextStyle}>變數名稱</div>);
    const variableValueTag = (<div style={{...explainTextStyle, ...middleStyle, ...rangeTableRowStyle}}>變數值</div>);
    return interfaceOfRangeLine(variableLabelTag, variableValueTag, undefined);
}

export function getVariableOfRangeLine(variableLabel, variableValue){
    const variableLabelTag = (<div className="text-center" style={elementTitleStyle}>{variableLabel}</div>);
    const variableValueTag = (
        <div className='text-center' style={{...elementOrigeBackgroundStyle, ...middleStyle, ...rangeTableRowStyle, ...variableMinWidth}}>
                {variableValue}
        </div>
    );
    return interfaceOfRangeLine(variableLabelTag, variableValueTag, undefined);
}

export function getRangeOfRangeLine(start, end, step, indicateIndex){
    const rangeTableFunctions = getRangeTableFunctions(start, end, step)
    
    return (
        <div className='px-1 ms-auto' style={{display: 'inline-block'}}>
            <div className="text-end" style={{...elementTitleStyle, ...lableStyle}}>| |</div>
            
            <Table className="table-borderless my-0">
                <tbody>
                    {rangeTableFunctions[0]()}
                    {rangeTableFunctions[1](indicateIndex)}
                </tbody>
            </Table>
        </div>
    );
}

export function getRangeAsignArrowOfRangeLine(isShow){
    const assignArrow = isShow ? (<RangeAsignArrow style={{...rangeTableRowStyle, width: '300px'}}/>) : undefined
    return interfaceOfRangeLine(undefined, assignArrow, undefined);
}

export function interfaceOfRangeLine(firstRow, secondRow, thirdRow){
    return (
        <div className='px-1' style={{display: 'inline-block'}}>
            <div style={lableStyle}>{firstRow}</div>
            <div style={rangeTableRowStyle}>{secondRow}</div>
            <div style={rangeTableRowStyle}>{thirdRow}</div>
        </div>
    );
}

function getRangeTableFunctions(start, end, step){
    const showVariable = () => {
        let result = [];
        for (var i = start; i < end; i+=step){
            result.push(i);
        }
        return result;
    }

    const rowContentGenerater = function(value, valueStyle, middleSign, splitSign, arrageLength, key) {
        const fixKey = key * 3
        const valueElementTag = (<td key={fixKey}><div className="text-center" style={{...valueStyle, ...middleStyle}}>{value}</div></td>);
        const splitElementTag = (<td key={fixKey + 1}>{splitSign}</td>);
        const middleElementTag = (<td key={fixKey + 2}>{middleSign}</td>);

        let returnTags = [];

        if (key < 3 ){
            returnTags.push(valueElementTag)
            if (key < arrageLength - 1)  returnTags.push(splitElementTag)
        }

        else if(key === 3){
            returnTags.push(middleElementTag)
        }
            
        return returnTags
    }  

    const arrayTables = (
        <tr style={{...elementRangeBackgroundStyle}}>
            <td>[</td>
            {showVariable().map((i, key)=>{
                return rowContentGenerater(i, {...elementWhiteBackgroundStyle, width: arrowSize.width}, '...', ',', showVariable().length, key)
            })}
            <td>]</td>
        </tr>
    )

    const indicateTables = function (indicateIndex) {
        
            return (
                <tr>
                    <td style={rangeTableRowStyle}></td>
                    {showVariable().map((i, key)=>{
                        const value = (indicateIndex === key) ? (<RangeArrow style={arrowSize}/>) : undefined
                        return rowContentGenerater(value, {}, '', '', showVariable().length, key)
                    })}
                    <td></td>
                </tr>
            )
    }

    return [() => arrayTables, indicateTables]
}

function displayOnMiddle(pramas){
    return (
        <div className="position-relative">
            <div className="position-absolute top-50 start-0 translate-middle-y">
                {pramas}
            </div>
        </div>
    )
}