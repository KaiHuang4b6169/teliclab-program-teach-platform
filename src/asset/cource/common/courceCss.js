export const elementTitleStyle = {color: 'DarkGreen'};
export const elementRangeBackgroundStyle = {background: '#D1A4CA'};
export const elementWhiteBackgroundStyle = {background: 'white'}
export const elementOrigeBackgroundStyle = {background: 'peachpuff'}
export const elementGrayBackgroundStyle = {background: '#C9C9C9'}

export const explainTextStyle = { color: 'LightSalmon' }

export const lableStyle = {clear: 'both', height: '30px'}
export const rangeTableRowStyle = {height:"40px"}
export const arrowSize = {width:"20px", height:"20px"}
export const middleStyle = {
    verticalAlign: 'middle',
    display: 'table-cell',
}

export const codeTableElementStyle = { height: '36px'};


export const labelTableElementStyle = {
    height: '18px',
    fontSize: '6px',
};

export const variableMinWidth = {minWidth: '20px'}

export const borderElementStyle = {borderStyle: 'solid'}