import { Stack } from "react-bootstrap";
import { AsignArrow } from "../../images.js";
import {elementGrayBackgroundStyle, elementOrigeBackgroundStyle, codeTableElementStyle, labelTableElementStyle, middleStyle, variableMinWidth, explainTextStyle, elementRangeBackgroundStyle, borderElementStyle} from "./courceCss.js"

export function getPrint(contentElements, blockStyle){
    return (
        <Stack direction="horizontal" style={{...elementGrayBackgroundStyle, ...blockStyle}}>
            {contentElements.map((element, key) => {
                return (
                    <div key={key}>
                        {element}
                    </div>
                )
            })}
        </Stack>
    );
}

export function getPurpleBlock(contentElements, blockStyle, className){
    return (
        <Stack className={className} direction="horizontal" style={{...elementRangeBackgroundStyle, ...blockStyle}}>
            {contentElements.map((element, key) => {
                return (
                    <div key={key}>
                        {element}
                    </div>
                )
            })}
        </Stack>
    );
}


export function getWhileBlock(label='', content=''){
    return interfaceProgramComponent(label, content, {}, explainTextStyle, elementRangeBackgroundStyle)
}

export function variableProgramComponent(label='', content='', blockClassName = 'px-1 py-1'){
    return interfaceProgramComponent(label, content, {}, {}, {...elementOrigeBackgroundStyle, ...variableMinWidth}, blockClassName)
}


export function inputProgramComponent(label='', content='', blockClassName = 'px-1 py-1'){
    const inputValueTag = <div className=' align-self-center'>{content}</div>
    return interfaceProgramComponent(label, inputValueTag, {}, {}, {...borderElementStyle, width: '100%', display: 'flex'}, blockClassName, '', 'justify-content-center')
}

export function textProgramComponent(content='', elementTitleStyle={}, blockClassName = 'px-1 py-1'){
    return interfaceProgramComponent('', content, {}, {}, elementTitleStyle, blockClassName)
}

export function assignProgramComponent(isSow){
    const assignArrow = (isSow) ? <AsignArrow style={{width: '50px'}} /> : undefined
    return interfaceProgramComponent('', assignArrow, {minWidth: '58px'})
}

export function explainProgramComponent(label='', content=''){
    return interfaceProgramComponent(label, content, {}, explainTextStyle, explainTextStyle, {}, 'px-1 py-1', '', '')
}

export function conditionProgramComponent(label='', content=''){
    return interfaceProgramComponent(label, content, {}, explainTextStyle, explainTextStyle)
}

export function interfaceProgramComponent(label='', content='', blockStyle={}, elementLabelStyle={}, elementContentStyle={}, blockClassName = 'px-1 py-1', lableClassName = 'text-center', contentClassName = 'text-center'){
    return (
        <div className={blockClassName} style={{...blockStyle, whiteSpace: 'nowrap'}}>
            <div className={lableClassName} style={{...elementLabelStyle, ...labelTableElementStyle}}>{label}</div>
            <div className={contentClassName} style={{...codeTableElementStyle, ...middleStyle, ...elementContentStyle}}>{content}</div>
        </div>
    );
}