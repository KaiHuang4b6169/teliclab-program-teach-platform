export function stageAddTools(margeStage, minorSage, stageArray) {
    if (margeStage === stageArray.length - 1 && minorSage === stageArray[margeStage]) return {margeStage, minorSage}

    const isOverflow = minorSage >= stageArray[margeStage]
    if (isOverflow){
        margeStage += 1
        minorSage = 0
    } else {
        minorSage += 1
    }
    
    return {margeStage, minorSage}
}


export function stageSubTools(margeStage, minorSage, stageArray) {
    const isOverflow = minorSage <= 0
    if (isOverflow){
        margeStage = (margeStage === 0) ? 0 : margeStage - 1
        minorSage = stageArray[margeStage]
    } else {
        minorSage = (minorSage === 0) ? 0 : minorSage - 1
    }
    
    return {margeStage, minorSage}
}