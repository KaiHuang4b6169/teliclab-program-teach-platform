export const courceTitle = `
    <p>美國職籃NBA球員除了有過人的球技，身材的優勢也是非常重要。請將球員身高從呎吋轉換為公分。一呎=12吋，一吋=2.54公分</p>
    <p>(EX:輸入-6(呎)9(吋)，輸出-205.74公分)</p>

`

export const programmingGrammar = `
    <p className="my-0"><span style="background: peachpuff;">變數</span> = int(input)</p>
    <p className="my-0"><span style="background: LightSkyBlue;">print(輸出內容)</span></p>
`

export const hintDialogContent = `
    <span style="fontWeight: bold;">程式碼第1行：</span>
    <p>透過input()函式，取得使用者所輸入的字串，為了使輸入的數字呈數值資料，使用int()函式，從字串型別轉換為整數型別，再將運算後的整數 assign(指派) 給變數a</p>

    <span style="fontWeight: bold;">程式碼第2行：</span>
    <p>透過input()函式，使得輸入字串，為了使輸入的數字呈數值資料，使用int()函式，從字串型別轉換為整數型別，再將整數 assign(指派) 給變數 b</p>

    <span style="fontWeight: bold;">程式碼第3行：</span>
    <p>使用 print() 函式列印出字串以及變數a與b運算內容</p>
`;

export const courceIndex = '單元範例-例題7'

export const sampleCode = [
    `<p className="my-0"><span style="background: peachpuff;">a</span>=int(input("請輸入呎："))</p>`,
    `<p className="my-0"><span style="background: peachpuff;">b</span>=int(input("請輸入吋："))</p>`,
    `<p className="my-0"><span style="background: LightSkyBlue;">print((a*12+b)*2.54,"公分")</span></p>`,
]

export const defaultCourceData = [
    {
        type: 'codeExplain',
        data: [
            {type: 'hint', lable:'變數名稱', content:'變數值'},
            {type: 'variable', lable:'a', content:'6', showInTopicStep: 1, showInTeachStep: 5},
            {type: 'assign', showInTopicStep: 1, showInTeachStep: 4},
            {type: 'input', lable:'int()', content:'6', showInTopicStep: 1, showInTeachStep: 3},
            {type: 'assign', showInTopicStep: 1, showInTeachStep: 2},
            {type: 'input', lable:'input("請輸入呎:")', content:'"6"', showInTopicStep: 1, showInTeachStep: 1},
            {type: 'hint', lable:'函式', content:'函數傳回值'},
        ],
    },
    {
        type: 'codeExplain',
        data: [
            {type: 'hint', lable:'變數名稱', content:'變數值'},
            {type: 'variable', lable:'b', content:'9', showInTopicStep: 2, showInTeachStep: 5},
            {type: 'assign', showInTopicStep: 2, showInTeachStep: 4},
            {type: 'input', lable:'int()', content:'9', showInTopicStep: 2, showInTeachStep: 3},
            {type: 'assign', showInTopicStep: 2, showInTeachStep: 2},
            {type: 'input', lable:'input("請輸入吋:")', content:'"9"', showInTopicStep: 2, showInTeachStep: 1},
            {type: 'hint', lable:'函式', content:'函數傳回值'},
        ],
    },
    {
        type: 'printer',
        data: [
            {type: 'hint'},
            {type: 'text', lable:'', content:'print(( '},
            {type: 'variable', lable:'a', content:'6', showInTopicStep: 3, showInTeachStep: 1},
            {type: 'text', lable:'', content:'* 12 + '},
            {type: 'variable', lable:'b', content:'9', showInTopicStep: 3, showInTeachStep: 1},
            {type: 'text', lable:'', content:' ) * 2.54, "公分")'},
            {type: 'hint'},
        ]
    },
]

export const stateObject = {
    topicStep: 0,
    teachStep: 0,
    teachStepOfTopic: [0, 5, 5, 1]
}

export const inputObject = [
    {
        label: '請輸入呎：',
        content: '6', 
        invalidMsg: '輸入欄位不能為空',
        showInTopicStep: 1, 
        showInTeachStep: 0, 
        setData(courceData, value) { return setFeet(courceData, value)}, 
        isValid(value){ 
            if (isEmpty(value)) return {isValid:false, errorMsg:'輸入欄位不能為空'};
            if (isNaN(value)) return {isValid:false, errorMsg:'請輸入數字'};
            return {isValid:true, errorMsg:''}
        }
    },
    {
        label: '請輸入吋：',
        content: '9', 
        invalidMsg: '輸入欄位不能為空',
        showInTopicStep: 2, 
        showInTeachStep: 0, 
        setData(courceData, value) { return setInches(courceData, value)}, 
        isValid(value){ 
            if (isEmpty(value)) return {isValid:false, errorMsg:'輸入欄位不能為空'};
            if (isNaN(value)) return {isValid:false, errorMsg:'請輸入數字'};
            return {isValid:true, errorMsg:''}
        }
    },
]

function setFeet(courceData, value){
    courceData[0].data[1].content = Number.parseInt(value);
    courceData[0].data[3].content = Number.parseInt(value);
    courceData[0].data[5].content = '"' + value + '"';

    courceData[2].data[2].content = Number.parseInt(value);
    return courceData;
}

function setInches(courceData, value){
    courceData[1].data[1].content = Number.parseInt(value);
    courceData[1].data[3].content = Number.parseInt(value);
    courceData[1].data[5].content = '"' + value + '"';

    courceData[2].data[4].content = Number.parseInt(value);
    return courceData;
}

function isEmpty(valid){
    return (valid === undefined) || (valid === '');
}

export function getResult(courceData){
    const feet = Number.parseInt(courceData[2].data[2].content);
    const inches = Number.parseInt(courceData[2].data[4].content);
    return [
        {
            showInTopicStep: 3,
            showInTeachStep: 1, 
            print: (feet * 12 + inches) * 2.54 + "公分"
        }
    ];
}