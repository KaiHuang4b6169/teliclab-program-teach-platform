export const courceTitle = `
    <p>請寫一個程式來幫忙計算圓面積及圓周長，輸入半徑，計算並輸出圓面積及圓周長。 </p>
    <p>(輸入-請輸入半徑：4 輸出-半徑為4公分的圓面積為50.24平方公分，圓周長為25.12公分)</p>
`

export const programmingGrammar = `
    <p className="my-0"><span style="background: peachpuff;">變數</span> = int(input)</p>
    <p className="my-0"><span style="background: LightSkyBlue;">print(輸出內容)</span></p>
`

export const hintDialogContent = `
    <span style="fontWeight: bold;">程式碼第1行 : </span>
    <p>先透過 input() 函式，取得使用者所輸入的字串，為了使輸入之半徑呈數值資料，使用int()函式，從字串型別轉換為整數型別，並assign(指派)給變數r</p>

    <span style="fontWeight: bold;">程式碼第2-3行 : </span>
    <p>將運算結果分別assign(指派)到對應的變數</p>

    <span style="fontWeight: bold;">程式碼第4行 : </span>
    <p>使用print() 函式輸出字串以及半徑、圓面積及圓周長的運算內容。</p>
`;

export const courceIndex = '單元範例-例題5'

export const sampleCode = [
    `<p className="my-0"><span style="background: peachpuff;">r</span> = int(input("請輸入半徑:"))</p>`,
    `<p className="my-0"><span style="background: peachpuff;">area</span> = 3.14 * r * r</p>`,
    `<p className="my-0"><span style="background: peachpuff;">c</span> = 2 * 3.14 * r</p>`,
    `<p className="my-0"><span style="background: LightSkyBlue;">print("半徑為",r,"公分的圓面積為",area,"平方公分，圓周長為",c,"公分")</span></p>`,
]

export const defaultCourceData = [
    {
        type: 'codeExplain',
        data: [
            {type: 'hint', lable:'變數名稱', content:'變數值'},
            {type: 'variable', lable:'r', content:'4', showInTopicStep: 1, showInTeachStep: 5},
            {type: 'assign', showInTopicStep: 1, showInTeachStep: 4},
            {type: 'input', lable:'int()', content:'4', showInTopicStep: 1, showInTeachStep: 3},
            {type: 'assign', showInTopicStep: 1, showInTeachStep: 2},
            {type: 'input', lable:'input("請輸入半徑:")', content:'"4"', showInTopicStep: 1, showInTeachStep: 1},
            {type: 'hint', lable:'函式', content:'函數傳回值'},
        ],
    },
    {
        type: 'codeExplain',
        data: [
            {type: 'hint'},
            {type: 'variable', lable:'area', content:'50.24', showInTopicStep: 2, showInTeachStep: 3},
            {type: 'assign', showInTopicStep: 2, showInTeachStep: 2},
            {type: 'text', lable:'', content:'3.14 * '},
            {type: 'variable', lable:'r', content:'4', showInTopicStep: 2, showInTeachStep: 1},
            {type: 'text', lable:'', content:'*'},
            {type: 'variable', lable:'r', content:'4', showInTopicStep: 2, showInTeachStep: 1},
            {type: 'hint'},
        ],
    },
    {
        type: 'codeExplain',
        data: [
            {type: 'hint'},
            {type: 'variable', lable:'c', content:'25.12', showInTopicStep: 3, showInTeachStep: 3},
            {type: 'assign', showInTopicStep: 3, showInTeachStep: 2},
            {type: 'text', lable:'', content:'2 * 3.14 * '},
            {type: 'variable', lable:'r', content:'4', showInTopicStep: 3, showInTeachStep: 1},
            {type: 'hint'},
        ],
    },
    {
        type: 'printer',
        data: [
            // {type: 'hint'},
            {type: 'text', lable:'', content:'print("半徑為",'},
            {type: 'variable', lable:'r', content:'4', showInTopicStep: 4, showInTeachStep: 1},
            {type: 'text', lable:'', content:',"公分的圓面積為",'},
            {type: 'variable', lable:'area', content:'50.24', showInTopicStep: 4, showInTeachStep: 1},
            {type: 'text', lable:'', content:',"平方公分，圓周長為",'},
            {type: 'variable', lable:'c', content:'25.21', showInTopicStep: 4, showInTeachStep: 1},
            {type: 'text', lable:'', content:',"公分")'},
            // {type: 'hint'},
        ]
    },
]

export const stateObject = {
    topicStep: 0,
    teachStep: 0,
    teachStepOfTopic: [0, 5, 3, 3, 1]
}

export const inputObject = [
    {
        label: '請輸入半徑：',
        content: '4', 
        invalidMsg: '輸入欄位不能為空',
        showInTopicStep: 1, 
        showInTeachStep: 0, 
        setData(courceData, value) { return setRadius(courceData, value)}, 
        isValid(value){ 
            if (isEmpty(value)) return {isValid:false, errorMsg:'輸入欄位不能為空'};
            if (isNaN(value)) return {isValid:false, errorMsg:'請輸入數字'};
            return {isValid:true, errorMsg:''}
        }
    },
]

function setRadius(courceData, value){
    const radiusInt =  Number.parseInt(value)
    courceData[0].data[1].content = radiusInt;
    courceData[0].data[3].content = radiusInt;
    courceData[0].data[5].content = '"' + value + '"';

    courceData[1].data[1].content = 3.14 * radiusInt * radiusInt;
    courceData[1].data[4].content = radiusInt;
    courceData[1].data[6].content = radiusInt;
    
    courceData[2].data[1].content = 2 * 3.14 * radiusInt;
    courceData[2].data[4].content = radiusInt;

    courceData[3].data[1].content = radiusInt;
    courceData[3].data[3].content = 3.14 * radiusInt * radiusInt;
    courceData[3].data[5].content = 2 * 3.14 * radiusInt;
    return courceData;
}

function isEmpty(valid){
    return (valid === undefined) || (valid === '');
}

export function getResult(courceData){
    const radiusInt =  Number.parseInt(courceData[3].data[1].content)
    const area = 3.14 * radiusInt * radiusInt
    const c = 3.14 * 2 * radiusInt
    return [
        {
            showInTopicStep: 4,
            showInTeachStep: 1, 
            print: "半徑為 " + radiusInt + " 公分的圓面積為 " + area + " 平方公分，圓周長為 " + c + " 公分"
        }
    ];
}