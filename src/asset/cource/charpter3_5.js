import { Row, Stack } from "react-bootstrap";
import { isShow } from "../../utils/tool";
import { DownAssign, UpAssign } from "../images";
import { stageAddTools, stageSubTools } from "./common/commonTools";
import { elementOrigeBackgroundStyle, elementRangeBackgroundStyle, variableMinWidth } from "./common/courceCss";
import { assignProgramComponent, explainProgramComponent, getPrint, getPurpleBlock, inputProgramComponent, textProgramComponent, variableProgramComponent } from "./common/teachingAreaCommon";
import { getExplainOfVariableOfRangeLine, getRangeAsignArrowOfRangeLine, getRangeExplain, getRangeOfRangeLine, getRangeTags, getVariableOfRangeLine, interfaceOfRangeLine } from "./common/teachingAreaLoopCommon";

let nValue = ['', undefined, undefined, undefined]

let sumAssignValue = undefined
let sumAssign = undefined

let sumAdded = [undefined, undefined, undefined]
let sumAddedAssign = undefined

let finalPrint = undefined

let inputValueAssign = false
let parseIntAssign = false

let rangeVariableOfLoop = undefined;
let loopIndecateIndex = undefined;
let isAssginShow = false
let numberOfPrintInRange = -1;

let isAddStage = undefined
let isSubStage = undefined
let rangeKeyList = []
let rangeTextConfig = {}
let rangeConfig = {}

const numberOfEachStage = [0, 6, 2, 3, 3, 1]

export const courceTitle = (
    <div>
        <p className="my-0">用For迴圈算出1~N的奇數和並列印到螢幕上</p>
        <p className="my-0">(EX:輸入-10  輸出-25)</p>
    </div>
)

export const sampleCode = [
    (<div className="py-1" style={{display: 'flex'}}>
        <span className='text-center' style={{...elementOrigeBackgroundStyle, ...variableMinWidth}}>n</span>
        <span>&nbsp;=&nbsp;int(input("請輸入n: "))</span>
    </div>),
    (<div className="py-1" style={{display: 'flex'}}>
        <span className='text-center' style={{...elementOrigeBackgroundStyle, ...variableMinWidth}}>sum</span>
        <span>&nbsp;=&nbsp;0</span>
    </div>),
    (<div className="py-1" style={{display: 'flex'}}>
        <span>for&nbsp;</span>
        <span className='text-center' style={{...elementOrigeBackgroundStyle, ...variableMinWidth}}>i</span>
        <span>&nbsp;in&nbsp;</span>
        <div style={elementRangeBackgroundStyle}>
            range(1, 
            <span className='text-center' style={{...elementOrigeBackgroundStyle, ...variableMinWidth}}>n</span>
            +1, 2)
        </div>
    </div>),
    (<p className="py-1 my-0"><UpAssign className='px-1' height='20px'/><span style={{background: '#C9C9C9'}}>sum = sum + i</span><DownAssign className='px-1' height='20px'/></p>),
    (<p className="py-1 my-0"><span style={{background: 'LightSkyBlue'}}>print( sum )</span></p>),
]

export const programExplaiin = 
`程式碼第1行 : 
透過input()函式，取得使用者所輸入的字串 ，再將所輸入的字串 assign(指派) 給變數 n

程式碼第2行:
把儲存答案的變數sum初始化為0

程式碼第3-5行:
for迴圈可以重複相同的指令，直到執行完設定的次數，而range()函式可以很有效率地創建一個整數序列，用法為 (起始值, 結束值, 遞增/減值)，range(1, n+1, 2):產生從1到n的整數序列(遞增值為2)，而題目的要求是由印出奇數，所以range()的起始值為1，然後遞增值為2(數列為:1, 1+2, 1+2+2, …)。

在迴圈中執行的指令:
    1. 把新的i值加到sum(例如，當現在執行到i = 3時，那程式就是把3的值加到sum中，而原有sum的值為1(當i = 1時, sum = 1，而因為遞增值為2，所以i會直接跳到3)，所以sum的值會變更為4)，
    2. 透過print()函式把sum的值印出來。
`;

export function nextStage(sampleCodeIndicateIndex, interactiveIndicateIndex){
    isAddStage = true
    isSubStage = false
    let lastLoopLine = 4
    let isLastLoopState = sampleCodeIndicateIndex === lastLoopLine && interactiveIndicateIndex === numberOfEachStage[lastLoopLine]

    let {margeStage, minorSage} = stageAddTools(sampleCodeIndicateIndex, interactiveIndicateIndex, numberOfEachStage)
    margeStage = (loopIndecateIndex !== 2 && isLastLoopState) ? 3 : margeStage

    return {
        sampleCodeIndicateIndex : margeStage,
        interactiveIndicateIndex: minorSage
    };
}

export function priviousStage(sampleCodeIndicateIndex, interactiveIndicateIndex){
    isAddStage = false
    isSubStage = true

    let firstLoopLine = 3
    let lastLoopLine = 4
    let isFiirstLoopState = sampleCodeIndicateIndex === firstLoopLine && interactiveIndicateIndex === 0

    if (isFiirstLoopState && loopIndecateIndex !== undefined) return {
        sampleCodeIndicateIndex : lastLoopLine,
        interactiveIndicateIndex: numberOfEachStage[lastLoopLine]
    };
    let {margeStage, minorSage} = stageSubTools(sampleCodeIndicateIndex, interactiveIndicateIndex, numberOfEachStage)

    return {
        sampleCodeIndicateIndex : margeStage,
        interactiveIndicateIndex: minorSage
    };
}

export function courceTeachingArea(sampleCodeIndicateIndex, interactiveIndicateIndex){
    if (sampleCodeIndicateIndex === 0) setMajorStageOfZero(interactiveIndicateIndex)
    if (sampleCodeIndicateIndex === 1) setMajorStageOfOne(interactiveIndicateIndex)
    if (sampleCodeIndicateIndex === 2) setMajorStageOfTwo(interactiveIndicateIndex)
    if (sampleCodeIndicateIndex === 3) setMajorStageOfThree(interactiveIndicateIndex)
    if (sampleCodeIndicateIndex === 4) setMajorStageOfFour(interactiveIndicateIndex)
    if (sampleCodeIndicateIndex === 5) setMajorStageOfFive(interactiveIndicateIndex)

    const assignTeachArea = (
        <div>
            <Stack className="my-1 justify-content-evenly" direction="horizontal">
                {explainProgramComponent('變數名稱', '變數值')}
                {variableProgramComponent('n', nValue[3])}
                {assignProgramComponent(parseIntAssign)}
                {variableProgramComponent('int()', nValue[2])}
                {assignProgramComponent(inputValueAssign)}
                {inputProgramComponent('input("請輸入n: ")', nValue[1])}
                {explainProgramComponent('函數', '函數回傳值')}
            </Stack>
            <Stack className="my-1 justify-content-evenly" direction="horizontal">
                {explainProgramComponent('', '')}
                {explainProgramComponent('', '')}
                {variableProgramComponent('sum', sumAssignValue)}
                {assignProgramComponent(sumAssign)}
                {textProgramComponent(' 0 ')}
                {explainProgramComponent('', '')}
                {explainProgramComponent('', '')}
            </Stack>
        </div>    
    )

    const loopTeachingArea = (
        <div>
            <Stack className="my-1" direction="horizontal">          
                {getPurpleBlock(
                    [
                        textProgramComponent('range( ' + rangeTextConfig.start + ', '),
                        variableProgramComponent('n', nValue[0]),
                        textProgramComponent('+ 1 , ' + rangeTextConfig.gap + ' )'),
                    ], {},'ms-auto' 
                )}
                {getRangeExplain(['迴圈變數', '範圍設定'])}
            </Stack>

            <Stack direction="horizontal">
                {getExplainOfVariableOfRangeLine()}
                {getVariableOfRangeLine('i', rangeVariableOfLoop)}
                {getRangeAsignArrowOfRangeLine(isAssginShow)}
                {getRangeOfRangeLine(rangeConfig.start, rangeConfig.end , rangeConfig.gap, loopIndecateIndex)}
                {interfaceOfRangeLine(undefined, getRangeExplain(['','迴圈變數', 'list']), undefined)}
            </Stack>

            <Stack className="justify-content-center" direction="horizontal">
                {getPrint(
                    [
                        variableProgramComponent('sum', sumAdded[2]),
                        assignProgramComponent(sumAddedAssign),
                        variableProgramComponent('sum', sumAdded[1]),
                        textProgramComponent(' + '),
                        variableProgramComponent('i', sumAdded[0]),
                    ]
                )}
            </Stack>
        </div>        
    );

    const printTeachArea = (
        <div>            
            <Stack className="justify-content-center" direction="horizontal">
                {getPrint(
                    [
                        textProgramComponent('print( '),
                        variableProgramComponent('sum', finalPrint),
                        textProgramComponent(' )'),
                    ], {background: 'LightSkyBlue'}
                )}
            </Stack>
        </div>
    )

    if (sampleCodeIndicateIndex < 3) return assignTeachArea    
    if (sampleCodeIndicateIndex < 5)  return loopTeachingArea
    return printTeachArea
}

function setMajorStageOfZero(){ 
    rangeConfig = {start: 0, end: 0, gap: 0}
    rangeTextConfig = {start: 0, end: 0, gap: 0}

    nValue = ['', undefined, undefined, undefined]

    inputValueAssign = false
    parseIntAssign = false

    rangeVariableOfLoop = undefined;
    loopIndecateIndex = undefined;
    isAssginShow = false
    numberOfPrintInRange = -1;


    nValue = ['', undefined, undefined, undefined]

    sumAssignValue = undefined
    sumAssign = undefined

    sumAdded = [undefined, undefined, undefined]
    sumAddedAssign = undefined

    finalPrint = undefined
    
    rangeKeyList = []
}

function setMajorStageOfOne(interactiveIndicateIndex){ 
    if (isAddStage){
        if (interactiveIndicateIndex === 2)
            nValue[1] = '"' + nValue[0] + '"'
        if (interactiveIndicateIndex === 3)
            inputValueAssign = true
        if (interactiveIndicateIndex === 4)
            nValue[2] = nValue[0]
        if (interactiveIndicateIndex === 5)
            parseIntAssign = true
        if (interactiveIndicateIndex === 6)
            nValue[3] = nValue[0]
    }

    if (isSubStage) {
        if (interactiveIndicateIndex === 1)
            nValue[1] = undefined
        if (interactiveIndicateIndex === 2)
            inputValueAssign = false
        if (interactiveIndicateIndex === 3)
            nValue[2] = undefined
        if (interactiveIndicateIndex === 4)
            parseIntAssign = false
        if (interactiveIndicateIndex === 5)
            nValue[3] = undefined
        if (interactiveIndicateIndex === 6){            
            numberOfPrintInRange = -1
            rangeTextConfig = {start: 0, end: 0, gap: 0}
            rangeKeyList = []
        }
    }
}


function setMajorStageOfTwo(interactiveIndicateIndex){ 
    if (isAddStage){
        if (interactiveIndicateIndex === 1)
            sumAssign = true
        if (interactiveIndicateIndex === 2)
            sumAssignValue = 0
    }

    if (isSubStage) {
        if (interactiveIndicateIndex === 0)
            sumAssign = false
        if (interactiveIndicateIndex === 1)
            sumAssignValue = undefined
    }
}

function setMajorStageOfThree(interactiveIndicateIndex){ 
    if (isAddStage){
        if (interactiveIndicateIndex === 0){
            rangeTextConfig = {start: 1, end: Number.parseInt(nValue[0]), gap: 2}
            isAssginShow = false
            sumAddedAssign = false
            rangeVariableOfLoop = undefined
            sumAdded = [undefined, undefined, undefined]
            sumAddedAssign = false
            numberOfPrintInRange = (numberOfPrintInRange === -1 ) ?  0 : numberOfPrintInRange
            rangeKeyList = []
            for(let i = rangeTextConfig.start; i < rangeTextConfig.end + 1; i += rangeTextConfig.gap){
                rangeKeyList.push(i)
            }
                
        }
        if (interactiveIndicateIndex === 1){
            rangeConfig = rangeTextConfig
            rangeConfig.end += 1
            loopIndecateIndex = (loopIndecateIndex === undefined) ? 0 : loopIndecateIndex + 1
        }
        if (interactiveIndicateIndex === 2)
            isAssginShow = true
        if (interactiveIndicateIndex === 3)
            rangeVariableOfLoop = rangeKeyList[loopIndecateIndex]
    }

    if (isSubStage) {
        if (interactiveIndicateIndex === 0){
            loopIndecateIndex = (loopIndecateIndex === 0) ? undefined : loopIndecateIndex - 1
            if (loopIndecateIndex === undefined) rangeConfig = {start: 0, end: 0, gap: 0}
        }
        if (interactiveIndicateIndex === 1)
            isAssginShow = false
        if (interactiveIndicateIndex === 2)
            rangeVariableOfLoop = undefined
    }
}

function setMajorStageOfFour(interactiveIndicateIndex){   
    if (isAddStage){
        if (interactiveIndicateIndex === 1){
            sumAdded[0] = rangeVariableOfLoop
            sumAdded[1] = sumAssignValue
        }
        if (interactiveIndicateIndex === 2){
            sumAddedAssign = true
        }
        if (interactiveIndicateIndex === 3){
            sumAdded[2] = sumAdded[1] + sumAdded[0]
            sumAssignValue = sumAdded[2]
        }
    }
    if (isSubStage){
        if (interactiveIndicateIndex === 0){
            sumAdded[0] = undefined
            sumAdded[1] = undefined
        }
        if (interactiveIndicateIndex === 1){
            sumAddedAssign = false
        }
        if (interactiveIndicateIndex === 2){
            sumAdded[2] = undefined
            sumAssignValue = sumAssignValue - sumAdded[0]
        }
        if (interactiveIndicateIndex === 3){
            rangeVariableOfLoop = rangeKeyList[loopIndecateIndex]
            isAssginShow = true

            sumAdded[0] = rangeVariableOfLoop
            sumAdded[2] = sumAssignValue
            sumAdded[1] = sumAdded[2] - sumAdded[0]
            sumAddedAssign = true
        }
    }
}

function setMajorStageOfFive(interactiveIndicateIndex){
    if (isAddStage){
        if (interactiveIndicateIndex === 1){
            finalPrint = 0
            rangeKeyList.map((i) => {finalPrint = finalPrint + i})
        }
    }
    if (isSubStage){
        if (interactiveIndicateIndex === 0){
            finalPrint = undefined
        }
    }

}

export function getPrintResult(){
    return [<Row style={{height: '40px'}}><label className="py-1">{finalPrint}</label></Row>];
}

export function getProgramGrammar(){
    return (
        <div>
            <div style={{display: 'flex'}}>
                <span>for&nbsp;</span>
                <span className='text-center' style={{...elementOrigeBackgroundStyle, ...variableMinWidth}}>&nbsp;變數&nbsp;</span>
                <span>&nbsp;in&nbsp;</span>
                <div style={elementRangeBackgroundStyle}>&nbsp;range(起始直, 結束值, 遞增/遞減)&nbsp;</div>
            </div>
            <p className="my-0"><UpAssign className='px-1' height='20px'/><span style={{background: '#C9C9C9'}}>重複執行的指令</span><DownAssign className='px-1' height='20px'/></p>
        </div>
    )
}

export function getInputObject(sampleCodeIndicateIndex, interactiveIndicateIndex){
    return [
        {
            isShow: sampleCodeIndicateIndex > 1 || (sampleCodeIndicateIndex === 1 && interactiveIndicateIndex > 0),
            label: '請輸入n: ',
            defaultValue: 10,
            value: nValue[0],
            isActive: (sampleCodeIndicateIndex === 1 && interactiveIndicateIndex === 1),
            onChange(value) {nValue[0] = value},
            isValid(inputString) {
                const inputValue = Number.parseInt(inputString)
                if (isEmpty(inputString)) return {valid:false, errorMsg:'輸入欄位不能為空'};
                if (isNaN(inputValue)) return {valid:false, errorMsg:'請輸入數字'};
                if (inputValue < 0) return {valid:false, errorMsg:'輸入數字必須大於零'};
                return {valid:true, errorMsg:''}
            }
        },
    ]
}

function isEmpty(valid){
    return (valid === undefined) || (valid === '');
}
