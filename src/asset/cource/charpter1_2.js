export const courceTitle = `
    <p>請寫一個程式計算紀念小書包數量及單價 輸出總共多少元 ?</p>
    <p>輸入：紀念小書包數量(正整數) 紀念小書包單價(正整數) 輸出：總價 (正整數) <p>
    <p>(EX: 請輸入紀念小書包數量:10 請輸入紀念小書包單價:120 總價為1200元)<p>
`

export const programmingGrammar = `
    <p className="my-0"><span style="background: peachpuff;">變數</span> = int(input)</p>
    <p className="my-0"><span style="background: LightSkyBlue;">print(輸出內容)</span></p>
`

export const hintDialogContent = `
    <span style="fontWeight: bold;">程式碼第1-2行：</span>
    <p>透過input()函式，取得使用者所輸入的字串，為了使輸入的數量及單價呈數值資料，使用int()函式，從字串型別轉換為整數型別，再將整數 assign(指派) 給變數 bag 和 price </p>

    <span style="fontWeight: bold;">程式碼第3行：</span>
    <p>使用print()函式列印出「總價錢為」、「元」字串以及變數bag和 price的運算內容 </p>

`;

export const courceIndex = '單元範例-例題2'

export const sampleCode = [
    `<p className="my-0"><span style="background: peachpuff;">bag</span> = int(input("請輸入紀念小包數量:"))</p>`,
    `<p className="my-0"><span style="background: peachpuff;">price </span> = int(input("請輸入紀念小包單價:"))</p>`,
    `<p className="my-0"><span style="background: LightSkyBlue;">print("總價為",bag*price,"元")</span></p>`,
]

export const defaultCourceData = [
    {
        type: 'codeExplain',
        data: [
            {type: 'hint', lable:'變數名稱', content:'變數值'},
            {type: 'variable', lable:'bag', content:'10', showInTopicStep: 1, showInTeachStep: 5},
            {type: 'assign', showInTopicStep: 1, showInTeachStep: 4},
            {type: 'input', lable:'int()', content:'10', showInTopicStep: 1, showInTeachStep: 3},
            {type: 'assign', showInTopicStep: 1, showInTeachStep: 2},
            {type: 'input', lable:'input("請輸入紀念小包數量:")', content:'"10"', showInTopicStep: 1, showInTeachStep: 1},
            {type: 'hint', lable:'函式', content:'函數傳回值'},
        ],
    },
    {
        type: 'codeExplain',
        data: [
            {type: 'hint'},
            {type: 'variable', lable:'price', content:'120', showInTopicStep: 2, showInTeachStep: 5},
            {type: 'assign', showInTopicStep: 2, showInTeachStep: 4},
            {type: 'input', lable:'int()', content:'120', showInTopicStep: 2, showInTeachStep: 3},
            {type: 'assign', showInTopicStep: 2, showInTeachStep: 2},
            {type: 'input', lable:'input("請輸入紀念小包單價:")', content:'"120"', showInTopicStep: 2, showInTeachStep: 1},
            {type: 'hint'},
        ],
    },
    {
        type: 'printer',
        data: [
            {type: 'hint'},
            {type: 'text', lable:'', content:'print(總價為'},
            {type: 'text', lable:'', content:','},
            {type: 'variable', lable:'bag', content:'10', showInTopicStep: 3, showInTeachStep: 1},
            {type: 'text', lable:'', content:'*'},
            {type: 'variable', lable:'price', content:'120', showInTopicStep: 3, showInTeachStep: 1},
            {type: 'text', lable:'', content:',"元")'},
            {type: 'hint'},
        ]
    },
]

export const stateObject = {
    topicStep: 0,
    teachStep: 0,
    teachStepOfTopic: [0, 5, 5, 1]
}

export const inputObject = [
    {
        label: '請輸入紀念小書包數量：',
        content: '10', 
        invalidMsg: '輸入欄位不能為空',
        showInTopicStep: 1, 
        showInTeachStep: 0, 
        setData(courceData, value) { return setBagNumber(courceData, value)}, 
        isValid(value){ 
            if (isEmpty(value)) return {isValid:false, errorMsg:'輸入欄位不能為空'};
            if (isNaN(value)) return {isValid:false, errorMsg:'請輸入數字'};
            return {isValid:true, errorMsg:''}
        }
    },
    {
        label: '請輸入紀念小書包單價：', 
        content: '120', 
        invalidMsg: '輸入欄位不能為空', 
        showInTopicStep: 2, 
        showInTeachStep: 0, 
        setData(courceData, value) { return setPrice(courceData, value)}, 
        isValid(value){ 
            if (isEmpty(value)) return {isValid:false, errorMsg:'輸入欄位不能為空'};
            if (isNaN(value)) return {isValid:false, errorMsg:'請輸入數字'};
            return {isValid:true, errorMsg:''}
        }
    },
]

function setBagNumber(courceData, value){
    courceData[0].data[1].content = Number.parseInt(value);
    courceData[0].data[3].content = Number.parseInt(value);
    courceData[0].data[5].content = '"' + value + '"';
    courceData[2].data[3].content = Number.parseInt(value);
    return courceData;
}

function setPrice(courceData, value){
    courceData[1].data[1].content = Number.parseInt(value);
    courceData[1].data[3].content = Number.parseInt(value);
    courceData[1].data[5].content = '"' + value + '"';
    courceData[2].data[5].content = Number.parseInt(value);
    return courceData;
}

function isEmpty(valid){
    return (valid === undefined) || (valid === '');
}

export function getResult(courceData){
    return [
        {
            showInTopicStep: 3,
            showInTeachStep: 1, 
            print: "總價為 " + courceData[2].data[3].content * courceData[2].data[5].content + " 元"
        }
    ];
}