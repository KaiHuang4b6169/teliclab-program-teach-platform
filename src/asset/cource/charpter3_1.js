import { Row, Stack } from "react-bootstrap";
import { DownAssign, UpAssign } from "../images";
import { stageAddTools, stageSubTools } from "./common/commonTools";
import { elementOrigeBackgroundStyle, elementRangeBackgroundStyle, variableMinWidth } from "./common/courceCss";
import { getPrint, textProgramComponent, variableProgramComponent } from "./common/teachingAreaCommon";
import { getExplainOfVariableOfRangeLine, getRangeAsignArrowOfRangeLine, getRangeExplain, getRangeOfRangeLine, getRangeTags, getVariableOfRangeLine, interfaceOfRangeLine } from "./common/teachingAreaLoopCommon";

let rangeVariableOfLoop = undefined;
let rangeVariableOfPrint = undefined;
let loopIndecateIndex = undefined;
let isAssginShow = false
let numberOfPrintInRange = 0;

let isAddStage = undefined
let isSubStage = undefined
let rangeKeyList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
let rangeConfig = {start: 1, end: 11, gap: 1}

const numberOfEachStage = [0, 3, 1]

export const courceTitle = (
    <div>
        <p className="my-0">for 迴圈從1印到10</p>
    </div>
)

export const sampleCode = [
    (<div style={{display: 'flex'}}>
        <span>for&nbsp;</span>
        <span className='text-center' style={{...elementOrigeBackgroundStyle, ...variableMinWidth}}>i</span>
        <span>&nbsp;in&nbsp;</span>
        <div style={elementRangeBackgroundStyle}>range(1, 11, 1)</div>
    </div>),
    (<p className="my-0"><UpAssign className='px-1' height='20px'/><span style={{background: '#C9C9C9'}}>print(i)</span><DownAssign className='px-1' height='20px'/></p>),
]

export const programExplaiin = 
`程式碼第1-2行 : 
for迴圈可以重複相同的指令，直到執行完設定的次數，而range()函式可以很有效率地創建一個整數序列，用法為 (起始值, 結束值, 遞增/減值)，range(1, 11, 1)：產生從1, 2, 3, 4, 5, 6, 7, 8, 9, 10的整數序列(遞增值為1)。
`;

export function nextStage(sampleCodeIndicateIndex, interactiveIndicateIndex){
    isAddStage = true
    isSubStage = false
    if (numberOfPrintInRange >= 3) {
        numberOfPrintInRange = rangeKeyList.length
        return {sampleCodeIndicateIndex, interactiveIndicateIndex}
    }
    let {margeStage, minorSage} = stageAddTools(sampleCodeIndicateIndex, interactiveIndicateIndex, numberOfEachStage)
    margeStage = (margeStage > numberOfEachStage.length - 1) ? 1 : margeStage
    
    return {
        sampleCodeIndicateIndex : margeStage,
        interactiveIndicateIndex: minorSage
    };
}

export function priviousStage(sampleCodeIndicateIndex, interactiveIndicateIndex){
    isAddStage = false
    isSubStage = true
    if (numberOfPrintInRange > 3) {
        numberOfPrintInRange = 3
        return {sampleCodeIndicateIndex, interactiveIndicateIndex}
    }
    let {margeStage, minorSage} = stageSubTools(sampleCodeIndicateIndex, interactiveIndicateIndex, numberOfEachStage)
    const isMinStage = (margeStage === 0) && (minorSage === 0)
    if (loopIndecateIndex !== undefined && isMinStage) {
        margeStage = numberOfEachStage.length - 1
        minorSage = numberOfEachStage[numberOfEachStage.length - 1]
    }

    return {
        sampleCodeIndicateIndex : margeStage,
        interactiveIndicateIndex: minorSage
    };
}

export function courceTeachingArea(sampleCodeIndicateIndex, interactiveIndicateIndex){
    if (sampleCodeIndicateIndex === 0) setMajorStageOfZero(interactiveIndicateIndex)
    if (sampleCodeIndicateIndex === 1) setMajorStageOfOne(interactiveIndicateIndex)
    if (sampleCodeIndicateIndex === 2) setMajorStageOfTwo(interactiveIndicateIndex)


    return (
        <div>
            <Stack className="my-1" direction="horizontal">
                {getRangeTags(1, 11, 1)}
                {getRangeExplain(['迴圈變數', '範圍設定'])}
            </Stack>

            <Stack direction="horizontal">
                {getExplainOfVariableOfRangeLine()}
                {getVariableOfRangeLine('i', rangeVariableOfLoop)}
                {getRangeAsignArrowOfRangeLine(isAssginShow)}
                {getRangeOfRangeLine(rangeConfig.start, rangeConfig.end, rangeConfig.gap, loopIndecateIndex)}
                {interfaceOfRangeLine(undefined, getRangeExplain(['','迴圈變數', 'list']), undefined)}
            </Stack>

            <Stack className="justify-content-center" direction="horizontal">
                {getPrint(
                    [
                        textProgramComponent('print('),
                        variableProgramComponent('i', rangeVariableOfPrint),
                        textProgramComponent(')'),
                    ]
                )}
            </Stack>
        </div>
    );
}

function setMajorStageOfZero(){ 
    rangeConfig = {start: 0, end: 0, gap: 0}
    rangeVariableOfLoop = undefined;
    rangeVariableOfPrint = undefined;
    loopIndecateIndex = undefined;
    isAssginShow = false
    numberOfPrintInRange = 0;
}

function setMajorStageOfOne(interactiveIndicateIndex){ 
    if (isAddStage){
        if (interactiveIndicateIndex === 0){
            rangeConfig = {start: 1, end: 11, gap: 1}
            isAssginShow = false
            rangeVariableOfLoop = undefined
            rangeVariableOfPrint = undefined
        }
        if (interactiveIndicateIndex === 1)
            loopIndecateIndex = (loopIndecateIndex === undefined) ? 0 : loopIndecateIndex + 1
        if (interactiveIndicateIndex === 2)
            isAssginShow = true
        if (interactiveIndicateIndex === 3)
            rangeVariableOfLoop = rangeKeyList[loopIndecateIndex]
    }
    if (isSubStage) {
        if (interactiveIndicateIndex === 0){
            loopIndecateIndex = (loopIndecateIndex === 0) ? undefined : loopIndecateIndex - 1
        }
        if (interactiveIndicateIndex === 1)
            isAssginShow = false
        if (interactiveIndicateIndex === 2)
            rangeVariableOfLoop = undefined
    }
}

function setMajorStageOfTwo(interactiveIndicateIndex){   
    if (isAddStage){
        if (interactiveIndicateIndex === 1){
            rangeVariableOfPrint = rangeVariableOfLoop
            numberOfPrintInRange += 1
        }
    }
    if (isSubStage){
        if (interactiveIndicateIndex === 0){
            numberOfPrintInRange = rangeKeyList.indexOf(rangeVariableOfPrint)
            rangeVariableOfPrint = undefined
        }
        if (interactiveIndicateIndex === 1){
            rangeVariableOfPrint = rangeKeyList[numberOfPrintInRange - 1]
            rangeVariableOfLoop = rangeVariableOfPrint
            isAssginShow = true
        }
    }
}

function generatePrintResult(){
    let printResult = []
    rangeKeyList.map((element, key) => {
        if (numberOfPrintInRange > key ) printResult.push(<Row style={{height: '40px'}}><label className="py-1">{element}</label></Row>)
    })
    return printResult;
}

export function getPrintResult(interactiveIndicateIndex){
    return generatePrintResult();
}

export function getProgramGrammar(){
    return (
        <div>
            <div style={{display: 'flex'}}>
                <span>for&nbsp;</span>
                <span className='text-center' style={{...elementOrigeBackgroundStyle, ...variableMinWidth}}>&nbsp;變數&nbsp;</span>
                <span>&nbsp;in&nbsp;</span>
                <div style={elementRangeBackgroundStyle}>&nbsp;range(起始直, 結束值, 遞增/遞減)&nbsp;</div>
            </div>
            <p className="my-0"><UpAssign className='px-1' height='20px'/><span style={{background: '#C9C9C9'}}>重複執行的指令</span><DownAssign className='px-1' height='20px'/></p>
        </div>
    )
}

export function getInputObject(sampleCodeIndicateIndex, interactiveIndicateIndex){return []}
