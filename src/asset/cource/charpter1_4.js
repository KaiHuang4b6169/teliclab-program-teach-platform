export const courceTitle = `
    <p>設計一個計算新台幣兌換日圓的程式。</p>
    <p>匯率：1新台幣=3.5日圓，手續費：5% </p>
    <p>(EX:請輸入新台幣:10000 新台幣10000共可換日圓33250)</p>

`

export const programmingGrammar = `
    <p className="my-0"><span style="background: peachpuff;">變數</span> = int(input)</p>
    <p className="my-0"><span style="background: LightSkyBlue;">print(輸出內容)</span></p>
`

export const hintDialogContent = `
    <span style="fontWeight: bold;">程式碼第1行 : </span>
    <p>先透過 input() 函式，取得使用者所輸入的字串，為了使輸入的金額呈數值資料，使用 int() 函式，從字串型別轉換為整數型別，並assign(指派)給變數TWD，</p>
    
    <span style="fontWeight: bold;">程式碼第2行 :</span> 
    <p>使用print() 函式列印出「新台幣」、「元」、「共可換日圓」、「元」字串以及TWD的運算內容</p>    

`;

export const courceIndex = '單元範例-例題4'

export const sampleCode = [
    `<p className="my-0"><span style="background: peachpuff;">TWD</span> = int(input("請輸入新台幣:"))</p>`,
    `<p className="my-0"><span style="background: LightSkyBlue;">print("新台幣",TWD,"元","共可換日圓",TWD*3.5*0.95,"元")</span></p>`,
]

export const defaultCourceData = [
    {
        type: 'codeExplain',
        data: [
            {type: 'hint', lable:'變數名稱', content:'變數值'},
            {type: 'variable', lable:'TWD', content:'10000', showInTopicStep: 1, showInTeachStep: 5},
            {type: 'assign', showInTopicStep: 1, showInTeachStep: 4},
            {type: 'input', lable:'int()', content:'10000', showInTopicStep: 1, showInTeachStep: 3},
            {type: 'assign', showInTopicStep: 1, showInTeachStep: 2},
            {type: 'input', lable:'input("請輸入新台幣:")', content:'"10000"', showInTopicStep: 1, showInTeachStep: 1},
            {type: 'hint', lable:'函式', content:'函數傳回值'},
        ],
    },
    {
        type: 'printer',
        data: [
            {type: 'hint'},
            {type: 'text', lable:'', content:'print("新台幣",'},
            {type: 'variable', lable:'TWD', content:'10000', showInTopicStep: 2, showInTeachStep: 1},
            {type: 'text', lable:'', content:',"元","共可換日圓",'},
            {type: 'variable', lable:'TWD', content:'10000', showInTopicStep: 2, showInTeachStep: 1},
            {type: 'text', lable:'', content:'*3.5*0.95,"元")'},
            {type: 'hint'},
        ]
    },
]

export const stateObject = {
    topicStep: 0,
    teachStep: 0,
    teachStepOfTopic: [0, 5, 1]
}

export const inputObject = [
    {
        label: '請輸入新台幣：',
        content: '10000', 
        invalidMsg: '輸入欄位不能為空',
        showInTopicStep: 1, 
        showInTeachStep: 0, 
        setData(courceData, value) { return setTWD(courceData, value)}, 
        isValid(value){ 
            if (isEmpty(value)) return {isValid:false, errorMsg:'輸入欄位不能為空'};
            if (isNaN(value)) return {isValid:false, errorMsg:'請輸入數字'};
            return {isValid:true, errorMsg:''}
        }
    },
]

function setTWD(courceData, value){
    courceData[0].data[1].content = Number.parseInt(value);
    courceData[0].data[3].content = Number.parseInt(value);
    courceData[0].data[5].content = '"' + value + '"';
    courceData[1].data[2].content = Number.parseInt(value);
    return courceData;
}

function isEmpty(valid){
    return (valid === undefined) || (valid === '');
}

export function getResult(courceData){
    const twd = Number.parseInt(courceData[1].data[2].content);
    return [
        {
            showInTopicStep: 2,
            showInTeachStep: 1, 
            print: "新台幣 " + twd + " 元 共可換日圓 " + twd*3.5*0.95 + " 元"
        }
    ];
}