export const courceTitle = `
    <p>請寫一個程式來幫忙計算梯形面積，分別輸入上底、下底、高之後，計算並輸出梯形面積。<p>
    <p>(EX: 輸入-請輸入上底：5 請輸入下底：12 請輸入高：4 輸出-梯形面積為34.0)<p>
`

export const programmingGrammar = `
    <p className="my-0"><span style="background: peachpuff;">變數</span> = int(input)</p>
    <p className="my-0"><span style="background: LightSkyBlue;">print(輸出內容)</span></p>
`

export const hintDialogContent = `
    <span style="fontWeight: bold;">程式碼第1-3行：</span>
    <p>透過input()函式，取得使用者所輸入的字串，為了使輸入的上底長、下底長及高度呈數值資料，使用int()函式，從字串型別轉換為整數型別，再將整數 assign(指派) 給變數 up 、 down 和 high</p>

    <span style="fontWeight: bold;">程式碼第4行：</span>
    <p>使用print()函式列印出「梯形面積」字串以及變數up 、 down 和 high的運算內容</p>


`;

export const courceIndex = '單元範例-例題3'

export const sampleCode = [
    `<p className="my-0"><span style="background: peachpuff;">up</span> = int(input("請輸入上底:"))</p>`,
    `<p className="my-0"><span style="background: peachpuff;">down</span> = int(input("請輸入下底:"))</p>`,
    `<p className="my-0"><span style="background: peachpuff;">high</span> = int(input("請輸入高:"))</p>`,
    `<p className="my-0"><span style="background: LightSkyBlue;">print("梯形面積為",(up+down)*high/2)</span></p>`,
]

export const defaultCourceData = [
    {
        type: 'codeExplain',
        data: [
            {type: 'hint', lable:'變數名稱', content:'變數值'},
            {type: 'variable', lable:'up', content:'5', showInTopicStep: 1, showInTeachStep: 5},
            {type: 'assign', showInTopicStep: 1, showInTeachStep: 4},
            {type: 'input', lable:'int()', content:'5', showInTopicStep: 1, showInTeachStep: 3},
            {type: 'assign', showInTopicStep: 1, showInTeachStep: 2},
            {type: 'input', lable:'input("請輸入上底:")', content:'"5"', showInTopicStep: 1, showInTeachStep: 1},
            {type: 'hint', lable:'函式', content:'函數傳回值'},
        ],
    },
    {
        type: 'codeExplain',
        data: [
            {type: 'hint'},
            {type: 'variable', lable:'down', content:'12', showInTopicStep: 2, showInTeachStep: 5},
            {type: 'assign', showInTopicStep: 2, showInTeachStep: 4},
            {type: 'input', lable:'int()', content:'12', showInTopicStep: 2, showInTeachStep: 3},
            {type: 'assign', showInTopicStep: 2, showInTeachStep: 2},
            {type: 'input', lable:'int(input("請輸入下底:"))', content:'"12"', showInTopicStep: 2, showInTeachStep: 1},
            {type: 'hint'},
        ],
    },
    {
        type: 'codeExplain',
        data: [
            {type: 'hint'},
            {type: 'variable', lable:'high', content:'4', showInTopicStep: 3, showInTeachStep: 5},
            {type: 'assign', showInTopicStep: 3, showInTeachStep: 4},
            {type: 'input', lable:'int()', content:'4', showInTopicStep: 3, showInTeachStep: 3},
            {type: 'assign', showInTopicStep: 3, showInTeachStep: 2},
            {type: 'input', lable:'int(input("請輸入高:"))', content:'"4"', showInTopicStep: 3, showInTeachStep: 1},
            {type: 'hint'},
        ],
    },
    {
        type: 'printer',
        data: [
            {type: 'hint'},
            {type: 'text', lable:'', content:'print("梯形面積為",('},
            {type: 'variable', lable:'up', content:'5', showInTopicStep: 4, showInTeachStep: 1},
            {type: 'text', lable:'', content:'+'},
            {type: 'variable', lable:'down', content:'12', showInTopicStep: 4, showInTeachStep: 1},
            {type: 'text', lable:'', content:')*'},
            {type: 'variable', lable:'high', content:'4', showInTopicStep: 4, showInTeachStep: 1},
            {type: 'text', lable:'', content:'/2)'},
            {type: 'hint'},
        ]
    },
]

export const stateObject = {
    topicStep: 0,
    teachStep: 0,
    teachStepOfTopic: [0, 5, 5, 5, 1]
}

export const inputObject = [
    {
        label: '請輸入上底：',
        content: '5', 
        invalidMsg: '輸入欄位不能為空',
        showInTopicStep: 1, 
        showInTeachStep: 0, 
        setData(courceData, value) { return setUp(courceData, value)}, 
        isValid(value){ 
            if (isEmpty(value)) return {isValid:false, errorMsg:'輸入欄位不能為空'};
            if (isNaN(value)) return {isValid:false, errorMsg:'請輸入數字'};
            return {isValid:true, errorMsg:''}
        }
    },
    {
        label: '請輸入下底：', 
        content: '12', 
        invalidMsg: '輸入欄位不能為空', 
        showInTopicStep: 2, 
        showInTeachStep: 0, 
        setData(courceData, value) { return setDown(courceData, value)}, 
        isValid(value){ 
            if (isEmpty(value)) return {isValid:false, errorMsg:'輸入欄位不能為空'};
            if (isNaN(value)) return {isValid:false, errorMsg:'請輸入數字'};
            return {isValid:true, errorMsg:''}
        }
    },
    {
        label: '請輸入高：', 
        content: '4', 
        invalidMsg: '輸入欄位不能為空', 
        showInTopicStep: 3, 
        showInTeachStep: 0, 
        setData(courceData, value) { return setHigh(courceData, value)}, 
        isValid(value){ 
            if (isEmpty(value)) return {isValid:false, errorMsg:'輸入欄位不能為空'};
            if (isNaN(value)) return {isValid:false, errorMsg:'請輸入數字'};
            return {isValid:true, errorMsg:''}
        }
    },
]

function setUp(courceData, value){
    courceData[0].data[1].content = Number.parseInt(value);
    courceData[0].data[3].content = Number.parseInt(value);
    courceData[0].data[5].content = '"' + value + '"';
    courceData[3].data[2].content = Number.parseInt(value);
    return courceData;
}

function setDown(courceData, value){
    courceData[1].data[1].content = Number.parseInt(value);
    courceData[1].data[3].content = Number.parseInt(value);
    courceData[1].data[5].content = '"' + value + '"';
    courceData[3].data[4].content = Number.parseInt(value);
    return courceData;
}

function setHigh(courceData, value){
    courceData[2].data[1].content = Number.parseInt(value);
    courceData[2].data[3].content = Number.parseInt(value);
    courceData[2].data[5].content = '"' + value + '"';
    courceData[3].data[6].content = Number.parseInt(value);
    return courceData;
}

function isEmpty(valid){
    return (valid === undefined) || (valid === '');
}

export function getResult(courceData){
    const up = Number.parseInt(courceData[3].data[2].content);
    const down = Number.parseInt(courceData[3].data[4].content);
    const high = Number.parseInt(courceData[3].data[6].content);
    return [
        {
            showInTopicStep: 4,
            showInTeachStep: 1, 
            print: "梯形面積為 " + (up + down) * high /2
        }
    ];
}