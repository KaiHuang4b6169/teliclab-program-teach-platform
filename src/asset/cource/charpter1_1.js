export const courceTitle = '<h1>輸入姓名、座位，顯示姓名座號</h1> <div>(EX: 王小明 01號)</div>'

export const programmingGrammar = `
    <p className="my-0"><span style="background: peachpuff;">變數</span> = int(input)</p>
    <p className="my-0"><span style="background: LightSkyBlue;">print(輸出內容)</span></p>
`

export const hintDialogContent = `
    <p>不同資料型別有不同的表示方式</p>

    <span style="fontWeight: bold;">程式碼第1行：</span>
    <p>透過input()函式，取得使用者所輸入的字串 ，再將所輸入的字串 assign(指派) 給變數YourName</p>

    <span style="fontWeight: bold;">程式碼第2行：</span>
    <p>透過input()函式，取得使用者所輸入的字串，為了使輸入的座號呈數值資料，使用int()函式，從字串型別轉換為整數型別，再將整數 assign(指派) 給變數YourNumber</p>

    <span style="fontWeight: bold;">程式碼第3行：</span>
    <p>透過print()函式列印出變數YourName和YourNumber內容</p>
`;

export const courceIndex = '單元範例-例題1'

export const sampleCode = [
    `<p className="my-0"><span style="background: peachpuff;">yourName</span> = input("請輸入姓名")</p>`,
    `<p className="my-0"><span style="background: peachpuff;">yourNumber</span> = int(input("請輸入座號"))</p>`,
    `<p className="my-0"><span style="background: LightSkyBlue;">print(yourName, yourNumber, "號")</span></p>`,
]

export const defaultCourceData = [
    {
        type: 'codeExplain',
        data: [
            {type: 'hint', lable:'變數名稱', content:'變數值'},
            {type: 'variable', lable:'yourName', content:'"王小明"', showInTopicStep: 1, showInTeachStep: 3},
            {type: 'assign' , showInTopicStep: 1, showInTeachStep: 2},
            {type: 'input', lable:'input("請輸入姓名")', content:'"王小明"', showInTopicStep: 1, showInTeachStep: 1 },
            {type: 'hint', lable:'函式', content:'函數傳回值'},
        ],
    },
    {
        type: 'codeExplain',
        data: [
            {type: 'hint'},
            {type: 'variable', lable:'yourNumber', content:'1', showInTopicStep: 2, showInTeachStep: 5},
            {type: 'assign', showInTopicStep: 2, showInTeachStep: 4},
            {type: 'input', lable:'int()', content:'1', showInTopicStep: 2, showInTeachStep: 3},
            {type: 'assign', showInTopicStep: 2, showInTeachStep: 2},
            {type: 'input', lable:'input("請輸入座號")', content:'"01"', showInTopicStep: 2, showInTeachStep: 1},
            {type: 'hint'},
        ],
    },
    {
        type: 'printer',
        data: [
            {type: 'hint'},
            {type: 'text', lable:'', content:'print('},
            {type: 'variable', lable:'yourName', content:'"王小明"', showInTopicStep: 3, showInTeachStep: 1},
            {type: 'text', lable:'', content:','},
            {type: 'variable', lable:'yourNumber', content:'1', showInTopicStep: 3, showInTeachStep: 1},
            {type: 'text', lable:'', content:',"號")'},
            {type: 'hint'},
        ]
    },
]

export const stateObject = {
    topicStep: 0,
    teachStep: 0,
    teachStepOfTopic: [0, 3, 5, 1]
}

export const inputObject = [
    {
        label: '請輸入姓名：',
        content: '王小明', 
        invalidMsg: '輸入欄位不能為空',
        showInTopicStep: 1, 
        showInTeachStep: 0, 
        setData(courceData, value) { return setName(courceData, value)}, 
        isValid(value){ 
            if (isEmpty(value)) return {isValid:false, errorMsg:'輸入欄位不能為空'};
            return {isValid:true, errorMsg:''}
        }
    },
    {
        label: '請輸入座號：', 
        content: '01', 
        invalidMsg: '輸入欄位不能為空', 
        showInTopicStep: 2, 
        showInTeachStep: 0, 
        setData(courceData, value) { return setNumber(courceData, value)}, 
        isValid(value){ 
            if (isEmpty(value)) return {isValid:false, errorMsg:'輸入欄位不能為空'};
            if (isNaN(value)) return {isValid:false, errorMsg:'請輸入數字'};
            return {isValid:true, errorMsg:''}
        }
    },
]

function setName(courceData, value){
    courceData[0].data[1].content = '"' + value + '"';
    courceData[0].data[3].content = '"' + value + '"';
    courceData[2].data[2].content = '"' + value + '"';
    return courceData;
}

function setNumber(courceData, value){
    courceData[1].data[1].content = Number.parseInt(value);
    courceData[1].data[3].content = Number.parseInt(value);
    courceData[1].data[5].content = '"' + value + '"';
    courceData[2].data[4].content = Number.parseInt(value);
    return courceData;
}

function isEmpty(valid){
    return (valid === undefined) || (valid === '');
}

export function getResult(courceData){
    const name = courceData[2].data[2].content;

    return [
        {
            showInTopicStep: 3,
            showInTeachStep: 1, 
            print: name.substring(1, name.length-1) + " " + courceData[2].data[4].content + " 號"
        }
    ];
}