import { Row, Stack } from "react-bootstrap";
import { DownAssign, UpAssign, WhileFalse, WhileGrammar, WhileTrue } from "../images";
import { stageAddTools, stageSubTools } from "./common/commonTools";
import { elementOrigeBackgroundStyle, elementRangeBackgroundStyle, elementWhiteBackgroundStyle, middleStyle, variableMinWidth } from "./common/courceCss";
import { assignProgramComponent, explainProgramComponent, getPrint, getWhileBlock, inputProgramComponent, interfaceProgramComponent, textProgramComponent, variableProgramComponent } from "./common/teachingAreaCommon";
import { getExplainOfVariableOfRangeLine, getRangeAsignArrowOfRangeLine, getRangeExplain, getRangeOfRangeLine, getRangeTags, getVariableOfRangeLine, interfaceOfRangeLine } from "./common/teachingAreaLoopCommon";


let nValue = ['', undefined, undefined, undefined]
let nValueAssign = [undefined, undefined]

let iValue = [undefined, undefined, undefined, undefined]
let iValueAssign = undefined

let sumValue = [undefined, undefined, undefined]
let sumValueAssign = undefined
let isConditionTrue = undefined

let scoreValue = ['', undefined, undefined, undefined, undefined]
let scoreValueAssign = [undefined, undefined]
let iLoopValue =  [undefined, undefined]
let iLoopValueAssign = undefined
let dymanicTempInputValue = undefined
let allInputScoreValue = []
let dynamicInputObject = []

let finalPrint = [undefined, undefined, undefined]

let isShowDownAssign = false
let isShowUpAssign = false
let isAddStage = undefined
let isSubStage = undefined

let defaultInputScore = [90, 89, 88, 87, 86]
const numberOfLoopOfState = [3, 6, 3, 3]
const numberOfPrintOfState = [2]
let numberOfEachStage = [0, 6, 2, 2, ...numberOfLoopOfState, ...numberOfPrintOfState]

export const courceTitle = (
    <div>
        <p className="my-0">while迴圈</p>
        <p className="my-0">(1)請輸入幾次考試?例如:5次</p>
        <p className="my-0">(2)則會輸入幾次考試成績，並計算平均成績。(或輸入十次，須出現十個輸入)</p>
        <p className="my-0">(EX： 輸入: 請輸入幾次考試? 5 考試成績? 90 考試成績? 89 考試成績? 88 考試成績? 87 考試成績? 86 輸出：平均為88分)</p>
    </div>
)

export const sampleCode = [
    (<div className="py-1" style={{display: 'flex'}}>
        <span className='text-center'  style={{...elementOrigeBackgroundStyle, ...variableMinWidth}}>n</span>
        <span>&nbsp;=&nbsp;int(input("請輸入幾次考試:"))</span>
    </div>),
    (<div className="py-1" style={{display: 'flex'}}>
        <span className='text-center'  style={{...elementOrigeBackgroundStyle, ...variableMinWidth}}>i</span>
        <span>&nbsp;=&nbsp;0</span>
    </div>),
    (<div className="py-1" style={{display: 'flex'}}>
        <span className='text-center'  style={{...elementOrigeBackgroundStyle, ...variableMinWidth}}>sum</span>
        <span>&nbsp;=&nbsp;0</span>
    </div>),
    (<div  className="py-1" style={{display: 'flex'}}>
        <div style={{background: '#D1A4CA'}}>
            while(
            <span style={{background: 'white'}}>{'(i<n)'}</span>
            ):
        </div>
    </div>),
    (<p className="py-1 my-0">&nbsp;&nbsp;&nbsp;&nbsp;<span style={{background: '#C9C9C9'}}>score = int(input("考試成績:"))</span></p>),
    (<p className="py-1 my-0">&nbsp;&nbsp;&nbsp;&nbsp;<span style={{background: '#C9C9C9'}}>sum = sum + score</span></p>),
    (<p className="py-1 my-0">&nbsp;&nbsp;&nbsp;&nbsp;<span style={{background: '#C9C9C9'}}>i = i + 1</span></p>),
    (<p className="py-1 my-0"><span style={{background: 'LightSkyBlue'}}>print("平均為",sum/n)</span></p>),
]

export const programExplaiin = 
`程式碼第1行 : 
透過input()函式，取得使用者所輸入的字串 ，再將所輸入的字串 assign(指派) 給變數 n

程式碼第2行:
初紹化變數i的值為0，即由0開始(用作判斷while是否需要繼續執行的變數)

程式碼第3行:
把儲存答案的變數sum初紹化為0

程式碼第4-7行:
透過while()函式，執行相同的指令，直到不符合條件式(i < n)，即會跳出迴圈。

在迴圈中執行的指令:
    1. 讓使用者可以輸入每次的分數，所以會透過input()函式事入字串(再使用Int()把字串更改為int類型的變數)，將所輸入的字串 assign(指派) 給變數 score。
    2. 把score的值加到sum。
    3. 更新i的值。

程式碼第8行:
透過print()函式，輸出變數sum的值。`;


const firstLoopLine = 4
const lastLoopLine = 7
export function nextStage(sampleCodeIndicateIndex, interactiveIndicateIndex){
    isAddStage = true
    isSubStage = false
    let newState = {margeStage: undefined, minorSage:undefined}
    let isNextStageOfLoop = sampleCodeIndicateIndex === (lastLoopLine+1)
    let isLastLoopState = sampleCodeIndicateIndex === lastLoopLine && interactiveIndicateIndex === numberOfEachStage[lastLoopLine]

    // if (isLastLoopState && !(Number.parseInt(nValue[0]) === allInputScoreValue.length)) newState = {margeStage: firstLoopLine, minorSage: 0}
    if(isConditionTrue === false && !isNextStageOfLoop) newState = {margeStage: 8, minorSage: 0}
    else if (isLastLoopState) newState = {margeStage: firstLoopLine, minorSage: 0}
    else newState = stageAddTools(sampleCodeIndicateIndex, interactiveIndicateIndex, numberOfEachStage)

    return {
        sampleCodeIndicateIndex : newState.margeStage,
        interactiveIndicateIndex: newState.minorSage
    };
}

export function priviousStage(sampleCodeIndicateIndex, interactiveIndicateIndex){
    isAddStage = false
    isSubStage = true
    let newState = {margeStage: undefined, minorSage:undefined}
    let isFirstLoopState = sampleCodeIndicateIndex === firstLoopLine && interactiveIndicateIndex === 0
    let isNextStageOfLoop = sampleCodeIndicateIndex === (lastLoopLine+1) && interactiveIndicateIndex === 0

    if (isNextStageOfLoop) newState = {margeStage: firstLoopLine, minorSage: numberOfEachStage[firstLoopLine]}
    else if (isFirstLoopState && allInputScoreValue.length!==0) newState = {margeStage: lastLoopLine, minorSage: numberOfEachStage[lastLoopLine]}
    else newState = stageSubTools(sampleCodeIndicateIndex, interactiveIndicateIndex, numberOfEachStage)

    return {
        sampleCodeIndicateIndex : newState.margeStage,
        interactiveIndicateIndex: newState.minorSage
    };
}

export function courceTeachingArea(sampleCodeIndicateIndex, interactiveIndicateIndex){

    if (sampleCodeIndicateIndex === 0) setMajorStageOfZero(interactiveIndicateIndex)
    if (sampleCodeIndicateIndex === 1) setMajorStageOfOne(interactiveIndicateIndex)
    if (sampleCodeIndicateIndex === 2) setMajorStageOfTwo(interactiveIndicateIndex)
    if (sampleCodeIndicateIndex === 3) setMajorStageOfThree(interactiveIndicateIndex)

    if (sampleCodeIndicateIndex === 4) setMajorStageOfFour(interactiveIndicateIndex)
    if (sampleCodeIndicateIndex === 5) setMajorStageOfFive(interactiveIndicateIndex)
    if (sampleCodeIndicateIndex === 6) setMajorStageOfSix(interactiveIndicateIndex)
    if (sampleCodeIndicateIndex === 7) setMajorStageOfSiven(interactiveIndicateIndex)

    if (sampleCodeIndicateIndex === 8) setMajorStageOfEight(interactiveIndicateIndex)

    const assignStyle = {minWidth: '66px'}
    const whileTrueNonUndifiend = (isConditionTrue) ? <WhileTrue style={{height: '40px'}}/> : <WhileFalse style={{height: '40px'}}/>
    const whileTrue = (isConditionTrue === undefined) ? undefined : whileTrueNonUndifiend
    const upAssign = (isShowUpAssign) ?  <UpAssign style={{height: '36px'}}/> : undefined
    const downAssign = (isShowDownAssign) ? <DownAssign style={{height: '36px'}}/> : undefined
    

    const assignTeachArea = (
        <div>
            <Stack className="my-1 justify-content-evenly" direction="horizontal">
                {explainProgramComponent('變數名稱', '變數值')}
                {variableProgramComponent('n', nValue[3])}
                {assignProgramComponent(nValueAssign[1])}
                {variableProgramComponent('int()', nValue[2])}
                {assignProgramComponent(nValueAssign[0])}
                {inputProgramComponent('input("請輸入幾次考試:")', nValue[1])}
                {explainProgramComponent('函數', '函數回傳值')}
            </Stack>
            <Stack className="my-1 justify-content-evenly" direction="horizontal">
                {explainProgramComponent('', '')}
                {explainProgramComponent('', '')}
                {variableProgramComponent('i', iValue[0])}
                {assignProgramComponent(iValueAssign)}
                {textProgramComponent(' 0 ')}
                {explainProgramComponent('', '')}
                {explainProgramComponent('', '')}
            </Stack>
            <Stack className="my-1 justify-content-evenly" direction="horizontal">
                {explainProgramComponent('', '')}
                {explainProgramComponent('', '')}
                {variableProgramComponent('sum', sumValue[0])}
                {assignProgramComponent(sumValueAssign)}
                {textProgramComponent(' 0 ')}
                {explainProgramComponent('', '')}
                {explainProgramComponent('', '')}
            </Stack>
        </div>    
    )

    const loopTeachingArea = (
        <div>
            <Stack direction="horizontal" className="justify-content-evenly">
                <Stack direction="horizontal">
                    {explainProgramComponent('變數名稱')}
                    {variableProgramComponent('i', iValue[2])}
                </Stack>


                <Stack direction="horizontal">
                    {getWhileBlock('while條件運算值', (
                        <Stack direction="horizontal" className='px-1'>                            
                            {textProgramComponent(' ', {...elementWhiteBackgroundStyle, minWidth: '4px'}, 'py-1')}
                            {textProgramComponent('( ', elementWhiteBackgroundStyle, 'py-1')}
                            {interfaceProgramComponent('i',<div style={{...elementOrigeBackgroundStyle, minWidth: '20px',  minHeight: '20px'}}>{iValue[3]}</div>, {}, {}, {...variableMinWidth, height: '36px', ...elementWhiteBackgroundStyle}, '')}
                            {textProgramComponent(' ) < ', elementWhiteBackgroundStyle, 'py-1')}
                            {interfaceProgramComponent('n',<div style={{...elementOrigeBackgroundStyle, minWidth: '20px',  minHeight: '20px'}}>{nValue[3]}</div>, {}, {}, {...variableMinWidth, height: '36px', ...elementWhiteBackgroundStyle}, '')}
                            {textProgramComponent(' ', {...elementWhiteBackgroundStyle, minWidth: '4px'}, 'py-1')}
                        </Stack>
                    ))}
                    
                    {interfaceProgramComponent('', whileTrue, {minWidth: '100px'})}
                </Stack>
            </Stack>

            <Stack className="mb-1 justify-content-center" direction="horizontal">
                <Stack direction="horizontal">
                    {interfaceProgramComponent('', undefined, assignStyle)}
                    {getPrint(
                        [
                            variableProgramComponent('score', scoreValue[3]),
                            assignProgramComponent(scoreValueAssign[1]),
                            variableProgramComponent('int()', scoreValue[2]),
                            assignProgramComponent(scoreValueAssign[0]),
                            inputProgramComponent('input("考試成績:")', scoreValue[1]),
                        ],                    
                        {width: '320px'}
                    )}
                    {interfaceProgramComponent('', downAssign, assignStyle)}
                </Stack>
            </Stack>

            <Stack className="mb-1 justify-content-center" direction="horizontal">
                
                <Stack direction="horizontal">
                    {interfaceProgramComponent('', upAssign, assignStyle)}
                    {getPrint(
                        [
                            variableProgramComponent('sum', sumValue[2]),
                            assignProgramComponent(sumValueAssign),
                            variableProgramComponent('sum', sumValue[1]),
                            textProgramComponent('+'),
                            variableProgramComponent('score', scoreValue[4]),
                        ],                      
                        {width: '320px'}
                    )}
                    {interfaceProgramComponent('', undefined, assignStyle)}
                </Stack>
            </Stack>

            <Stack className="mb-1 justify-content-center" direction="horizontal">
                
                <Stack direction="horizontal">
                    {interfaceProgramComponent('', upAssign, assignStyle)}
                    {getPrint(
                        [
                            variableProgramComponent('i', iLoopValue[1]),
                            assignProgramComponent(iLoopValueAssign),
                            variableProgramComponent('i', iLoopValue[0]),
                            textProgramComponent(' + 1'),
                        ],                   
                        {width: '320px'}
                    )}
                    {interfaceProgramComponent('', undefined, assignStyle)}
                </Stack>
            </Stack>
        </div>
    )

    const printTeachArea = (
        <div>            
            <Stack className="justify-content-center" direction="horizontal">
                {getPrint(
                    [
                        textProgramComponent('print("平均為", '),
                        variableProgramComponent('sum', finalPrint[1]),
                        textProgramComponent('/'),
                        variableProgramComponent('n', finalPrint[0]),
                        textProgramComponent(')'),
                    ], {background: 'LightSkyBlue'}
                )}
            </Stack>
        </div>
    )

    if (sampleCodeIndicateIndex < 4) return assignTeachArea    
    if (sampleCodeIndicateIndex < 8)  return loopTeachingArea
    return printTeachArea
}

function setMajorStageOfZero(){ 
    nValue = ['', undefined, undefined, undefined]
    nValueAssign = [undefined, undefined]

    iValue = [undefined, undefined, undefined, undefined]
    iValueAssign = undefined

    sumValue = [undefined, undefined, undefined]
    sumValueAssign = undefined
    isConditionTrue = undefined

    scoreValue = ['', undefined, undefined, undefined, undefined]
    scoreValueAssign = [undefined, undefined]
    iLoopValue =  [undefined, undefined]
    iLoopValueAssign = undefined
    dymanicTempInputValue = undefined
    allInputScoreValue = []
    dynamicInputObject = []

    finalPrint = [undefined, undefined, undefined]
}

function setMajorStageOfOne(interactiveIndicateIndex){ 
    if (isAddStage){
        if (interactiveIndicateIndex === 2){
            nValue[1] = '"' + nValue[0] + '"'
        }
        if (interactiveIndicateIndex === 3)
            nValueAssign[0] = true
        if (interactiveIndicateIndex === 4)
            nValue[2] = nValue[0]
        if (interactiveIndicateIndex === 5)
            nValueAssign[1] = true
        if (interactiveIndicateIndex === 6)
            nValue[3] = nValue[0]
    }

    if (isSubStage) {
        if (interactiveIndicateIndex === 1)
            nValue[1] = undefined
        if (interactiveIndicateIndex === 2)
            nValueAssign[0] = false
        if (interactiveIndicateIndex === 3)
            nValue[2] = undefined
        if (interactiveIndicateIndex === 4)
            nValueAssign[1] = false
        if (interactiveIndicateIndex === 5)
            nValue[3] = undefined
        if (interactiveIndicateIndex === 6){
        }
    }
}

function setMajorStageOfTwo(interactiveIndicateIndex){ 
    if (isAddStage){
        if (interactiveIndicateIndex === 1)
            iValueAssign = true
        if (interactiveIndicateIndex === 2)
            iValue[0] = 0
    }

    if (isSubStage) {
        if (interactiveIndicateIndex === 0)
            iValueAssign = false
        if (interactiveIndicateIndex === 1)
            iValue[0] = undefined
    }
}

function setMajorStageOfThree(interactiveIndicateIndex){ 
    if (isAddStage){
        if (interactiveIndicateIndex === 1)
            sumValueAssign = true
        if (interactiveIndicateIndex === 2)
            sumValue[0] = 0
    }

    if (isSubStage) {
        if (interactiveIndicateIndex === 0)
            sumValueAssign = false
        if (interactiveIndicateIndex === 1)
            sumValue[0] = undefined
    }
}

function setMajorStageOfFour(interactiveIndicateIndex){ 
    if (isAddStage){
        if (interactiveIndicateIndex === 0){
            sumValueAssign = undefined
            scoreValue = ['', undefined, undefined, undefined, undefined]
            scoreValueAssign = [undefined, undefined]
            iLoopValue =  [undefined, undefined]
            iLoopValueAssign = undefined
            dymanicTempInputValue = undefined
            isConditionTrue = undefined
            iValue = [iValue[0], undefined, undefined, undefined]
            sumValue = [sumValue[0], undefined, undefined]
        }
        if (interactiveIndicateIndex === 1)
            iValue[2] = iValue[0]
        if (interactiveIndicateIndex === 2)
            iValue[3] = iValue[0]
        if (interactiveIndicateIndex === 3)
            isConditionTrue = Number.parseInt(iValue[3]) < Number.parseInt(nValue[3])
    }

    if (isSubStage) {
        if (interactiveIndicateIndex === 0){
            iValue[0] = iValue[2]
            iValue[2] = undefined
        }
        if (interactiveIndicateIndex === 1)
            iValue[3] = undefined
        if (interactiveIndicateIndex === 2)
            isConditionTrue = undefined
    }
}

function setMajorStageOfFive(interactiveIndicateIndex){

    if (isAddStage){
        if (interactiveIndicateIndex === 1)
            dynamicInputObject.push({
                isShow: true,
                label: '考試成績: ',
                defaultValue: defaultInputScore[dynamicInputObject.length],
                // value: nValue[0],
                isActive: true,
                onChange(value) {dymanicTempInputValue = value},
                isValid(inputString) {
                    const inputValue = Number.parseInt(inputString)
                    if (isEmpty(inputString)) return {valid:false, errorMsg:'輸入欄位不能為空'};
                    if (isNaN(inputValue)) return {valid:false, errorMsg:'請輸入數字'};
                    if (inputValue < 0) return {valid:false, errorMsg:'輸入數字必須大於零'};
                    return {valid:true, errorMsg:''}
                }
            })
        if (interactiveIndicateIndex === 2){
            allInputScoreValue.push(dymanicTempInputValue)
            dynamicInputObject[dynamicInputObject.length - 1].isActive = false
            scoreValue[0] = dymanicTempInputValue
            scoreValue[1] = '"' + scoreValue[0] + '"'
        }
        if (interactiveIndicateIndex === 3)
            scoreValueAssign[0] = true
        if (interactiveIndicateIndex === 4)
            scoreValue[2] = scoreValue[0]
        if (interactiveIndicateIndex === 5)
            scoreValueAssign[1] = true
        if (interactiveIndicateIndex === 6)
            scoreValue[3] = scoreValue[0]
    }

    if (isSubStage) {
        if (interactiveIndicateIndex === 0)
            dynamicInputObject.pop()
        if (interactiveIndicateIndex === 1){
            scoreValue[1] = undefined
            allInputScoreValue.pop()
        }
        if (interactiveIndicateIndex === 2)
            scoreValueAssign[0] = false
        if (interactiveIndicateIndex === 3)
            scoreValue[2] = undefined
        if (interactiveIndicateIndex === 4)
            scoreValueAssign[1] = false
        if (interactiveIndicateIndex === 5)
            scoreValue[3] = undefined
    }
}

function setMajorStageOfSix(interactiveIndicateIndex){   
    if (isAddStage){
        if (interactiveIndicateIndex === 1){
            sumValue[1] = sumValue[0]
            scoreValue[4] = scoreValue[0]
        }
        if (interactiveIndicateIndex === 2){
            sumValueAssign = true
        }
        if (interactiveIndicateIndex === 3){
            sumValue[2] = Number.parseInt(sumValue[1]) + Number.parseInt(scoreValue[4])
            sumValue[0] = sumValue[2]
        }
    }
    if (isSubStage) {
        if (interactiveIndicateIndex === 0){
            sumValue[1] = undefined
            scoreValue[4] = undefined
        }
        if (interactiveIndicateIndex === 1){
            sumValueAssign = false
        }
        if (interactiveIndicateIndex === 2){
            sumValue[0] = sumValue[2] - Number.parseInt(scoreValue[4])
            sumValue[2] = undefined
        }
    }
}

function setMajorStageOfSiven(interactiveIndicateIndex){   
    if (isAddStage){
        if (interactiveIndicateIndex === 1)
            iLoopValue[0] = iValue[0]
        if (interactiveIndicateIndex === 2){
            iLoopValueAssign = true
        }
        if (interactiveIndicateIndex === 3){
            iLoopValue[1] = iLoopValue[0] + 1
            iValue[0] = iLoopValue[1]
        }
    }
    if (isSubStage){
        if (interactiveIndicateIndex === 0)
            iLoopValue[0] = undefined
        if (interactiveIndicateIndex === 1)
            iLoopValueAssign = false
        if (interactiveIndicateIndex === 2){
            iValue[1] = iLoopValue[0] - 1
            iLoopValue[1] = undefined
        }
        if (interactiveIndicateIndex === 3){

            iLoopValue[1] = iValue[0]
            iLoopValueAssign = true
            iLoopValue[0] = iValue[0] - 1

            let temp = dymanicTempInputValue
            scoreValue = [temp, '"' + temp + '"', temp, temp, temp]
            sumValue[2] = sumValue[0]
            sumValueAssign = true
            sumValue[1] = sumValue[2] - temp


            iValue[2] = iLoopValue[0]
            iValue[3] = iLoopValue[0]

            isConditionTrue = Number.parseInt(iValue[3]) < Number.parseInt(nValue[3])
            iValueAssign = true
        }
    }
}

function setMajorStageOfEight(interactiveIndicateIndex){   
    if (isAddStage){
        if (interactiveIndicateIndex === 1){
            finalPrint[0] = nValue[3]
            finalPrint[1] = sumValue[0]
        }
        if (interactiveIndicateIndex === 2){
            finalPrint[2] = "平均為: " + (finalPrint[1] / finalPrint[0])
        }
    }

    if (isSubStage) {
        if (interactiveIndicateIndex === 0){
            finalPrint[0] = undefined
            finalPrint[1] = undefined
        }
        if (interactiveIndicateIndex === 1)
            finalPrint[2] = undefined
    }
}

export function getPrintResult(interactiveIndicateIndex){
    return [finalPrint[2]];
}

export function getProgramGrammar(){
    return (<WhileGrammar width={'390px'}/>)
}

export function getInputObject(sampleCodeIndicateIndex, interactiveIndicateIndex){
    return [
        {
            isShow: sampleCodeIndicateIndex > 1 || (sampleCodeIndicateIndex === 1 && interactiveIndicateIndex > 0),
            label: '請輸入n: ',
            defaultValue: 5,
            value: nValue[0],
            isActive: (sampleCodeIndicateIndex === 1 && interactiveIndicateIndex === 1),
            onChange(value) {nValue[0] = value},
            isValid(inputString) {
                const inputValue = Number.parseInt(inputString)
                if (isEmpty(inputString)) return {valid:false, errorMsg:'輸入欄位不能為空'};
                if (isNaN(inputValue)) return {valid:false, errorMsg:'請輸入數字'};
                if (inputValue < 0) return {valid:false, errorMsg:'輸入數字必須大於零'};
                return {valid:true, errorMsg:''}
            }
        },
        ...dynamicInputObject
    ]
}

function isEmpty(valid){
    return (valid === undefined) || (valid === '');
}
