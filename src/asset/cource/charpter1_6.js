export const courceTitle = `
    <p>身體質量指數 Body Mass Index (BMI) 是一個衡量肥胖程度的數值。</p>
    <p>請讀取身高、體重，並計算 BMI值。(float) Bmi=體重/(身高*身高) </p>
    <p>(EX：輸入-身高1.75(公尺)，體重70(公斤)，輸出-BMI =22.85…)</p>

`

export const programmingGrammar = `
    <p className="my-0"><span style="background: peachpuff;">變數</span> = int(input)</p>
    <p className="my-0"><span style="background: LightSkyBlue;">print(輸出內容)</span></p>
`

export const hintDialogContent = `
    <span style="fontWeight: bold;">程式碼第1行 : </span>
    <p>透過 input() 函式，取得使用者所輸入的字串，因身高可能會有小數點，使用float()函式，將數值定義為浮點數，將計算結果assign(指派)到變數 h 。</p>

    <span style="fontWeight: bold;">程式碼第2行：</span>
    <p>透過 input() 函式，使得輸入字串，因體重可能會有小數點，使用float()函式，將數值定義為浮點數，並assign(指派)給變數w。</p>

    <span style="fontWeight: bold;">程式碼第3行 : </span>
    <p>使用print() 函式列印出字串以及BMI運算內容。</p>
`;

export const courceIndex = '單元範例-例題6'

export const sampleCode = [
    `<p className="my-0"><span style="background: peachpuff;">h</spn>=float(input("請輸入身高(公尺):"))</p>`,
    `<p className="my-0"><span style="background: peachpuff;">w</spn> = float(input("請輸入體重(公斤):"))</p>`,
    `<p className="my-0"><span style="background: LightSkyBlue;">print("BMI=",w/(h*h))</span></p>`,
]

export const defaultCourceData = [
    {
        type: 'codeExplain',
        data: [
            {type: 'hint', lable:'變數名稱', content:'變數值'},
            {type: 'variable', lable:'h', content:'1.75', showInTopicStep: 1, showInTeachStep: 5},
            {type: 'assign', showInTopicStep: 1, showInTeachStep: 4},
            {type: 'input', lable:'float()', content:'1.75', showInTopicStep: 1, showInTeachStep: 3},
            {type: 'assign', showInTopicStep: 1, showInTeachStep: 2},
            {type: 'input', lable:'input("請輸入身高(公尺):")', content:'"1.75"', showInTopicStep: 1, showInTeachStep: 1},
            {type: 'hint', lable:'函式', content:'函數傳回值'},
        ],
    },
    {
        type: 'codeExplain',
        data: [
            {type: 'hint', lable:'變數名稱', content:'變數值'},
            {type: 'variable', lable:'w', content:'70', showInTopicStep: 2, showInTeachStep: 5},
            {type: 'assign', showInTopicStep: 2, showInTeachStep: 4},
            {type: 'input', lable:'float()', content:'70', showInTopicStep: 2, showInTeachStep: 3},
            {type: 'assign', showInTopicStep: 2, showInTeachStep: 2},
            {type: 'input', lable:'input("請輸入體重(公斤):")', content:'"70"', showInTopicStep: 2, showInTeachStep: 1},
            {type: 'hint', lable:'函式', content:'函數傳回值'},
        ],
    },
    {
        type: 'printer',
        data: [
            {type: 'hint'},
            {type: 'text', lable:'', content:'print("BMI=",'},
            {type: 'variable', lable:'w', content:'70', showInTopicStep: 3, showInTeachStep: 1},
            {type: 'text', lable:'', content:'/('},
            {type: 'variable', lable:'h', content:'1.75', showInTopicStep: 3, showInTeachStep: 1},
            {type: 'text', lable:'', content:'*'},
            {type: 'variable', lable:'h', content:'1.75', showInTopicStep: 3, showInTeachStep: 1},
            {type: 'text', lable:'', content:'))'},
            {type: 'hint'},
        ]
    },
]

export const stateObject = {
    topicStep: 0,
    teachStep: 0,
    teachStepOfTopic: [0, 5, 5, 1]
}

export const inputObject = [
    {
        label: '請輸入身高(公尺)：',
        content: '1.75', 
        invalidMsg: '輸入欄位不能為空',
        showInTopicStep: 1, 
        showInTeachStep: 0, 
        setData(courceData, value) { return setHigh(courceData, value)}, 
        isValid(value){ 
            if (isEmpty(value)) return {isValid:false, errorMsg:'輸入欄位不能為空'};
            if (isNaN(value)) return {isValid:false, errorMsg:'請輸入數字'};
            return {isValid:true, errorMsg:''}
        }
    },
    {
        label: '請輸入體重(公斤)：',
        content: '70', 
        invalidMsg: '輸入欄位不能為空',
        showInTopicStep: 2, 
        showInTeachStep: 0, 
        setData(courceData, value) { return setWiegth(courceData, value)}, 
        isValid(value){ 
            if (isEmpty(value)) return {isValid:false, errorMsg:'輸入欄位不能為空'};
            if (isNaN(value)) return {isValid:false, errorMsg:'請輸入數字'};
            return {isValid:true, errorMsg:''}
        }
    },
]

function setHigh(courceData, value){
    courceData[0].data[1].content = Number.parseFloat(value);
    courceData[0].data[3].content = Number.parseFloat(value);
    courceData[0].data[5].content = '"' + value + '"';

    courceData[2].data[4].content = Number.parseFloat(value);
    courceData[2].data[6].content = Number.parseFloat(value);
    return courceData;
}

function setWiegth(courceData, value){
    courceData[1].data[1].content = Number.parseFloat(value);
    courceData[1].data[3].content = Number.parseFloat(value);
    courceData[1].data[5].content = '"' + value + '"';
    courceData[2].data[2].content = Number.parseFloat(value);
    return courceData;
}

function isEmpty(valid){
    return (valid === undefined) || (valid === '');
}

export function getResult(courceData){
    const high = Number.parseFloat(courceData[2].data[4].content);
    const wiegth = Number.parseFloat(courceData[2].data[2].content);    
    return [
        {
            showInTopicStep: 3,
            showInTeachStep: 1, 
            print: "BMI= " + wiegth / (high * high)
        }
    ];
}