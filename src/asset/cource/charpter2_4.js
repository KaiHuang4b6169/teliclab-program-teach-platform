export const courceTitle = `
    <p className="my-0">有台測謊機的運作機制是這樣的，它會根據手掌出汗有無(1為出汗，0為無)，以及心跳快慢(每分鐘高於80次則算快)，藉此判斷這個人是否有說謊。 </p>
    <p className="my-0">請試著將這台測謊機的運作，以Python if…else條件判斷表示。</p>
    <p className="my-0">(EX: 輸入-手掌是否有出汗(1為出汗，0為無):1   心跳快慢(每分鐘高於80次則算快):90 輸出-說謊)</p>
`

export const programmingGrammar = `
    <p className="my-0"><span style="background:#AAD8C1;">if 條件運算值 :</span></p>
    <p className="my-0"><span style="background:#AAD8C1; white-space:pre;">  </span>&nbsp;條件"成立"時所執行的指令</p>
    <p className="my-0"><span style="background:#AAD8C1;">else:</span></p>
    <p className="my-0"><span style="background:#AAD8C1; white-space:pre;">  </span>&nbsp;條件"不成立"時所執行的指令</p>
`

export const hintDialogContent = `
    <p>程式碼第1-2行：</p>
    <p>透過input()函式，取得使用者所輸入的字串，為了使輸入的數字都呈數值資料，使用int()函式，從字串型別轉換為整數型別，再 assign(指派) 給變數 h 和b。</p>

    <p>程式碼第3-6行：</p>
    <p>運用if…else條件判斷語法，如果手掌有出汗且心跳每分鐘大於等於80下，代表說謊，反之沒有說謊。</p>
`;

export const courceIndex = '單元範例-例題4'

export const sampleCode = [
    `<p className="my-0" style="font-size: 10px"><span style="background: peachpuff;">h</span> = int(input("手掌是否有出汗(1為出汗，0為無)："))</p>`,
    `<p className="my-0" style="font-size: 10px"><span style="background: peachpuff;">b</span> = int(input("每分鐘心跳速度："))</p>`,
    `<p className="my-0" style="font-size: 10px"><span style="background:#AAD8C1;">if (h == 1 and b >= 80):</p>`,
    `<p className="my-0" style="font-size: 10px"><span style="background:#AAD8C1; white-space:pre;">  </span>&nbsp;<span style="background: LightSkyBlue;">print("說謊")</span></p>`,
    `<p className="my-0" style="font-size: 10px"><span style="background:#AAD8C1;">else:</span></p>`,
    `<p className="my-0" style="font-size: 10px"><span style="background:#AAD8C1; white-space:pre;">  </span>&nbsp;<span style="background: LightSkyBlue;">print("沒說謊")</span></p>`

]

export const defaultCourceData = [
    {
        type: 'codeExplain',
        data: [
            {type: 'hint', lable:'變數名稱', content:'變數值'},
            {type: 'variable', lable:'h', content:'1', showInTopicStep: 1, showInTeachStep: 5},
            {type: 'assign', showInTopicStep: 1, showInTeachStep: 4},
            {type: 'input', lable:'int()', content:'1', showInTopicStep: 1, showInTeachStep: 3},
            {type: 'assign', showInTopicStep: 1, showInTeachStep: 2},
            {type: 'input', lable:'input("手掌是否有出汗(1為出汗，0為無)：")', content:'"1"', showInTopicStep: 1, showInTeachStep: 1},
            {type: 'hint', lable:'函式', content:'函數傳回值'},
        ],
    },
    {
        type: 'codeExplain',
        data: [
            {type: 'hint'},
            {type: 'variable', lable:'b', content:'90', showInTopicStep: 2, showInTeachStep: 5},
            {type: 'assign', showInTopicStep: 2, showInTeachStep: 4},
            {type: 'input', lable:'int()', content:'90', showInTopicStep: 2, showInTeachStep: 3},
            {type: 'assign', showInTopicStep: 2, showInTeachStep: 2},
            {type: 'input', lable:'input("每分鐘心跳速度：")', content:'"90"', showInTopicStep: 2, showInTeachStep: 1},
            {type: 'hint'},
        ],
    },
]

export function conditionCourceData(teachData) {
    const h = teachData[0].data[1].content;
    const b = teachData[1].data[1].content;
    return [
        {
            type: 'condition',
            content: {
                type: 'conditionStatementBlock',
                content:{
                    title: 'if條件運算值',
                    data: [
                        {type: 'variable', lable:'h', content: h, showInTopicStep: 1, showInTeachStep: 0},
                        {type: 'text', lable:'', content:'== 1 and'},
                        {type: 'variable', lable:'b', content: b, showInTopicStep: 1, showInTeachStep: 0},
                        {type: 'text', lable:'', content:'>= 80)'},
                    ],
                }
            }
        },
        {
            type: 'true',
            content: {
                type: 'printBlock',
                content: {
                    title: '條件 "成立" 時\n所執行的指令',
                    data: [
                        {type: 'text', lable:'', content:'print("說謊")'},
                    ],
                }
            }
        },
        {
            type: 'false',
            content: {
                type: 'printBlock',
                content: {
                    title: '條件 "不成立" 時\n所執行的指令',
                    data: [
                        {type: 'text', lable:'', content:'print("沒說謊")'},
                    ],
                }
            }
        },
    ]
}

export const stateObject = {
    topicStep: 0,
    teachStep: 0,
    teachStepOfTopic: [0, 5, 5, 1, 1],
    teachAreatype: [
        {type: "normal", topicStep: 0},
        {type: "condition", topicStep: 3}
    ]
}

export const inputObject = [
    {
        label: '手掌是否有出汗(1為出汗，0為無)：',
        content: '1', 
        invalidMsg: '輸入欄位不能為空',
        showInTopicStep: 1, 
        showInTeachStep: 0, 
        setData(courceData, value) { return setSweat(courceData, value)}, 
        isValid(value){ 
            if (isEmpty(value)) return {isValid:false, errorMsg:'輸入欄位不能為空'};
            if (isNaN(value)) return {isValid:false, errorMsg:'請輸入數字'};
            return {isValid:true, errorMsg:''}
        }
    },
    {
        label: '每分鐘心跳速度：',
        content: '90', 
        invalidMsg: '輸入欄位不能為空',
        showInTopicStep: 2, 
        showInTeachStep: 0, 
        setData(courceData, value) { return setBeat(courceData, value)}, 
        isValid(value){ 
            if (isEmpty(value)) return {isValid:false, errorMsg:'輸入欄位不能為空'};
            if (isNaN(value)) return {isValid:false, errorMsg:'請輸入數字'};
            return {isValid:true, errorMsg:''}
        }
    },
]

function setSweat(courceData, value){    
    courceData[0].data[1].content = Number.parseInt(value);
    courceData[0].data[3].content = Number.parseInt(value);
    courceData[0].data[5].content = '"' + value + '"';
    return courceData;
}

function setBeat(courceData, value){    
    courceData[1].data[1].content = Number.parseInt(value);
    courceData[1].data[3].content = Number.parseInt(value);
    courceData[1].data[5].content = '"' + value + '"';
    return courceData;
}

function isEmpty(valid){
    return (valid === undefined) || (valid === '');
}

export function getResult(courceData){
    return [
        {
            showInTopicStep: 4,
            showInTeachStep: 1, 
            print: isLie(courceData) ? '說謊' : '沒說謊'
        }
    ];
}

export function isShowSampleCodeArrow(activateStep, index, courceData){
    if (activateStep < 4)
        return activateStep === index+1
    else if (activateStep === 4)
        return isLie(courceData) ? index === 3 : index === 5
    else
        return false
}

export function graphCondition(courceData){
    return {showArrow: {showInTopicStep: 3, showInTeachStep: 1}, changeLink: {showInTopicStep: 4, showInTeachStep: 1}, isStatify: isLie(courceData)}
}

function isLie(courceData){    
    const sweat = Number.parseInt(courceData[0].data[1].content);
    const beat = Number.parseInt(courceData[1].data[1].content);

    return sweat === 1 && beat >= 80;
}