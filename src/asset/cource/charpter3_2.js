import { Row, Stack } from "react-bootstrap";
import { DownAssign, UpAssign, WhileGrammar, WhileTrue } from "../images";
import { stageAddTools, stageSubTools } from "./common/commonTools";
import { elementOrigeBackgroundStyle, elementRangeBackgroundStyle, elementWhiteBackgroundStyle, middleStyle, variableMinWidth } from "./common/courceCss";
import { assignProgramComponent, explainProgramComponent, getPrint, getWhileBlock, interfaceProgramComponent, textProgramComponent, variableProgramComponent } from "./common/teachingAreaCommon";
import { getExplainOfVariableOfRangeLine, getRangeAsignArrowOfRangeLine, getRangeExplain, getRangeOfRangeLine, getRangeTags, getVariableOfRangeLine, interfaceOfRangeLine } from "./common/teachingAreaLoopCommon";

let variableInWhile=undefined
let variableOfAssignInWhile=undefined
let variableOfPrint=undefined
let variableOfAssign=undefined
let variableOfBeAdded=undefined
let isConditionTrue = false
let isShowAssign = false
let isShowDownAssign = false
let isShowUpAssign = false
let isAddStage = undefined
let isSubStage = undefined

let rangeKeyList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
let numberOfPrintInRange = 0;

const numberOfEachStage = [0, 1, 2, 2, 4]

export const courceTitle = (
    <div>
        <p className="my-0">while迴圈算出1~10的 值並列印到螢幕上</p>
    </div>
)

export const sampleCode = [
    (<div style={{display: 'flex'}}>
        <span style={{background: 'peachpuff'}}>i</span>
        <span> = </span>
        <span>1</span>
    </div>),
    (<div style={{display: 'flex'}}>
        <div style={{background: '#D1A4CA'}}>
            while(
            <span style={{background: 'white'}}>{'(i<11)'}</span>
            ):
        </div>
    </div>),
    (<p className="my-0" style={{display: 'flex'}}><div style={{width: '20px'}}/>&nbsp;<span style={{background: '#C9C9C9'}}>print(i)</span>&nbsp;<DownAssign height='20px' width='20px'/></p>),
    (<p className="my-0"><UpAssign height='20px' width='20px'/>&nbsp;<span style={{background: '#C9C9C9'}}>i = i + 1</span></p>),
]

export const programExplaiin = 
`程式碼第1行 : 
初紹化變數i的值為1，即由1開始(用來判斷while是否需要繼續執行的變數)

程式碼第2行:
透過while()函式，執行相同的指令，直到不符合條件式(i <11)，即會跳出迴圈。

在迴圈中執行的指令:
    1. 透過print()函式把i的值印出來
    2. i值加1且更新i的值。`;

export function nextStage(sampleCodeIndicateIndex, interactiveIndicateIndex){
    isAddStage = true
    isSubStage = false
    if (numberOfPrintInRange > 4) 
        return {
            sampleCodeIndicateIndex : sampleCodeIndicateIndex,
            interactiveIndicateIndex: interactiveIndicateIndex
        };

    const isLastestStage = (sampleCodeIndicateIndex === numberOfEachStage.length - 1) && (interactiveIndicateIndex === numberOfEachStage[numberOfEachStage.length - 1])

    if (isLastestStage) {
        sampleCodeIndicateIndex = 2
        interactiveIndicateIndex = 0
    }
    else {
        let {margeStage, minorSage} = stageAddTools(sampleCodeIndicateIndex, interactiveIndicateIndex, numberOfEachStage)
        sampleCodeIndicateIndex = margeStage
        interactiveIndicateIndex = minorSage
    }
    
    return {
        sampleCodeIndicateIndex : sampleCodeIndicateIndex,
        interactiveIndicateIndex: interactiveIndicateIndex
    };
}

export function priviousStage(sampleCodeIndicateIndex, interactiveIndicateIndex){
    isAddStage = false
    isSubStage = true
    let {margeStage, minorSage} = stageSubTools(sampleCodeIndicateIndex, interactiveIndicateIndex, numberOfEachStage)
    const isMinStage = (margeStage === 1) && (minorSage === numberOfEachStage[1])
    if (variableInWhile !== undefined && isMinStage){
        margeStage = numberOfEachStage.length - 1
        minorSage = numberOfEachStage[numberOfEachStage.length - 1]
    }

    return {
        sampleCodeIndicateIndex : margeStage,
        interactiveIndicateIndex: minorSage
    };
}

export function courceTeachingArea(sampleCodeIndicateIndex, interactiveIndicateIndex){
    if (sampleCodeIndicateIndex === 0) setMajorStageOfZero(interactiveIndicateIndex)
    if (sampleCodeIndicateIndex === 1) setMajorStageOfOne(interactiveIndicateIndex)
    if (sampleCodeIndicateIndex === 2) setMajorStageOfTwo(interactiveIndicateIndex)
    if (sampleCodeIndicateIndex === 3) setMajorStageOfThree(interactiveIndicateIndex)
    if (sampleCodeIndicateIndex === 4) setMajorStageOfFour(interactiveIndicateIndex)

    const assignStyle = {minWidth: '66px'}
    const whileTrue = (isConditionTrue) ? <WhileTrue style={{height: '40px'}}/> : undefined
    const upAssign = (isShowUpAssign) ?  <UpAssign style={{height: '36px'}}/> : undefined
    const downAssign = (isShowDownAssign) ? <DownAssign style={{height: '36px'}}/> : undefined
    
    return (
        <div>
            <Stack direction="horizontal" className="justify-content-evenly">
                <Stack direction="horizontal">
                    {explainProgramComponent('變數名稱')}
                    {variableProgramComponent('i', variableOfAssignInWhile)}
                </Stack>


                <Stack direction="horizontal">
                    {getWhileBlock('while條件運算值', (
                        <Stack direction="horizontal" className='px-1'>
                            {textProgramComponent('( ', elementWhiteBackgroundStyle, 'py-1')}
                            {interfaceProgramComponent('i',<div style={{...elementOrigeBackgroundStyle, minWidth: '20px',  minHeight: '20px'}}>{variableInWhile}</div>, {}, {}, {...variableMinWidth, height: '36px', ...elementWhiteBackgroundStyle}, '')}
                            {textProgramComponent(' ) < 11', elementWhiteBackgroundStyle, 'py-1')}
                        </Stack>
                    ))}
                    
                    {interfaceProgramComponent('', whileTrue, {minWidth: '100px'})}
                </Stack>
            </Stack>

            <Stack className="mb-1 justify-content-center" direction="horizontal">
                <Stack direction="horizontal">
                    {interfaceProgramComponent('', undefined, assignStyle)}
                    {getPrint(
                        [
                            textProgramComponent('print('),
                            variableProgramComponent('i', variableOfPrint),
                            textProgramComponent(')'),
                        ],                    
                        {width: '150px'}
                    )}
                    {interfaceProgramComponent('', downAssign, assignStyle)}
                </Stack>
            </Stack>

            <Stack className="mb-1 justify-content-center" direction="horizontal">
                
                <Stack direction="horizontal">
                    {interfaceProgramComponent('', upAssign, assignStyle)}
                    {getPrint(
                        [
                            variableProgramComponent('i', variableOfAssign),
                            assignProgramComponent(isShowAssign),
                            variableProgramComponent('i', variableOfBeAdded),
                            textProgramComponent('+1'),
                        ],
                        {width: '150px'}
                    )}
                    {interfaceProgramComponent('', undefined, assignStyle)}
                </Stack>
            </Stack>
        </div>
    );
}

function setMajorStageOfZero(){ 
    variableInWhile=undefined
    variableOfAssignInWhile=undefined
    variableOfPrint=undefined
    variableOfAssign=undefined
    variableOfBeAdded=undefined
    isConditionTrue = false
    isShowAssign = false
    isShowUpAssign = false
    isShowDownAssign = false
    numberOfPrintInRange = 0
}

function setMajorStageOfOne(interactiveIndicateIndex){ 
    
    if (isAddStage){
        if (interactiveIndicateIndex === 1)
            variableOfAssignInWhile = 1
    } else {
        if (interactiveIndicateIndex === 0)
        variableOfAssignInWhile = undefined
    }
}

function setMajorStageOfTwo(interactiveIndicateIndex){   
    if (isAddStage){ 
        if (interactiveIndicateIndex === 1){
            if (isShowUpAssign) {
                variableOfAssignInWhile = variableOfAssign
                variableOfPrint=undefined
                variableOfAssign=undefined
                variableOfBeAdded=undefined
                isConditionTrue = false
                isShowAssign = false
                isShowUpAssign = false
                isShowDownAssign = false
            }
            variableInWhile = variableOfAssignInWhile
        }
        if (interactiveIndicateIndex === 2)
            isConditionTrue = variableInWhile < 11
    }

    if (isSubStage){
        if (interactiveIndicateIndex === 0){
            if (variableInWhile === 1){
                variableInWhile = undefined
            }
            else {
                variableOfAssignInWhile -= 1
                variableInWhile = variableOfAssignInWhile

                isShowUpAssign = true
                variableOfAssign = variableOfAssignInWhile + 1
                isShowAssign = true
                variableOfBeAdded = variableOfAssign - 1
                variableOfPrint = variableOfBeAdded
                isShowDownAssign = true
                isConditionTrue = true
            }
        }
        if (interactiveIndicateIndex === 1)
            isConditionTrue = false
    }
}

function setMajorStageOfThree(interactiveIndicateIndex){
    if (isAddStage){
        if (interactiveIndicateIndex === 1){
            isShowDownAssign = true
        }
        if (interactiveIndicateIndex === 2){
            variableOfPrint = variableOfAssignInWhile
            numberOfPrintInRange += 1
        }
    }

    if (isSubStage){
        if (interactiveIndicateIndex === 0)
            isShowDownAssign = false
        if (interactiveIndicateIndex === 1){
            numberOfPrintInRange = numberOfPrintInRange - 1
            variableOfPrint = undefined
        }

    }
}

function setMajorStageOfFour(interactiveIndicateIndex){
    if (isAddStage){
        if (interactiveIndicateIndex === 1){
            variableOfBeAdded = variableOfAssignInWhile
        }
        if (interactiveIndicateIndex === 2){
            isShowAssign = true
        }
        if (interactiveIndicateIndex === 3){
            variableOfAssign = variableOfBeAdded + 1
            if (numberOfPrintInRange >= 3) numberOfPrintInRange = rangeKeyList.length
        }
        if (interactiveIndicateIndex === 4){
            isShowUpAssign = true
        }
    }
    
    if (isSubStage){
        if (interactiveIndicateIndex === 0){
            variableOfBeAdded = undefined
        }
        if (interactiveIndicateIndex === 1){
            isShowAssign = false
        }
        if (interactiveIndicateIndex === 2){
            variableOfAssign = undefined
            if (variableOfBeAdded === 3) numberOfPrintInRange -= 1
            if (numberOfPrintInRange > 4) numberOfPrintInRange = 3
        }
        if (interactiveIndicateIndex === 3){
            isShowUpAssign = false
        }
        if (interactiveIndicateIndex === 4){
        }
    }
}

function generatePrintResult(){
    let printResult = []
    rangeKeyList.map((element, key) => {
        if (numberOfPrintInRange > key ) printResult.push(<Row style={{height: '40px'}}><label className="py-1">{element}</label></Row>)
    })
    return printResult;
}

export function getPrintResult(interactiveIndicateIndex){
    return generatePrintResult();
}

export function getProgramGrammar(){
    return (<WhileGrammar width={'390px'}/>)
}

export function getInputObject(sampleCodeIndicateIndex, interactiveIndicateIndex){return []}