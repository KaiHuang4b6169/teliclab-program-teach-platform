export const courceTitle = `
    <p className="my-0">假日，美伢決定帶小新去看電影。來到電影院後，看著人工窗口大排長龍的隊伍，於是便決定去空無一人的電子售票機購票。</p>
    <p className="my-0">這台電子售票機的功能並不完善，每次只能販售一張電影票。</p>
    <p className="my-0">請同學試著寫出，能夠讓機器自動進行條件判斷的程式，亦即：消費者只要輸入年齡，電子售票機就會顯示相對應的票價。 </p>
    <p className="my-0">開心電影院的票價： 60歲以上(敬老票)，票價為150元。 12歲到59歲(一般票)，票價為200元。 低於12歲，票價為170元。</p>
    <p className="my-0">(EX:輸入-10  輸出-票價為170)</p>

`

export const programmingGrammar = `
    <p className="my-0"><span style="background:#AAD8C1;">if 條件運算值1 :</span></p>
    <p className="my-0"><span style="background:#AAD8C1; white-space:pre;">  </span>&nbsp;條件1"成立"時所執行的指令</p>
    <p className="my-0"><span style="background:#AAD8C1;">elif 條件運算值2 :</span></p>
    <p className="my-0"><span style="background:#AAD8C1; white-space:pre;">  </span>&nbsp;條件2"成立"時所執行的指令</p>
    <p className="my-0"><span style="background:#AAD8C1;">else:</span></p>
    <p className="my-0"><span style="background:#AAD8C1; white-space:pre;">  </span>&nbsp;條件1、2皆"不成立"時所執行的指令</p>
`

export const hintDialogContent = `
    <p>程式碼第1行：</p>
    <p>透過input()函式，取得使用者所輸入的字串，為了使輸入的年齡都呈數值資料，使用int()函式，從字串型別轉換為整數型別，再 assign(指派) 給變數 age。</p>

    <p>程式碼第2-3行：</p>
    <p>運用if…elif…else條件判斷語法，如果年齡大於等於60歲，代表票價為150元。</p>

    <p>程式碼第4-5行：</p>
    <p>當條件1的比較結果不為真時，來到判斷條件2，如果年齡大於等於12歲，代表票價為200元。</p>

    <p>程式碼第6-7行：</p>
    <p>反之票價170元。</p>
`;

export const courceIndex = '單元範例-例題5'

export const sampleCode = [
    `<p className="my-0"><span style="background: peachpuff;">age</span> =int(input("請輸入年齡："))</p>`,
    `<p className="my-0"><span style="background:#AAD8C1;">if (age>=60):</p>`,
    `<p className="my-0"><span style="background:#AAD8C1; white-space:pre;">  </span>&nbsp;<span style="background: LightSkyBlue;">print("票價150元")</span></p>`,
    `<p className="my-0"><span style="background:#AAD8C1;">elif(age>=12):</p>`,
    `<p className="my-0"><span style="background:#AAD8C1; white-space:pre;">  </span>&nbsp;<span style="background: LightSkyBlue;">print("票價200元")</span></p>`,
    `<p className="my-0"><span style="background:#AAD8C1;">else:</span></p>`,
    `<p className="my-0"><span style="background:#AAD8C1; white-space:pre;">  </span>&nbsp;<span style="background: LightSkyBlue;">print("票價170元")</span></p>`

]

export const defaultCourceData = [
    {
        type: 'codeExplain',
        data: [
            {type: 'hint', lable:'變數名稱', content:'變數值'},
            {type: 'variable', lable:'age', content:'10', showInTopicStep: 1, showInTeachStep: 5},
            {type: 'assign', showInTopicStep: 1, showInTeachStep: 4},
            {type: 'input', lable:'int()', content:'10', showInTopicStep: 1, showInTeachStep: 3},
            {type: 'assign', showInTopicStep: 1, showInTeachStep: 2},
            {type: 'input', lable:'input("請輸入年齡：")', content:'"10"', showInTopicStep: 1, showInTeachStep: 1},
            {type: 'hint', lable:'函式', content:'函數傳回值'},
        ],
    },
]

export function conditionCourceData(teachData) {
    return [
        {
            type: 'condition',
            content: {
                type: 'conditionStatementBlock',
                content:{
                    title: 'if條件運算值',
                    data: [
                        {type: 'variable', lable:'age', content: teachData[0].data[1].content, showInTopicStep: 1, showInTeachStep: 5},
                        {type: 'text', lable:'', content:'>=60'},
                    ],
                }
            }
        },
        {
            type: 'true',
            content: {
                type: 'printBlock',
                content: {
                    title: '條件 "成立" 時\n所執行的指令',
                    data: [
                        {type: 'text', lable:'', content:'print("票價150元)'},
                    ],
                }
            }
        },
        {
            type: 'false',
            content: {
                type: 'conditionBlock',
                content: {
                    title: '條件 "不成立" 時\n所執行的指令',
                    data: [
                        {
                            type: 'condition',
                            content: {
                                type: 'conditionStatementBlock',
                                content:{
                                    title: 'if條件運算值',
                                    data: [
                                        {type: 'variable', lable:'age', content: teachData[0].data[1].content, showInTopicStep: 1, showInTeachStep: 5},
                                        {type: 'text', lable:'', content:'>=12'},
                                    ],
                                }
                            }
                        },
                        {
                            type: 'true',
                            content: {
                                type: 'printBlock',
                                content: {
                                    title: '條件 "成立" 時\n所執行的指令',
                                    data: [
                                        {type: 'text', lable:'', content:'print("票價200元)'},
                                    ],
                                }
                            }
                        },
                        {
                            type: 'false',
                            content: {
                                type: 'printBlock',
                                content: {
                                    title: '條件 "不成立" 時\n所執行的指令',
                                    data: [
                                        {type: 'text', lable:'', content:'print("票價170元")'},
                                    ],
                                }
                            }
                        },
                    ]
                }
            }
        },
    ]
}

export const stateObject = {
    topicStep: 0,
    teachStep: 0,
    teachStepOfTopic: [0, 5, 1, 2, 1],
    teachAreatype: [
        {type: "normal", topicStep: 0},
        {type: "condition", topicStep: 2}
    ]
}

export const inputObject = [
    {
        label: '請輸入年齡：',
        content: '10', 
        invalidMsg: '輸入欄位不能為空',
        showInTopicStep: 1, 
        showInTeachStep: 0, 
        setData(courceData, value) { return setAge(courceData, value)}, 
        isValid(value){ 
            if (isEmpty(value)) return {isValid:false, errorMsg:'輸入欄位不能為空'};
            if (isNaN(value)) return {isValid:false, errorMsg:'請輸入數字'};
            return {isValid:true, errorMsg:''}
        }
    },
]

function setAge(courceData, value){    
    courceData[0].data[1].content = Number.parseInt(value);
    courceData[0].data[3].content = Number.parseInt(value);
    courceData[0].data[5].content = '"' + value + '"';

    courceData[3].data[3].content = Number.parseInt(value);
    return courceData;
}

function isEmpty(valid){
    return (valid === undefined) || (valid === '');
}

export function getResult(courceData){
    const age = Number.parseInt(courceData[0].data[1].content);
    const isElder = age >= 60
    const isAldult = age >= 12

    let resultMsg = '';
    if (isElder)
        resultMsg = '票價150元';
    else if (isAldult)
        resultMsg = '票價200元';
    else        
        resultMsg = '票價170元';

    return [     
        {
            showInTopicStep: isElder ? 3 : 4,
            showInTeachStep: 1, 
            print: resultMsg
        }
    ];
}

export function isShowSampleCodeArrow(activateStep, index, courceData){
    const age = Number.parseInt(courceData[0].data[1].content);
    const isElder = age >= 60
    const isAldult = age >= 12

    if (activateStep < 3)
        return activateStep === index+1
    else if (activateStep >= 3)
        if (isElder)
            return index === 2;
        else{
            if (activateStep === 4)
                return isAldult ? index === 4 :  index === 6
            return index === 3;
        }
    else
        return false
}

const UNLIMITED = 100;

export function graphCondition(courceData){
    const age = Number.parseInt(courceData[0].data[1].content);
    const isElder = age >= 60
    const isAldult = age >= 12
    return {
        showArrow: {showInTopicStep: 2, showInTeachStep: 1}, 
        changeLink: {showInTopicStep: 3, showInTeachStep: 1}, 
        isStatify: isElder,
        childCondition: {
            false: {
                showArrow: {showInTopicStep: isElder ? UNLIMITED : 3, showInTeachStep: 2}, 
                changeLink: {showInTopicStep: isElder ? UNLIMITED : 4, showInTeachStep: 1}, 
                isStatify: isAldult,
            }
        }
    }
}