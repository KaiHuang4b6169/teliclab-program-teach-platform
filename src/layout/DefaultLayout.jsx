function DefaultLayout(){ 
    return (    
        <Router history={hashHistory}>
            <Route path="/" component={App}>
            <Route path="about" component={About}/>
            <Route path="users/:userId" component={Users} />
            </Route>
        </Router>
        );
}

export default DefaultLayout;